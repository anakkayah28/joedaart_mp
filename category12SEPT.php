

<style>
.hero {
  width:960px;
  margin:1rem auto;
  position: relative;
}

.text {
  background:rgba(255,255,255,0.5);
  position: center;
  top:50%;
  left:0;
  padding:1rem;
  transform:translateY(-50%);
}

.card{
  border: 1.5px solid rgba(0,0,0,.125);
}

.col-md-3{
  width:100px;
}

hr { 
  width:90%; 
  height:0.002rem; 
  background: #676a6c;
  margin-top: -0.4rem;
  margin-bottom: 0rem;
}

@media (min-width: 768px) {
  /* show 3 items */
  .carousel-inner .active,
  .carousel-inner .active + .carousel-item,
  .carousel-inner .active + .carousel-item + .carousel-item {
    display: block;
  }


  .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),
  .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item,
  .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item {
    transition: none;
  }

  .carousel-inner .carousel-item-next,
  .carousel-inner .carousel-item-prev {
    position: relative;
    transform: translate3d(0, 0, 0);
  }

  .carousel-inner .active.carousel-item + .carousel-item + .carousel-item + .carousel-item {
    position: absolute;
    top: 0;
    right: -33.3333%;
    z-index: -1;
    display: block;
    visibility: visible;
  }

  /* left or forward direction */
  .active.carousel-item-left + .carousel-item-next.carousel-item-left,
  .carousel-item-next.carousel-item-left + .carousel-item,
  .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item,
  .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item {
    position: relative;
    transform: translate3d(-100%, 0, 0);
    visibility: visible;
  }

  /* farthest right hidden item must be abso position for animations */
  .carousel-inner .carousel-item-prev.carousel-item-right {
    position: absolute;
    top: 0; 
    left: 0;
    z-index: -1;
    display: block;
    visibility: visible;
  }

  /* right or prev direction */
  .active.carousel-item-right + .carousel-item-prev.carousel-item-right,
  .carousel-item-prev.carousel-item-right + .carousel-item,
  .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item,
  .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item {
    position: relative;
    transform: translate3d(100%, 0, 0);
    visibility: visible;
    display: block;
    visibility: visible;
  }

}


.carousel-control-prev-icon,
    .carousel-control-next-icon {
     
      background-image: none;
    }

    .carousel-control-next-icon:after
    {
      content: '>';
      font-size: 55px;
   
      
    }

    .carousel-control-prev-icon:after {
      content: '<';
      font-size: 55px;
    
    }

    .list-group-item{
      margin-left: -15px;
      margin-right: -15px;
    }

/* test style="max-width:1500px;"*/
</style>
  <div class="container">
    <div class="row mb-8">
		<?php
			$mySql = "SELECT * FROM categories  ORDER BY indeks";
			$myQry = mysqli_query($koneksidb, $mySql)  or die ("Query salah : ".mysql_error());
			while ($myData = mysqli_fetch_array($myQry)) {
		?>                   
    <div class="hero col-md-3 text-center  my-3 my-sm-2">     
      <div class="box1">
					<a class="img-link img-wrap w_hover"  href="./?page=collections&id=<?php  echo $myData['id']; ?>"> 
          <img  alt="" src="./public/images/<?php  echo $myData['image']; ?>" style="width:225px;height: auto;margin-top: 20px;"><span class="link-icon"></span></a> 
        <div class="d-flex flex-column text-center" style="margin-top: 15px;">
              <a class="text-dark h5"  href="/products/<?php   echo $myData['slug']; ?>"><b><?php  echo $myData['title']; ?></b></a>
              <a class="text-secondary h5 mb-0"  href="/products/<?php   echo $myData['slug']; ?>"><b><hr><?php   echo $myData['judul']; ?></hr></b></a>
              <a class="text-secondary h5 mt-0"  href="/products/<?php   echo $myData['slug']; ?>"><b><?php echo $myData['judul2']; ?></b></a>
      </div>
      </div>
    </div>
     <?php }; ?>
</div>

<div class="container-fluid  well span6" style="padding-top: 50px">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner row w-100 mx-auto">
        <?php
            $not=1;
            $active = '';
            $productrand = $db->getAllRecords(TB_PRD,'id,title,descs,artist,image,tahun,categorychar,ukuran','ORDER BY RAND() LIMIT 5');
          if(!empty($productrand)){
            foreach($productrand as $row){            
                if($not==1){$active = 'active'; } else {$active = '';}
                ?>
                  <div class="carousel-item col-md-4 <?php echo $active; ?>">
                    <div class="card" style="text-align: center;">
                    <a href="./?page=detail-collection&id=<?php echo $row['id'] ?>">
                    <img class="card-img-top img-fluid"  style="width: auto; height:240px; text-align: center;" src="./public/product_images/<?php echo $row['id'] ?>/<?php echo $row['image'] ?>" alt="Card image cap">
                      <div class="card-body"  style="font-family: Arial Bold;">
                        <h3 class="group inner list-group-item-heading" style="margin-bottom: 0px; font-size:17px"><?php echo $row['artist'] ?></h3>
                        <h4 class="group inner" style="margin-top: 5px;"><i>"<?php echo $row['title'] ?>"</i></h4>
                        <p class="group inner mb-0"><i><?php echo $row['ukuran']; ?></i></p>   
                        <p class="group inner mb-0" >( <?php echo $row['tahun']; ?> )</p>   
                      </div>
                    </a>
                    </div>
                  </div>
              <?php
              $not++;
            }
          }
      ?>
    </div>
   
    <a class="left carousel-control-prev" style="margin-left: -55px;" href="#myCarousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" style="filter: invert(100%);margin-left: -120px;align-self: center;  margin-top: -170px;" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control-next" style="margin-right: -45px;" href="#myCarousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" style="filter: invert(100%);margin-right: -120px;align-self: center;  margin-top: -170px;" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
</div>

<div class="container well span6" style="padding-top: 70px;max-width:1200px;"> 
<div class="header text-center h3 text-white" style="padding-bottom: 20px;padding-top: 20px;background-color:#FF0000;margin-left:-14px;margin-right:-14px;"><b> ABOUT ART & DESIGN</b> </div>
    <div class="row mb-5">
		<?php
			$mySql = "SELECT p.id,p.title,p.descs,p.artist,p.image,p.tahun,p.media,p.categorychar,p.ukuran,c.title FROM products p,categories c 
      WHERE p.category=c.id group by c.title order by c.indeks
      ";
			$myQry = mysqli_query($koneksidb, $mySql)  or die ("Query salah : ".mysql_error());
			while ($myData = mysqli_fetch_array($myQry)) {
		?>                   
   
<!-- style="width: 10rem;margin-left:1px;" -->
  <div class="card col-md-3  text-center box1" style="margin-bottom:15px;">
    <ul class="list-group list-group-flush">
      <li class="list-group-item h5"><b><?php  echo $myData['title']; ?></b></li>
      <li class="list-group-item" style="border-bottom-width: 0px;border-top-width: 3px;">
      <a class="img-link img-wrap w_hover"  href="./?page=collections&id=<?php  echo $myData['id']; ?>"> 
          <img  alt="" src="./public/product_images/<?php echo $myData['id'] ?>/<?php echo $myData['image'] ?>?>" style="width:225px;height: auto;margin-top: 20px;"><span class="link-icon"></span></a> 
      </li>
      </ul>
      <div class="align-self-end text-right"  style="margin-top: auto; border-top-width: 1px;">
                      <div class="p2" style="font-family: Arial Bold;">
                        <h3 class="group inner list-group-item-heading" style="margin-bottom: 0px; font-size:17px"><?php echo $myData['artist'] ?> "<?php echo $myData['title'] ?>"</h3>
                        <b><p class="group inner mb-0"><?php echo $myData['ukuran']; ?>, <?php echo $myData['media']; ?> ( <?php echo $myData['tahun']; ?> )</p></b> 
                        <p class="group inner mb-0"><i><?php echo $myData['descs']; ?></i></p>   
                         <a href="./?page=detail-collection&id=<?php echo $myData['id'] ?>">..Show More</a></p>   
                      </div>
      </div>
    
</div>

<?php }; ?>
</div>