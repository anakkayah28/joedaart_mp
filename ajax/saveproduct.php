<?php

include_once('../library/config.php');
include_once('../library/inc.library.php');


$title          = isset($_GET['title']) ? $_GET['title'] : '';
$sellerid       = '';
$artist         = isset($_GET['artist']) ? $_GET['artist'] : '';
$sellername     = '';
$category       = isset($_GET['category']) ? $_GET['category'] : '1';
$categorychar   = '';
$slug           = strtolower(preg_replace('/\s+/', '', $title));
$descs          = isset($_GET['description']) ? $_GET['description'] : '';
$price          = isset($_GET['price']) ? $_GET['price'] : '0';
$discount       = isset($_GET['discount']) ? $_GET['discount'] : '0';
$negotiable     = '0';
$folder         = '-';
$media          = isset($_GET['media']) ? $_GET['media'] : '';
$tahun          = isset($_GET['year']) ? $_GET['year'] : '';
$ukuran         = isset($_GET['size']) ? $_GET['size'] : '';
$kodeuser         = '1';
 
$kodeproduct = randgenerate(25);


$dataProduct = array(   
 	'kode'=>$kodeproduct,
    'title'=>$_GET['title'],
	'sellerid'=>$sellerid,
	'artist'=>$artist,
	'sellername'=>$sellername,
	'category'=>$category,
	'categorychar'=>$categorychar,
	'slug'=>$slug, 
	'descs'=>$descs,
	'price'=>$price,
	'discount'=>$discount, 
	'negotiable'=>$negotiable, 
	'folder'=>$folder, 
	'media'=>$media, 
	'tahun'=>$tahun,
	'ukuran'=>$ukuran,  
	'kodeuser'=>$kodeuser, 
	'activeflag'=>'1', 
	'created_by'=>'',
	'updated_by'=>''
 );
 $db->insert(TB_PRD,$dataProduct);

?>
