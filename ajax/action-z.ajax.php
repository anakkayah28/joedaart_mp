<?php
include_once('../library/config.php');
include_once('../library/inc.library.php');
 
$kodeproduct = randgenerate(25);
$kodeimage= randgenerate(25);

//$kodeproduct    = $_GET['kodeproduct'];

$title          = $_REQUEST['title'];
$sellerid       = $_REQUEST['sellerid'];
$artist         = $_REQUEST['artist'];
$sellername     = '-';//$_REQUEST['sellername'];
$category       = $_REQUEST['category'];
$categorychar   = '-';//$_REQUEST['categorychar'];
$slug           = '-';//$_REQUEST['slug'];
$descs          = $_REQUEST['descs'];
$price          = $_REQUEST['price'];
$discount       = $_REQUEST['discount'];
$negotiable     = $_REQUEST['negotiable'];
$folder         = '-';//$_REQUEST['folder'];
$media          = $_REQUEST['media'];
$tahun          = $_REQUEST['tahun'];
$ukuran         = $_REQUEST['ukuran'];
$active         = $_REQUEST['active'];

$dataProduct   =   array(
	'kode'=>$kodeproduct,
    'title'=>$title,
    'sellerid'=>$sellerid,
	'artist'=>$artist,
    'sellername'=>$sellername,
    'category'=>$category, 
	'categorychar'=>$categorychar,
	'slug'=>$slug, 
	'descs'=>$descs,
	'price'=>$price,
	'discount'=>$discount,
	'negotiable'=>$negotiable,
	'folder'=>$folder, 
	'media'=>$media,
	'tahun'=>$tahun,
	'ukuran'=>$ukuran,
	'activeflag'=>$active, 
	'created_by'=>'',
	'updated_by'=>''
);
$db->insert(TB_PRD,$dataProduct);


if(!empty($_FILES['files'])){
    $n=0;
    $s=0;
    $prepareNames   =   array();
    foreach($_FILES['files']['name'] as $val)
    {
        $infoExt        =   getimagesize($_FILES['files']['tmp_name'][$n]);
        $s++;
        $filesName      =   str_replace(" ","",trim($_FILES['files']['name'][$n]));
        $files          =   explode(".",$filesName);
        $File_Ext       =   substr($_FILES['files']['name'][$n], strrpos($_FILES['files']['name'][$n],'.'));
         
        if($infoExt['mime'] == 'image/gif' || $infoExt['mime'] == 'image/jpeg' || $infoExt['mime'] == 'image/png')
        {
            $srcPath    =   '../public/product_images/';
            $fileName   =   $s.rand(0,999).time().$File_Ext;
            $path   =   trim($srcPath.$fileName);
            if(move_uploaded_file($_FILES['files']['tmp_name'][$n], $path))
            {
                $prepareNames[] .=  $fileName; //need to be fixed.
                $Sflag      =   1; // success
            }else{
                $Sflag  = 2; // file not move to the destination
            }
        }
        else
        {
            $Sflag  = 3; //extention not valid
        }
        $n++;
    }
    if($Sflag==1){
        echo '{Images uploaded successfully!}';
    }else if($Sflag==2){
        echo '{File not move to the destination.}';
    }else if($Sflag==3){
        echo '{File extention not good. Try with .PNG, .JPEG, .GIF, .JPG}';
    }

    if(!empty($prepareNames)){
        $count  =  1;
        foreach($prepareNames as $name){
            $data   =   array(
                            'kode'=>$kodeimage,
                            'product'=>$kodeproduct,
                            'img_name'=>$name,
                            'img_order'=>$count++,
                        );
            $db->insert(TB_IMG,$data);
        }
    }
}
?>