<?php

# KONTROL MENU PROGRAM
if($_GET) {
	// Jika mendapatkan variabel URL ?page
	switch($_GET['page']){				
		case '' :
			if(!file_exists ("category.php")) die ("Access Denied"); 
			include "category.php";	break;
			
		 case 'collections' :
		 	if(!file_exists ("products.php")) die ("Access Denied"); 
			include "products.php";	break;
			 
		case 'kebijakan-privasi' :
			if(!file_exists ("kebijakanprivasi.php")) die ("Access Denied"); 
			include "kebijakanprivasi.php";	break;

		case 'pusat-bantuan' :
			if(!file_exists ("pusatbantuan.php")) die ("Access Denied"); 
			include "pusatbantuan.php";	break;
		
		case 'syarat-ketentuan' :
			if(!file_exists ("syaratketentuan.php")) die ("Access Denied"); 
			include "syaratketentuan.php";	break;

		case 'detail-collection' :
			if(!file_exists ("product-detail.php")) die ("Access Denied"); 
			include "product-detail.php";	break;

		case 'searching' :
			if(!file_exists ("search.php")) die ("Access Denied"); 
			include "search.php";	break;

		case 'register' :
			if(!file_exists ("register.php")) die ("Access Denied"); 
			include "register.php";	break;
	
		case 'register-success' :
			if(!file_exists ("register_success.php")) die ("Access Denied"); 
			include "register_success.php";	break;
	
		case 'login' :
			if(!file_exists ("./user/login.php")) die ("Access Denied"); 
			include "./user/login.php"; break;
		
		case 'login-validasi' :
			if(!file_exists ("./user/login_validasi.php")) die ("Access Denied"); 
			include "./user/login_validasi.php"; break;
			
		case 'logout' :
			if(!file_exists ("./user/login_out.php")) die ("Access Denied"); 
			include "./user/login_out.php"; break;		

		case 'conf' :
			if(!file_exists ("./user/verificationemail.php")) die ("Access Denied"); 
			include "./user/verificationemail.php"; break;		

		case 'changepass' :
			if(!file_exists ("./user/change_password.php")) die ("Access Denied"); 
			include "./user/change_password.php"; break;	
			
		case 'chapass' :	
			if(!file_exists ("./user/change_password_input.php")) die ("Access Denied"); 
			include "./user/change_password_input.php"; break;	

		case 'pass-success' :	
			if(!file_exists ("./user/update_pass_success.php")) die ("Access Denied"); 
			include "./user/update_pass_success.php"; break;	
		// case 'iadmin' :
		// 	if(!file_exists ("index-admin.php")) die ("Access Denied"); 
		// 	header("Location: http://theos.in/");
			
		// # Kategori
		// case 'Kategori-Data' :
		// 	if(!file_exists ("kategori/kategori_data.php")) die ("Access Denied"); 
		// 	include "kategori/kategori_data.php"; break;		
		// case 'Kategori-Add' :
		// 	if(!file_exists ("kategori/kategori_add.php")) die ("Access Denied"); 
		// 	include"kategori/kategori_add.php"; break;
		// case 'Kategori-Add?status=true' :
		// 	if(!file_exists ("kategori/kategori_add.php")) die ("Access Denied"); 
		// 	include"kategori/kategori_add.php"; break;		
		// case 'Kategori-Delete' :
		// 	if(!file_exists ("kategori/kategori_delete.php")) die ("Access Denied"); 
		// 	include "kategori/kategori_delete.php"; break;		
		// case 'Kategori-Edit' :
		// 	if(!file_exists ("kategori/kategori_edit.php")) die ("Access Denied"); 
		// 	include "kategori/kategori_edit.php"; break;

	
		// default:
		// 	if(!file_exists ("category.php")) die ("Access Denied"); 
		// 	include "category.php";						
		// break;
	}
}
else {
	// Jika tidak mendapatkan variabel URL : ?page
	if(!file_exists ("category.php")) die ("Access Denied1"); 
	include "category.php";	
}
?>