<?php

function generate_string($strength = 16) {
    $input = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $input_length = strlen($input);
    $random_string = '';
    for($i = 0; $i < $strength; $i++) {
        $random_character = $input[mt_rand(0, $input_length - 1)];
        $random_string .= $random_character;
    }
    return $random_string;
}
 
// Output: iNCHNGzByPjhApvn7XBD
echo generate_string(25);
 echo '<br>';
// Output: 70Fmr9mOlGID7OhtTbyj
echo generate_string(25);
echo '<br>';
echo generate_string(25);
echo '<br>';
echo generate_string(25);
echo '<br>';
echo generate_string(25);
echo '<br>';
echo generate_string(25);
echo '<br>';
// Output: Jp8iVNhZXhUdSlPi1sMNF7hOfmEWYl2UIMO9YqA4faJmS52iXdtlA3YyCfSlAbLYzjr0mzCWWQ7M8AgqDn2aumHoamsUtjZNhBfU

?>