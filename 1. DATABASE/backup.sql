-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.21 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for apitexhi_exhibition
DROP DATABASE IF EXISTS `apitexhi_exhibition`;
CREATE DATABASE IF NOT EXISTS `apitexhi_exhibition` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `apitexhi_exhibition`;

-- Dumping structure for table apitexhi_exhibition.hitung
DROP TABLE IF EXISTS `hitung`;
CREATE TABLE IF NOT EXISTS `hitung` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pengunjung_harian` int(11) NOT NULL DEFAULT '0',
  `pengunjung_bulanan` int(11) NOT NULL DEFAULT '0',
  `pengunjung_sampai_saat_ini` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table apitexhi_exhibition.hitung: 1 rows
DELETE FROM `hitung`;
/*!40000 ALTER TABLE `hitung` DISABLE KEYS */;
INSERT INTO `hitung` (`id`, `pengunjung_harian`, `pengunjung_bulanan`, `pengunjung_sampai_saat_ini`) VALUES
	(1, 665, 665, 666);
/*!40000 ALTER TABLE `hitung` ENABLE KEYS */;

-- Dumping structure for table apitexhi_exhibition.kategori
DROP TABLE IF EXISTS `kategori`;
CREATE TABLE IF NOT EXISTS `kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `gambar` varchar(50) DEFAULT NULL,
  `folder` varchar(50) DEFAULT NULL,
  `index_lihat` int(3) DEFAULT '1',
  `flag_aktif` enum('Y','N') DEFAULT 'Y',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(25) DEFAULT NULL,
  `updated_by` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Dumping data for table apitexhi_exhibition.kategori: 4 rows
DELETE FROM `kategori`;
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;
INSERT INTO `kategori` (`id`, `judul`, `keterangan`, `gambar`, `folder`, `index_lihat`, `flag_aktif`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(18, 'MEJA', '', '05082019042200IMG_20140924_130521.jpg', '-', 1, 'Y', '2019-08-05 11:22:00', '2019-08-05 11:22:00', NULL, NULL),
	(17, 'GATE', '', '05082019042102GATE.jpg', '-', 1, 'Y', '2019-08-05 11:21:02', '2019-08-05 11:21:02', NULL, NULL),
	(16, 'BASE DISPLAY', '', '05082019041454BASE MIRING PORTFOLIO ATAU BUKU.jpg', '-', 1, 'Y', '2019-08-05 11:14:54', '2019-08-05 11:14:54', NULL, NULL),
	(15, 'BACKDROP', '-', '050820190411181. SITH ICBP 2009.JPG', '-', 1, 'Y', '2019-08-05 11:11:18', '2019-08-05 11:11:18', NULL, NULL);
/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;

-- Dumping structure for table apitexhi_exhibition.kategori_sub
DROP TABLE IF EXISTS `kategori_sub`;
CREATE TABLE IF NOT EXISTS `kategori_sub` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) DEFAULT NULL,
  `kategori` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `gambar` varchar(100) DEFAULT NULL,
  `folder` varchar(50) DEFAULT NULL,
  `index_lihat` int(11) DEFAULT NULL,
  `flag_aktif` enum('Y','N') DEFAULT 'Y',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table apitexhi_exhibition.kategori_sub: 9 rows
DELETE FROM `kategori_sub`;
/*!40000 ALTER TABLE `kategori_sub` DISABLE KEYS */;
INSERT INTO `kategori_sub` (`id`, `judul`, `kategori`, `keterangan`, `gambar`, `folder`, `index_lihat`, `flag_aktif`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(9, 'tes', 9, '-', '15072019045531backdrop-17-1024x576.jpg', '-', 2, 'Y', '2019-07-15 11:55:31', '2019-08-05 10:55:46', NULL, NULL),
	(8, 'set', 9, '123', '14072019133050backdrop-33.jpg', '-', 1, 'Y', '2019-07-14 20:30:50', '2019-08-05 10:55:46', NULL, NULL),
	(7, '123123213', 9, '123123', '14072019130914backdrop-12-1024x768.jpg', '-', 3, 'Y', '2019-07-14 20:09:14', '2019-08-05 10:55:46', NULL, NULL),
	(10, 'coba', 10, '122', '15072019045531backdrop-17-1024x576.jpg', '-', 1, 'Y', '2019-08-04 05:43:05', '2019-08-04 05:43:17', NULL, NULL),
	(11, 'tes', 15, '123', '12082019032232BACKDROP SBY (2) ACARA.jpg', '-', 1, 'Y', '2019-08-12 10:22:32', '2019-08-12 10:22:32', NULL, NULL),
	(12, '123', 15, '123', '12082019032959BACKDROP SBY (1).jpg', '-', 1, 'Y', '2019-08-12 10:29:59', '2019-08-12 10:29:59', NULL, NULL),
	(13, '213123', 15, '123', '12082019033010BACKDROP SBY (4).jpg', '-', 1, 'Y', '2019-08-12 10:30:10', '2019-08-12 10:30:10', NULL, NULL),
	(14, '123132', 15, '32', '12082019033020BACKDROP SBY (16).jpg', '-', 1, 'Y', '2019-08-12 10:30:20', '2019-08-12 10:30:20', NULL, NULL),
	(15, '123123', 15, '23', '12082019033030BACKDROP SBY (7).jpg', '-', 1, 'Y', '2019-08-12 10:30:30', '2019-08-12 10:30:30', NULL, NULL);
/*!40000 ALTER TABLE `kategori_sub` ENABLE KEYS */;

-- Dumping structure for table apitexhi_exhibition.petugas
DROP TABLE IF EXISTS `petugas`;
CREATE TABLE IF NOT EXISTS `petugas` (
  `kd_petugas` char(4) NOT NULL,
  `nm_petugas` varchar(100) NOT NULL,
  `no_telepon` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(200) NOT NULL,
  `level` varchar(20) NOT NULL DEFAULT 'Kasir',
  `aktif` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  PRIMARY KEY (`kd_petugas`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table apitexhi_exhibition.petugas: 1 rows
DELETE FROM `petugas`;
/*!40000 ALTER TABLE `petugas` DISABLE KEYS */;
INSERT INTO `petugas` (`kd_petugas`, `nm_petugas`, `no_telepon`, `username`, `password`, `level`, `aktif`) VALUES
	('1', 'administrator', '1111', 'admin', '123', 'Admin', 'Yes');
/*!40000 ALTER TABLE `petugas` ENABLE KEYS */;

-- Dumping structure for table apitexhi_exhibition.rumah
DROP TABLE IF EXISTS `rumah`;
CREATE TABLE IF NOT EXISTS `rumah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul_web_header` varchar(200) NOT NULL DEFAULT '-',
  `logo_header` varchar(200) NOT NULL DEFAULT '-',
  `deskripsi_header` text NOT NULL,
  `deskripsi_menu` text NOT NULL,
  `logo_footer` varchar(100) NOT NULL DEFAULT '-',
  `deskripsi_footer` text NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `wa1` varchar(100) NOT NULL DEFAULT '-',
  `wa2` varchar(100) NOT NULL DEFAULT '-',
  `hp1` varchar(100) NOT NULL DEFAULT '-',
  `hp2` varchar(100) NOT NULL DEFAULT '-',
  `email1` varchar(100) NOT NULL DEFAULT '-',
  `email2` varchar(100) NOT NULL DEFAULT '-',
  `fb` varchar(100) NOT NULL DEFAULT '-',
  `ig` varchar(100) NOT NULL DEFAULT '-',
  `tw` varchar(100) NOT NULL DEFAULT '-',
  `logo_about_us` varchar(100) NOT NULL DEFAULT '-',
  `deskripsi_about_us` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table apitexhi_exhibition.rumah: 1 rows
DELETE FROM `rumah`;
/*!40000 ALTER TABLE `rumah` DISABLE KEYS */;
INSERT INTO `rumah` (`id`, `judul_web_header`, `logo_header`, `deskripsi_header`, `deskripsi_menu`, `logo_footer`, `deskripsi_footer`, `alamat`, `wa1`, `wa2`, `hp1`, `hp2`, `email1`, `email2`, `fb`, `ig`, `tw`, `logo_about_us`, `deskripsi_about_us`) VALUES
	(1, 'Apit Exhibition Contractor', 'LOGO.jpg', 'stand pameran &nbsp;&nbsp;&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; booth standar &nbsp;&nbsp;&nbsp;&nbsp;.&nbsp;&nbsp;&nbsp; special design custom booth &nbsp;&nbsp;.&nbsp;&nbsp; base display &nbsp;&nbsp;&nbsp;.&nbsp;&nbsp; backdrop panggung frame screen . sign system . projector . screen . plakat desain grafis . tenda . sofa vip . meja bar . huruf timbul', 'JASA SEWA PERALATAN PAMERAN DAN SEMINAR', 'FOOTER.jpg', 'PERALATAN PAMERAN DAN SEMINAR', 'jl. sukahaji permai II no.3 bandung 40152, jawa barat, indonesia', '081394202000', 'apitexhibition', '081394202000', '081394202001', 'chafidyoeda@gmail.com', 'chafidyoeda@gmail.com', 'apitexhibition', 'apitexhibition', 'apitexhibition', '', 'Kami adalah Perusahaan yang bergerak dalam bidang Jasa Sewa Peralatan Pameran dan Seminar, <br>\r\nPembuatan Display Custom, Museum, Interior, dll. <br> \r\nTim kami terdiri dari Tim Profesional Alumni FSRD ITB <br>  Desain Interior, Desain Komunikasi Visual (DKV), Desain Produk, Seni Murni.  <br>  Kami melayani di seluruh Indonesia terutama di Kota Bandung. \r\n\r\n');
/*!40000 ALTER TABLE `rumah` ENABLE KEYS */;


-- Dumping database structure for dinadbase
DROP DATABASE IF EXISTS `dinadbase`;
CREATE DATABASE IF NOT EXISTS `dinadbase` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `dinadbase`;

-- Dumping structure for table dinadbase.hitung
DROP TABLE IF EXISTS `hitung`;
CREATE TABLE IF NOT EXISTS `hitung` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pengunjung_harian` int(11) NOT NULL DEFAULT '0',
  `pengunjung_bulanan` int(11) NOT NULL DEFAULT '0',
  `pengunjung_sampai_saat_ini` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table dinadbase.hitung: 1 rows
DELETE FROM `hitung`;
/*!40000 ALTER TABLE `hitung` DISABLE KEYS */;
INSERT INTO `hitung` (`id`, `pengunjung_harian`, `pengunjung_bulanan`, `pengunjung_sampai_saat_ini`) VALUES
	(1, 676, 676, 677);
/*!40000 ALTER TABLE `hitung` ENABLE KEYS */;

-- Dumping structure for table dinadbase.kategori
DROP TABLE IF EXISTS `kategori`;
CREATE TABLE IF NOT EXISTS `kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `gambar` varchar(50) DEFAULT NULL,
  `folder` varchar(50) DEFAULT NULL,
  `index_lihat` int(3) DEFAULT '1',
  `flag_aktif` enum('Y','N') DEFAULT 'Y',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(25) DEFAULT NULL,
  `updated_by` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Dumping data for table dinadbase.kategori: 4 rows
DELETE FROM `kategori`;
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;
INSERT INTO `kategori` (`id`, `judul`, `keterangan`, `gambar`, `folder`, `index_lihat`, `flag_aktif`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(18, '1999', '', '05082019042200IMG_20140924_130521.jpg', '-', 1, 'Y', '2019-08-05 11:22:00', '2019-09-17 11:07:09', NULL, NULL),
	(17, '2000', '', '05082019042102GATE.jpg', '-', 1, 'Y', '2019-08-05 11:21:02', '2019-09-17 11:07:12', NULL, NULL),
	(16, '2001', '', '05082019041454BASE MIRING PORTFOLIO ATAU BUKU.jpg', '-', 1, 'Y', '2019-08-05 11:14:54', '2019-09-17 11:07:18', NULL, NULL),
	(15, '2002', '-', '050820190411181. SITH ICBP 2009.JPG', '-', 1, 'Y', '2019-08-05 11:11:18', '2019-09-17 11:07:21', NULL, NULL);
/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;

-- Dumping structure for table dinadbase.kategori_sub
DROP TABLE IF EXISTS `kategori_sub`;
CREATE TABLE IF NOT EXISTS `kategori_sub` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) DEFAULT NULL,
  `kategori` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `gambar` varchar(100) DEFAULT NULL,
  `folder` varchar(50) DEFAULT NULL,
  `media` varchar(150) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `ukuran` varchar(150) DEFAULT NULL,
  `index_lihat` int(11) DEFAULT NULL,
  `flag_aktif` enum('Y','N') DEFAULT 'Y',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table dinadbase.kategori_sub: 9 rows
DELETE FROM `kategori_sub`;
/*!40000 ALTER TABLE `kategori_sub` DISABLE KEYS */;
INSERT INTO `kategori_sub` (`id`, `judul`, `kategori`, `keterangan`, `gambar`, `folder`, `media`, `tahun`, `ukuran`, `index_lihat`, `flag_aktif`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(9, 'Mentari Pagi', 9, 'kehidupan pagi yang indah', '15072019045531backdrop-17-1024x576.jpg', '-', 'canvas', '1990', '200cm x 100cm', 2, 'Y', '2019-07-15 11:55:31', '2019-09-23 10:08:13', NULL, NULL),
	(8, 'Solusi Kehidupan Mental', 9, 'mentari solusi manusia ', '14072019133050backdrop-33.jpg', '-', 'canvas', '1991', '100cm x 50cm', 1, 'Y', '2019-07-14 20:30:50', '2019-09-23 10:08:28', NULL, NULL),
	(7, 'Kuda Lumping ', 9, 'karya pada saat melihat kuda lumping', '14072019130914backdrop-12-1024x768.jpg', '-', 'canvas', '2000', '175cm x 100cm ', 3, 'Y', '2019-07-14 20:09:14', '2019-09-23 10:08:37', NULL, NULL),
	(10, 'Kabah Mendekat', 10, 'keinginan naik haji kembali', '15072019045531backdrop-17-1024x576.jpg', '-', 'canvas', '1995', '250cm x 300cm', 1, 'Y', '2019-08-04 05:43:05', '2019-09-23 10:08:46', NULL, NULL),
	(11, 'Dekat Dengan Saya', 15, 'Kedekatan anak dan orang tua', '12082019032232BACKDROP SBY (2) ACARA.jpg', '-', 'canvas', '1996', '100cm x 100cm', 1, 'Y', '2019-08-12 10:22:32', '2019-09-23 10:09:06', NULL, NULL),
	(12, 'Ujian Agar Dekat Dengan Alloh', 15, 'ujian adalah ujian', '12082019032959BACKDROP SBY (1).jpg', '-', 'canvas', '1997', '125cm x 100cm', 1, 'Y', '2019-08-12 10:29:59', '2019-09-23 10:09:15', NULL, NULL),
	(13, 'Masalah Siapa', 15, 'Siapa yang bermasalah', '12082019033010BACKDROP SBY (4).jpg', '-', 'canvas', '1998', '235cm x 150cm', 1, 'Y', '2019-08-12 10:30:10', '2019-09-23 10:09:24', NULL, NULL),
	(14, 'Lima Puluh Tahun Yang Lalu', 15, '50 tahun yang lalu', '12082019033020BACKDROP SBY (16).jpg', '-', 'canvas', '1999', '100cm x 100cm', 1, 'Y', '2019-08-12 10:30:20', '2019-09-23 10:09:29', NULL, NULL),
	(15, 'Pahala 1000 Bulan', 15, 'Lailatul Qodar', '12082019033030BACKDROP SBY (7).jpg', '-', 'canvas', '2000', '150cm x 150cm', 1, 'Y', '2019-08-12 10:30:30', '2019-09-23 10:09:43', NULL, NULL);
/*!40000 ALTER TABLE `kategori_sub` ENABLE KEYS */;

-- Dumping structure for table dinadbase.petugas
DROP TABLE IF EXISTS `petugas`;
CREATE TABLE IF NOT EXISTS `petugas` (
  `kd_petugas` char(4) NOT NULL,
  `nm_petugas` varchar(100) NOT NULL,
  `no_telepon` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(200) NOT NULL,
  `level` varchar(20) NOT NULL DEFAULT 'Kasir',
  `aktif` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  PRIMARY KEY (`kd_petugas`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table dinadbase.petugas: 1 rows
DELETE FROM `petugas`;
/*!40000 ALTER TABLE `petugas` DISABLE KEYS */;
INSERT INTO `petugas` (`kd_petugas`, `nm_petugas`, `no_telepon`, `username`, `password`, `level`, `aktif`) VALUES
	('1', 'administrator', '1111', 'admin', '123', 'Admin', 'Yes');
/*!40000 ALTER TABLE `petugas` ENABLE KEYS */;

-- Dumping structure for table dinadbase.rumah
DROP TABLE IF EXISTS `rumah`;
CREATE TABLE IF NOT EXISTS `rumah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul_web_header` varchar(200) NOT NULL DEFAULT '-',
  `logo_header` varchar(200) NOT NULL DEFAULT '-',
  `deskripsi_header` text NOT NULL,
  `deskripsi_menu` text NOT NULL,
  `logo_footer` varchar(100) NOT NULL DEFAULT '-',
  `deskripsi_footer` text NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `wa1` varchar(100) NOT NULL DEFAULT '-',
  `wa2` varchar(100) NOT NULL DEFAULT '-',
  `hp1` varchar(100) NOT NULL DEFAULT '-',
  `hp2` varchar(100) NOT NULL DEFAULT '-',
  `email1` varchar(100) NOT NULL DEFAULT '-',
  `email2` varchar(100) NOT NULL DEFAULT '-',
  `fb` varchar(100) NOT NULL DEFAULT '-',
  `ig` varchar(100) NOT NULL DEFAULT '-',
  `tw` varchar(100) NOT NULL DEFAULT '-',
  `logo_about_us` varchar(100) NOT NULL DEFAULT '-',
  `deskripsi_about_us` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table dinadbase.rumah: 1 rows
DELETE FROM `rumah`;
/*!40000 ALTER TABLE `rumah` DISABLE KEYS */;
INSERT INTO `rumah` (`id`, `judul_web_header`, `logo_header`, `deskripsi_header`, `deskripsi_menu`, `logo_footer`, `deskripsi_footer`, `alamat`, `wa1`, `wa2`, `hp1`, `hp2`, `email1`, `email2`, `fb`, `ig`, `tw`, `logo_about_us`, `deskripsi_about_us`) VALUES
	(1, 'Dina Maretta', 'LOGO.jpg', '', '', 'FOOTER.jpg', '', 'jl. sukahaji permai II no.3 bandung 40152, jawa barat, indonesia', '081394202000', 'apitexhibition', '081394202000', '081394202001', 'dinamaretta@gmail.com', 'chafidyoeda@gmail.com', 'apitexhibition', 'apitexhibition', 'apitexhibition', '', 'Kami adalah Perusahaan yang bergerak dalam bidang Jasa Sewa Peralatan Pameran dan Seminar, <br>\r\nPembuatan Display Custom, Museum, Interior, dll. <br> \r\nTim kami terdiri dari Tim Profesional Alumni FSRD ITB <br>  Desain Interior, Desain Komunikasi Visual (DKV), Desain Produk, Seni Murni.  <br>  Kami melayani di seluruh Indonesia terutama di Kota Bandung. \r\n\r\n');
/*!40000 ALTER TABLE `rumah` ENABLE KEYS */;


-- Dumping database structure for joedaart
DROP DATABASE IF EXISTS `joedaart`;
CREATE DATABASE IF NOT EXISTS `joedaart` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `joedaart`;

-- Dumping structure for table joedaart.categories
DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(25) COLLATE utf8_unicode_ci DEFAULT '0',
  `judul` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `judul2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `indeks` tinyint(2) DEFAULT NULL,
  `activeflag` smallint(2) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table joedaart.categories: ~12 rows (approximately)
DELETE FROM `categories`;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `kode`, `judul`, `judul2`, `title`, `slug`, `indeks`, `activeflag`, `image`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(1, 'V2hQ6ckCzlJGlOdYSSGmCQbIQ', 'LUKISAN', '', 'PAINTING', 'painting', 1, 1, '01.jpg', '2019-07-19 09:52:02', '2019-07-19 09:52:03', NULL, NULL),
	(2, 'JxfIIPEyi4m3GRnZ3aK8cM242', 'PATUNG', '', 'SCULPTURE', 'sculpture', 2, 1, '02.png', '2019-07-19 10:02:55', '2019-07-19 10:02:56', NULL, NULL),
	(3, '5BMaVCQM3qbWPYyB36Ej6snM9', 'KERAMIK', '', 'POTTERY', 'pottery', 3, 1, '03.png', '2019-07-19 10:03:52', '2019-07-19 10:03:53', NULL, NULL),
	(4, 'AHdG8qoEpmmV7pHK1yrq0Up7I', 'KERAJINAN', '', 'CRAFT', 'craft', 10, 1, '04.png', '2019-07-19 10:04:28', '2019-07-19 10:04:30', NULL, NULL),
	(5, '0OeXDbC3QiVN0ARKVFaEB0ynE', 'KARYA SENI DIGITAL', '', 'DIGITAL ART', 'digital-art', 12, 1, '05.png', '2019-07-19 10:05:05', '2019-07-19 10:05:06', NULL, NULL),
	(6, 'MsoZaQPbqqSGk1sE5FtXl3inI', 'ELEMEN ESTETIS', '& INSTALASI SENI', 'ARTWORK', 'artwork', 7, 1, '06.png', '2019-07-19 10:06:05', '2019-07-19 10:06:06', NULL, NULL),
	(7, '1231231231231231sdfsafsdf', 'DESAIN INTERIOR', '', 'INTERIOR DESIGN', 'interior-design', 5, 1, '07.jpg', '2019-09-02 17:47:41', '2019-09-02 17:47:42', NULL, NULL),
	(8, 'adsfsafd234234asfdfdsaf', 'DESAIN PRODUK', '', 'PRODUCT DESIGN', 'product', 6, 1, '08.jpg', '2019-09-02 17:48:51', '2019-09-02 17:48:52', NULL, NULL),
	(9, '0asdfasdfsadfsadfdsafadsf', 'KARYA FOTOGRAFI', '', 'PHOTOGRAPHY', 'photography', 11, 1, '09.jpg', '2019-09-02 17:49:45', '2019-09-02 17:49:46', NULL, NULL),
	(10, 'asdfsadfn324234243', 'SENI GRAFIS', '', 'GRAPHIC ART', 'graphic-art', 4, 1, '10.jpg', '2019-09-02 17:52:39', '2019-09-02 17:52:40', NULL, NULL),
	(11, '213213123sdfsadfsdafsdf', 'KRIYA TEXTIL', '', 'FASHION DESIGN', 'textile-kriya', 9, 1, '11.jpg', '2019-09-02 17:53:39', '2019-09-02 17:53:40', NULL, NULL),
	(12, '32423424234', 'SENI PERFORMANCE', '', 'PERFORMANCE ART', 'performance-art', 8, 1, '12.jpg', '2019-09-02 17:54:23', '2019-09-02 17:54:24', NULL, NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table joedaart.countries
DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=190 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table joedaart.countries: ~189 rows (approximately)
DELETE FROM `countries`;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`id`, `name`) VALUES
	(1, 'Indonesia'),
	(2, 'United States (US)'),
	(3, 'United Kingdom (UK)'),
	(4, 'Afghanistan'),
	(5, 'Albania'),
	(6, 'Algeria'),
	(7, 'Andorra'),
	(8, 'Angola'),
	(9, 'Antigua & Barbuda'),
	(10, 'Argentina'),
	(11, 'Armenia'),
	(12, 'Australia'),
	(13, 'Austria'),
	(14, 'Azerbaijan'),
	(15, 'The Bahamas'),
	(16, 'Bahrain'),
	(17, 'Bangladesh'),
	(18, 'Barbados'),
	(19, 'Belarus'),
	(20, 'Belgium'),
	(21, 'Belize'),
	(22, 'Benin'),
	(23, 'Bhutan'),
	(24, 'Bolivia'),
	(25, 'Bosnia & Herzegovina'),
	(26, 'Botswana'),
	(27, 'Brazil'),
	(28, 'Brunei'),
	(29, 'Bulgaria'),
	(30, 'Burkina Faso'),
	(31, 'Burundi'),
	(32, 'Cambodia'),
	(33, 'Cameroon'),
	(34, 'Canada'),
	(35, 'Cape Verde'),
	(36, 'Central African Republic'),
	(37, 'Chad'),
	(38, 'Chile'),
	(39, 'China'),
	(40, 'Colombia'),
	(41, 'Comoros'),
	(42, 'Republic of the Congo'),
	(43, 'Costa Rica'),
	(44, 'Ivory Coast'),
	(45, 'Croatia'),
	(46, 'Cuba'),
	(47, 'Cyprus'),
	(48, 'Czech Republic'),
	(49, 'Denmark'),
	(50, 'Djibouti'),
	(51, 'Dominica'),
	(52, 'Dominican Republic'),
	(53, 'East Timor'),
	(54, 'Ecuador'),
	(55, 'Egypt'),
	(56, 'El Salvador'),
	(57, 'Equatorial Guinea'),
	(58, 'Eritrea'),
	(59, 'Estonia'),
	(60, 'Ethiopia'),
	(61, 'Fiji'),
	(62, 'Finland'),
	(63, 'France'),
	(64, 'Gabon'),
	(65, 'The Gambia'),
	(66, 'Georgia'),
	(67, 'Germany'),
	(68, 'Ghana'),
	(69, 'Greece'),
	(70, 'Grenada'),
	(71, 'Guatemala'),
	(72, 'Guinea'),
	(73, 'Guinea Bissau'),
	(74, 'Guyana'),
	(75, 'Haiti'),
	(76, 'Honduras'),
	(77, 'Hungary'),
	(78, 'Iceland'),
	(79, 'India'),
	(80, 'Iran'),
	(81, 'Iraq'),
	(82, 'Ireland'),
	(83, 'Israel'),
	(84, 'Italy'),
	(85, 'Jamaica'),
	(86, 'Japan'),
	(87, 'Jordan'),
	(88, 'Kazakhstan'),
	(89, 'Kenya'),
	(90, 'Kiribati'),
	(91, 'Korea, North'),
	(92, 'Korea, South'),
	(93, 'Kuwait'),
	(94, 'Kyrgyzstan'),
	(95, 'Laos'),
	(96, 'Latvia'),
	(97, 'Lebanon'),
	(98, 'Libya'),
	(99, 'Liechtenstein'),
	(100, 'Luxembourg'),
	(101, 'Macedonia'),
	(102, 'Madagascar'),
	(103, 'Malawi'),
	(104, 'Malaysia'),
	(105, 'Maldives'),
	(106, 'Mali'),
	(107, 'Malta'),
	(108, 'Marshall Islands'),
	(109, 'Mauritania'),
	(110, 'Mauritius'),
	(111, 'Mexico'),
	(112, 'Federated States of Micronesia'),
	(113, 'Moldova'),
	(114, 'Monaco'),
	(115, 'Mongolia'),
	(116, 'Montenegro'),
	(117, 'Morocco'),
	(118, 'Mozambique'),
	(119, 'Myanmar'),
	(120, 'Namibia'),
	(121, 'Nauru'),
	(122, 'Nepal'),
	(123, 'Netherlands'),
	(124, 'New Zealand'),
	(125, 'Nicaragua'),
	(126, 'Niger'),
	(127, 'Nigeria'),
	(128, 'Norway'),
	(129, 'Oman'),
	(130, 'Pakistan'),
	(131, 'Palau'),
	(132, 'Panama'),
	(133, 'Papua New Guinea'),
	(134, 'Paraguay'),
	(135, 'Peru'),
	(136, 'Philippines'),
	(137, 'Poland'),
	(138, 'Portugal'),
	(139, 'Qatar'),
	(140, 'Romania'),
	(141, 'Russia'),
	(142, 'Rwanda'),
	(143, 'Saint Kitts and Nevis'),
	(144, 'Saint Lucia'),
	(145, 'Saint Vincent and the Grenadines'),
	(146, 'Samoa'),
	(147, 'San Marino'),
	(148, 'Sao Tome and Principe'),
	(149, 'Saudi Arabia'),
	(150, 'Senegal'),
	(151, 'Serbia'),
	(152, 'Seychelles'),
	(153, 'Sierra Leone'),
	(154, 'Singapore'),
	(155, 'Slovakia'),
	(156, 'Slovenia'),
	(157, 'Solomon Islands'),
	(158, 'Somalia'),
	(159, 'South Africa'),
	(160, 'Spain'),
	(161, 'Sri Lanka'),
	(162, 'Sudan'),
	(163, 'Suriname'),
	(164, 'Swaziland'),
	(165, 'Sweden'),
	(166, 'Switzerland'),
	(167, 'Syria'),
	(168, 'Tajikistan'),
	(169, 'Tanzania'),
	(170, 'Thailand'),
	(171, 'Togo'),
	(172, 'Tonga'),
	(173, 'Trinidad and Tobago'),
	(174, 'Tunisia'),
	(175, 'Turkey'),
	(176, 'Turkmenistan'),
	(177, 'Tuvalu'),
	(178, 'Uganda'),
	(179, 'Ukraine'),
	(180, 'United Arab Emirates'),
	(181, 'Uruguay'),
	(182, 'Uzbekistan'),
	(183, 'Vanuatu'),
	(184, 'Vatican City'),
	(185, 'Venezuela'),
	(186, 'Vietnam'),
	(187, 'Yemen'),
	(188, 'Zambia'),
	(189, 'Zimbabwe');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Dumping structure for table joedaart.favorites
DROP TABLE IF EXISTS `favorites`;
CREATE TABLE IF NOT EXISTS `favorites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` char(1) COLLATE utf8_unicode_ci DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table joedaart.favorites: ~1 rows (approximately)
DELETE FROM `favorites`;
/*!40000 ALTER TABLE `favorites` DISABLE KEYS */;
INSERT INTO `favorites` (`id`, `kode`, `product`, `user`, `flag`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(1, '123132', '2', '4', '1', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `favorites` ENABLE KEYS */;

-- Dumping structure for table joedaart.images
DROP TABLE IF EXISTS `images`;
CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_order` int(5) DEFAULT '0',
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('1','0') COLLATE utf8_unicode_ci DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=201 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table joedaart.images: 23 rows
DELETE FROM `images`;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` (`id`, `kode`, `product`, `img_name`, `img_order`, `created`, `modified`, `status`) VALUES
	(197, 'p0tYlx9ExW9pstLJbXexibXAI', 'XHMtTdxC6z9uzKMxFYa0cJOjd', '17921568341432.jpg', 1, '2019-09-13 09:23:52', '2019-09-13 09:23:52', '1'),
	(198, 'cGt2I1D2JtYkeziiXc1Jwvzse', 'S3g9M9fzV4sy5MN0fRk5PF6Vw', '15121568343743.jpg', 1, '2019-09-13 10:02:23', '2019-09-13 10:02:23', '1'),
	(199, 'M465Lggl7HxQh8pdvDqVHyrjP', 'Pdf21mSqNvB3QB6xq5aTEuj5w', '12731572189753.png', 1, '2019-10-27 22:22:33', '2019-10-27 22:22:33', '1'),
	(200, 'Ige7Eb80QQZBUZd7yItlJfGfI', '89A0D7yFeED8neMQ9M1QCH2bV', '12181572190476.jpg', 1, '2019-10-27 22:34:36', '2019-10-27 22:34:36', '1'),
	(195, 'icSciaD40GAw2SQdzQfkGOSTr', 'ZC4QhAmg4sNzOZTa3QHmSvZlm', '1631567478271.jpg', 1, '2019-09-03 09:37:51', '2019-09-03 09:37:51', '1'),
	(196, 'ibmoKZza9OeSSXBG729icAxnb', 'cDrE6ON6k9HqBbyLbr0YioWUw', '12671568336029.jpg', 1, '2019-09-13 07:53:49', '2019-09-13 07:53:49', '1'),
	(194, 'qbRJb90ZcYx6lpwFAah2k5wRF', 'SNiQErPU510EPZCZMibae989t', '17031567478227.jpg', 1, '2019-09-03 09:37:07', '2019-09-03 09:37:07', '1'),
	(193, '3kzwlRvy3ztDhL3ETT7Cpa9ho', '3h7qMHTAKEAdlG0z69MiG6X2x', '1331567478173.jpg', 1, '2019-09-03 09:36:13', '2019-09-03 09:36:13', '1'),
	(188, 'D0b9IKETy1ayZXqu4mh90h8QB', 'Vw9P9Ul4NwOXb7L3Oi3LO1ETN', '12871567463211.jpg', 1, '2019-09-03 05:26:52', '2019-09-03 05:26:52', '1'),
	(189, 'oDIF0UW6RHhxljqC6NRexaxEe', 'MNhnQrqUqAOihTbyqlc2Uq5wc', '13581567463579.jpg', 1, '2019-09-03 05:32:59', '2019-09-03 05:32:59', '1'),
	(190, '5XBYeUscJfhc5OlyTrNBlGxdN', 'EJ8IAmiSLASX2YIR4x9gVbVvn', '13141567477506.jpg', 1, '2019-09-03 09:25:06', '2019-09-03 09:25:06', '1'),
	(191, 'kwtVHxOuqCBpiC1g6g2L4y1u1', '9u5pGhh5w7XdWrv1XykgZb8Dk', '13931567477562.jpg', 1, '2019-09-03 09:26:02', '2019-09-03 09:26:02', '1'),
	(192, 'OYjrxLn6aYHg4GLG2LSfZunYX', '7tTAQibtS17FQM8K2Bate0tAQ', '12541567477627.jpg', 1, '2019-09-03 09:27:07', '2019-09-03 09:27:07', '1'),
	(187, 'QmnqM2RMFk5YtOV7n756Ca4Se', 'mujXVouyp9x0nhnPEqknasfHx', '13011567431033.jpg', 1, '2019-09-02 20:30:33', '2019-09-02 20:30:33', '1'),
	(185, 'Q6cyt3awjshW7XxoXG5b5pDL6', 'ILtxhAWqJkZMQZE9Y3RKDE224', '18161565747293.jpg', 1, '2019-08-14 08:48:13', '2019-08-14 08:48:13', '1'),
	(186, '25lvCtjyAaOuLFpxE1XijE7tt', 'gcfXQF0eEmr0GCerP5uofQoGE', '11681567430707.jpg', 1, '2019-09-02 20:25:07', '2019-09-02 20:25:07', '1'),
	(178, 'T6BoL6xGtsZYfu4DN9g86hYVV', 'K2mLkGwtRKynFbyhfcfFK6fNL', '12241565705300.jpg', 1, '2019-08-13 21:08:20', '2019-08-23 11:18:20', '1'),
	(179, 'T6BoL6xGtsZYfu4DN9g86hYVV', 'K2mLkGwtRKynFbyhfcfFK6fNL', '29741565705300.jpg', 6, '2019-08-13 21:08:20', '2019-08-23 11:18:20', '1'),
	(180, 'T6BoL6xGtsZYfu4DN9g86hYVV', 'K2mLkGwtRKynFbyhfcfFK6fNL', '31491565705300.jpg', 2, '2019-08-13 21:08:20', '2019-08-23 11:18:20', '1'),
	(181, 'T6BoL6xGtsZYfu4DN9g86hYVV', 'K2mLkGwtRKynFbyhfcfFK6fNL', '49251565705300.jpg', 5, '2019-08-13 21:08:20', '2019-08-23 11:18:20', '1'),
	(182, 'T6BoL6xGtsZYfu4DN9g86hYVV', 'K2mLkGwtRKynFbyhfcfFK6fNL', '5391565705300.jpg', 4, '2019-08-13 21:08:20', '2019-08-23 11:18:20', '1'),
	(183, 'T6BoL6xGtsZYfu4DN9g86hYVV', 'K2mLkGwtRKynFbyhfcfFK6fNL', '65551565705300.jpg', 3, '2019-08-13 21:08:20', '2019-08-23 11:18:20', '1'),
	(184, 'sQluV8q3aWoEvvw1gIq0tLrUh', 'QSLOW6pwqNvXPYttfk7P287Pc', '18391565744014.jpg', 1, '2019-08-14 07:53:34', '2019-08-14 07:53:34', '1');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;

-- Dumping structure for table joedaart.menu
DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `roles` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activeflag` smallint(2) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table joedaart.menu: ~0 rows (approximately)
DELETE FROM `menu`;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;

-- Dumping structure for table joedaart.products
DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(25) COLLATE utf8_unicode_ci DEFAULT '0',
  `title` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sellerid` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `artist` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sellername` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` smallint(4) DEFAULT NULL,
  `categorychar` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descs` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `discount` decimal(3,0) DEFAULT NULL,
  `negotiable` char(2) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `galery` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `folder` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sms` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wa` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `media` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tahun` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ukuran` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uploaddate` timestamp NULL DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `province` int(11) DEFAULT NULL,
  `city` int(11) DEFAULT NULL,
  `kodeuser` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(1) DEFAULT '1',
  `status_permanen` smallint(1) DEFAULT '1',
  `activeflag` char(2) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table joedaart.products: ~17 rows (approximately)
DELETE FROM `products`;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `kode`, `title`, `sellerid`, `artist`, `sellername`, `category`, `categorychar`, `slug`, `descs`, `price`, `discount`, `negotiable`, `galery`, `phone`, `address`, `image`, `folder`, `sms`, `wa`, `media`, `tahun`, `ukuran`, `email`, `uploaddate`, `country`, `province`, `city`, `kodeuser`, `status`, `status_permanen`, `activeflag`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(52, 'K2mLkGwtRKynFbyhfcfFK6fNL', 'Senandung Nacita', '1DWYH470pJ4OJVJiPXSgoLB1s', 'Artist Test', '-', 1, '-', '-', 'Deskripsi tentang kehidupan cinta seseorang perempuan yang ditinggal kekasih pada saat tertimpa masalah masalah masalah masalah', 100000, 10, 'Y', NULL, NULL, NULL, 'Brimstone.JPG', '-', NULL, NULL, 'Canvas', '2000', '150cm x 150cm', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Y', '2019-08-13 21:08:20', '2019-09-12 09:56:33', '', ''),
	(53, 'QSLOW6pwqNvXPYttfk7P287Pc', 'title test1', '1DWYH470pJ4OJVJiPXSgoLB1s', 'Artist Test', '-', 2, '-', '-', 'artis description', 1000, 0, 'Y', NULL, NULL, NULL, 'Brimstone.JPG', '-', NULL, NULL, 'tanah liat', '2000', '100cm x 100cm', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Y', '2019-08-14 07:53:34', '2019-09-03 08:44:35', '', ''),
	(54, 'gcfXQF0eEmr0GCerP5uofQoGE', 'Tekstilku', '1DWYH470pJ4OJVJiPXSgoLB1s', 'Artist Test', '-', 11, '-', '-', 'Keaneka ragaman tekstis', 1000000, 0, 'Y', NULL, NULL, NULL, 'Brimstone.JPG', '-', NULL, NULL, 'tekstil', '2000', '100 x 100cm', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Y', '2019-09-02 20:25:07', '2019-09-03 08:44:38', '', ''),
	(55, 'mujXVouyp9x0nhnPEqknasfHx', 'Elang Buntul', '1DWYH470pJ4OJVJiPXSgoLB1s', 'Artist Test', '-', 4, '-', '-', 'Keindahan alam indonesia', 25000000, 0, 'Y', NULL, NULL, NULL, 'Brimstone.JPG', '-', NULL, NULL, 'Kain', '2000', '200x150cm', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Y', '2019-09-02 20:30:33', '2019-09-03 08:44:43', '', ''),
	(56, 'Vw9P9Ul4NwOXb7L3Oi3LO1ETN', 'Sky Blue', '1DWYH470pJ4OJVJiPXSgoLB1s', 'Artist Test', '-', 2, '-', '-', 'Corak Bangsa Tertuang di Patung', 34000000, 0, 'Y', NULL, NULL, NULL, 'Brimstone.JPG', '-', NULL, NULL, 'Tanah Liat', '1998', '200x300cm', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Y', '2019-09-03 05:26:51', '2019-09-03 08:44:40', '', ''),
	(57, 'MNhnQrqUqAOihTbyqlc2Uq5wc', 'Perjalanan', '1DWYH470pJ4OJVJiPXSgoLB1s', 'Artist Test', '-', 5, '-', '-', 'Mengenang perjalanan bangsa revolusi', 1000000, 0, 'Y', NULL, NULL, NULL, 'Brimstone.JPG', '-', NULL, NULL, 'Photo', '2000', '200cmx180cm', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Y', '2019-09-03 05:32:59', '2019-09-03 08:44:42', '', ''),
	(58, 'EJ8IAmiSLASX2YIR4x9gVbVvn', 'Alam Pegunungan', '1DWYH470pJ4OJVJiPXSgoLB1s', 'Artist Test', '-', 5, '-', '-', 'Semua Tentang Alam', 0, 0, 'Y', NULL, NULL, NULL, 'Brimstone.JPG', '-', NULL, NULL, 'Alami', '2000', '100 cm x 100 cm', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Y', '2019-09-03 09:25:06', '2019-09-03 09:27:28', '', ''),
	(59, '9u5pGhh5w7XdWrv1XykgZb8Dk', 'Simpati', '1DWYH470pJ4OJVJiPXSgoLB1s', 'Artist Test', '-', 9, '-', '-', 'Lihat kedunia simpati', 2000000, 0, 'Y', NULL, NULL, NULL, 'Brimstone.JPG', '-', NULL, NULL, 'Frame', '1975', '74 cm x 100 cm', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Y', '2019-09-03 09:26:02', '2019-09-03 09:27:26', '', ''),
	(60, '7tTAQibtS17FQM8K2Bate0tAQ', 'Sumbawa Lima', '1DWYH470pJ4OJVJiPXSgoLB1s', 'Artist Test', '-', 11, '-', '-', 'Sumbawa lima enam tujuh', 50000000, 0, 'Y', NULL, NULL, NULL, 'Brimstone.JPG', '-', NULL, NULL, 'Kain', '1950', '300 cm x 200 cm', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Y', '2019-09-03 09:27:07', '2019-09-03 09:27:24', '', ''),
	(61, '3h7qMHTAKEAdlG0z69MiG6X2x', 'Good Question', '1DWYH470pJ4OJVJiPXSgoLB1s', 'Artist Test', '-', 1, '-', '-', 'Good question about a life', 1000000, 0, 'Y', NULL, NULL, NULL, 'Brimstone.JPG', '-', NULL, NULL, 'Canvas', '2000', '100cm x 100cm', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Y', '2019-09-03 09:36:13', '2019-09-03 09:38:06', '', ''),
	(62, 'SNiQErPU510EPZCZMibae989t', 'Aromatic', '1DWYH470pJ4OJVJiPXSgoLB1s', 'Artist Test', '-', 7, '-', '-', 'Deskripsi tentang kehidupan cinta seseorang perempuan yang ditinggal kekasih pada saat tertimpa masalah', 2000000, 0, 'Y', NULL, NULL, NULL, 'Brimstone.JPG', '-', NULL, NULL, 'Media', '1975', '200cm x 100cm', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Y', '2019-09-03 09:37:07', '2019-09-12 09:39:27', '', ''),
	(63, 'ZC4QhAmg4sNzOZTa3QHmSvZlm', 'Mobil', '1DWYH470pJ4OJVJiPXSgoLB1s', 'Artist Test', '-', 4, '-', '-', 'Description more description', 2000000, 0, 'Y', NULL, NULL, NULL, 'Brimstone.JPG', '-', NULL, NULL, 'Media', '2000', '100 cm x 100 cm', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Y', '2019-09-03 09:37:51', '2019-09-03 09:38:04', '', ''),
	(64, 'cDrE6ON6k9HqBbyLbr0YioWUw', 'art title 1', 's4barqnOdx3lTYgli6KRpZtt9', 'artis test 1', '-', 1, '-', '-', 'description 1', 1000000, 0, 'Y', NULL, NULL, NULL, NULL, '-', NULL, NULL, 'media 1', '2000', 'size 1', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 'Y', '2019-09-13 07:53:48', '2019-09-17 22:09:33', '', ''),
	(65, 'XHMtTdxC6z9uzKMxFYa0cJOjd', '123', 's4barqnOdx3lTYgli6KRpZtt9', '123', '-', 3, '-', '-', '123', 123, 0, 'Y', NULL, NULL, NULL, NULL, '-', NULL, NULL, '132', '213', '123', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'Y', '2019-09-13 09:23:51', '2019-09-17 05:32:29', '', ''),
	(66, 'S3g9M9fzV4sy5MN0fRk5PF6Vw', '432432', 's4barqnOdx3lTYgli6KRpZtt9', '234432', '-', 4, '-', '-', '324432', 20000, 0, 'Y', NULL, NULL, NULL, NULL, '-', NULL, NULL, '432432', '2344', '432234', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'Y', '2019-09-13 10:02:23', '2019-09-17 05:32:24', '', ''),
	(67, 'Pdf21mSqNvB3QB6xq5aTEuj5w', 'art title test', 'enGkSamJzp8ZK35SBoDr0G5hI', 'artis test', '-', 1, '-', '-', '123', 123, 0, 'Y', NULL, NULL, NULL, NULL, '-', NULL, NULL, 'canvas', '123', '123 x 123 ', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Y', '2019-10-27 22:22:32', '2019-10-27 22:22:32', '', ''),
	(68, '89A0D7yFeED8neMQ9M1QCH2bV', 'art title', 'enGkSamJzp8ZK35SBoDr0G5hI', 'aartis', '-', 1, '-', '-', 'descrpiton', 1000000, 0, 'Y', NULL, NULL, NULL, NULL, '-', NULL, NULL, 'canvas', '200', '100', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Y', '2019-10-27 22:34:36', '2019-10-27 22:34:36', '', '');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Dumping structure for table joedaart.provinces
DROP TABLE IF EXISTS `provinces`;
CREATE TABLE IF NOT EXISTS `provinces` (
  `id` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `countryid` int(3) DEFAULT NULL,
  `sort` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table joedaart.provinces: ~35 rows (approximately)
DELETE FROM `provinces`;
/*!40000 ALTER TABLE `provinces` DISABLE KEYS */;
INSERT INTO `provinces` (`id`, `name`, `countryid`, `sort`) VALUES
	('10', 'All Provinces', 1, 1),
	('11', 'Aceh D.I.', 1, 2),
	('12', 'Sumatera Utara', 1, 34),
	('13', 'Sumatera Barat', 1, 32),
	('14', 'Riau', 1, 26),
	('15', 'Jambi', 1, 8),
	('16', 'Sumatera Selatan', 1, 33),
	('17', 'Bengkulu', 1, 5),
	('18', 'Lampung', 1, 19),
	('19', 'Kepulauan Bangka Belitung', 1, 17),
	('21', 'Kepulauan Riau', 1, 18),
	('31', 'Dki Jakarta', 1, 6),
	('32', 'Jawa Barat', 1, 9),
	('33', 'Jawa Tengah', 1, 10),
	('34', 'Yogyakarta D.I.', 1, 35),
	('35', 'Jawa Timur', 1, 11),
	('36', 'Banten', 1, 4),
	('51', 'Bali', 1, 3),
	('52', 'Nusa Tenggara Barat', 1, 22),
	('53', 'Nusa Tenggara Timur', 1, 23),
	('61', 'Kalimantan Barat', 1, 12),
	('62', 'Kalimantan Tengah', 1, 14),
	('63', 'Kalimantan Selatan', 1, 13),
	('64', 'Kalimantan Timur', 1, 15),
	('65', 'Kalimantan Utara', 1, 16),
	('71', 'Sulawesi Utara', 1, 31),
	('72', 'Sulawesi Tengah', 1, 29),
	('73', 'Sulawesi Selatan', 1, 28),
	('74', 'Sulawesi Tenggara', 1, 30),
	('75', 'Gorontalo', 1, 7),
	('76', 'Sulawesi Barat', 1, 27),
	('81', 'Maluku', 1, 20),
	('82', 'Maluku Utara', 1, 21),
	('91', 'Papua Barat', 1, 25),
	('94', 'Papua', 1, 24);
/*!40000 ALTER TABLE `provinces` ENABLE KEYS */;

-- Dumping structure for table joedaart.regencies
DROP TABLE IF EXISTS `regencies`;
CREATE TABLE IF NOT EXISTS `regencies` (
  `id` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `province_id` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `regencies_province_id_index` (`province_id`),
  CONSTRAINT `regencies_province_id_foreign` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table joedaart.regencies: ~546 rows (approximately)
DELETE FROM `regencies`;
/*!40000 ALTER TABLE `regencies` DISABLE KEYS */;
INSERT INTO `regencies` (`id`, `province_id`, `name`, `sort`) VALUES
	('1100', '11', 'All City', 1),
	('1101', '11', 'Simeulue Kab.', 2),
	('1102', '11', 'Aceh Singkil Kab.', 3),
	('1103', '11', 'Aceh Selatan Kab.', 4),
	('1104', '11', 'Aceh Tenggara Kab.', 5),
	('1105', '11', 'Aceh Timur Kab.', 6),
	('1106', '11', 'Aceh Tengah Kab.', 7),
	('1107', '11', 'Aceh Barat Kab.', 8),
	('1108', '11', 'Aceh Besar Kab.', 9),
	('1109', '11', 'Pidie Kab.', 10),
	('1110', '11', 'Bireuen Kab.', 11),
	('1111', '11', 'Aceh Utara Kab.', 12),
	('1112', '11', 'Aceh Barat Daya Kab.', 13),
	('1113', '11', 'Gayo Lues Kab.', 14),
	('1114', '11', 'Aceh Tamiang Kab.', 15),
	('1115', '11', 'Nagan Raya Kab.', 16),
	('1116', '11', 'Aceh Jaya Kab.', 17),
	('1117', '11', 'Bener Meriah Kab.', 18),
	('1118', '11', 'Pidie Jaya Kab.', 19),
	('1171', '11', 'Banda Aceh Kota', 20),
	('1172', '11', 'Sabang Kota', 21),
	('1173', '11', 'Langsa Kota', 22),
	('1174', '11', 'Lhokseumawe Kota', 23),
	('1175', '11', 'Subulussalam Kota', 24),
	('1200', '12', 'All City', 1),
	('1201', '12', 'Nias Kab.', 2),
	('1202', '12', 'Mandailing Natal Kab.', 3),
	('1203', '12', 'Tapanuli Selatan Kab.', 4),
	('1204', '12', 'Tapanuli Tengah Kab.', 5),
	('1205', '12', 'Tapanuli Utara Kab.', 6),
	('1206', '12', 'Toba Samosir Kab.', 7),
	('1207', '12', 'Labuhan Batu Kab.', 8),
	('1208', '12', 'Asahan Kab.', 9),
	('1209', '12', 'Simalungun Kab.', 10),
	('1210', '12', 'Dairi Kab.', 11),
	('1211', '12', 'Karo Kab.', 12),
	('1212', '12', 'Deli Serdang Kab.', 13),
	('1213', '12', 'Langkat Kab.', 14),
	('1214', '12', 'Nias Selatan Kab.', 15),
	('1215', '12', 'Humbang Hasundutan Kab.', 16),
	('1216', '12', 'Pakpak Bharat Kab.', 17),
	('1217', '12', 'Samosir Kab.', 18),
	('1218', '12', 'Serdang Bedagai Kab.', 19),
	('1219', '12', 'Batu Bara Kab.', 20),
	('1220', '12', 'Padang Lawas Utara Kab.', 21),
	('1221', '12', 'Padang Lawas Kab.', 22),
	('1222', '12', 'Labuhan Batu Selatan Kab.', 23),
	('1223', '12', 'Labuhan Batu Utara Kab.', 24),
	('1224', '12', 'Nias Utara Kab.', 25),
	('1225', '12', 'Nias Barat Kab.', 26),
	('1271', '12', 'Sibolga Kota', 27),
	('1272', '12', 'Tanjung Balai Kota', 28),
	('1273', '12', 'Pematang Siantar Kota', 29),
	('1274', '12', 'Tebing Tinggi Kota', 30),
	('1275', '12', 'Medan Kota', 31),
	('1276', '12', 'Binjai Kota', 32),
	('1277', '12', 'Padangsidimpuan Kota', 33),
	('1278', '12', 'Gunungsitoli Kota', 34),
	('1300', '13', 'All City', 1),
	('1301', '13', 'Kepulauan Mentawai Kab.', 2),
	('1302', '13', 'Pesisir Selatan Kab.', 3),
	('1303', '13', 'Solok Kab.', 4),
	('1304', '13', 'Sijunjung Kab.', 5),
	('1305', '13', 'Tanah Datar Kab.', 6),
	('1306', '13', 'Padang Pariaman Kab.', 7),
	('1307', '13', 'Agam Kab.', 8),
	('1308', '13', 'Lima Puluh Kota Kab.', 9),
	('1309', '13', 'Pasaman Kab.', 10),
	('1310', '13', 'Solok Selatan Kab.', 11),
	('1311', '13', 'Dharmasraya Kab.', 12),
	('1312', '13', 'Pasaman Barat Kab.', 13),
	('1371', '13', 'Padang Kota', 14),
	('1372', '13', 'Solok Kota', 15),
	('1373', '13', 'Sawah Lunto Kota', 16),
	('1374', '13', 'Padang Panjang Kota', 17),
	('1375', '13', 'Bukittinggi Kota', 18),
	('1376', '13', 'Payakumbuh Kota', 19),
	('1377', '13', 'Pariaman Kota', 20),
	('1400', '14', 'All City', 1),
	('1401', '14', 'Kuantan Singingi Kab.', 2),
	('1402', '14', 'Indragiri Hulu Kab.', 3),
	('1403', '14', 'Indragiri Hilir Kab.', 4),
	('1404', '14', 'Pelalawan Kab.', 5),
	('1405', '14', 'Siak Kab.', 6),
	('1406', '14', 'Kampar Kab.', 7),
	('1407', '14', 'Rokan Hulu Kab.', 8),
	('1408', '14', 'Bengkalis Kab.', 9),
	('1409', '14', 'Rokan Hilir Kab.', 10),
	('1410', '14', 'Kepulauan Meranti Kab.', 11),
	('1471', '14', 'Pekanbaru Kota', 12),
	('1473', '14', 'Dumai Kota', 13),
	('1500', '15', 'All City', 1),
	('1501', '15', 'Kerinci Kab.', 2),
	('1502', '15', 'Merangin Kab.', 3),
	('1503', '15', 'Sarolangun Kab.', 4),
	('1504', '15', 'Batang Hari Kab.', 5),
	('1505', '15', 'Muaro Jambi Kab.', 6),
	('1506', '15', 'Tanjung Jabung Timur Kab.', 7),
	('1507', '15', 'Tanjung Jabung Barat Kab.', 8),
	('1508', '15', 'Tebo Kab.', 9),
	('1509', '15', 'Bungo Kab.', 10),
	('1571', '15', 'Jambi Kota', 11),
	('1572', '15', 'Sungai Penuh Kota', 12),
	('1600', '16', 'All City', 1),
	('1601', '16', 'Ogan Komering Ulu Kab.', 2),
	('1602', '16', 'Ogan Komering Ilir Kab.', 3),
	('1603', '16', 'Muara Enim Kab.', 4),
	('1604', '16', 'Lahat Kab.', 5),
	('1605', '16', 'Musi Rawas Kab.', 6),
	('1606', '16', 'Musi Banyuasin Kab.', 7),
	('1607', '16', 'Banyu Asin Kab.', 8),
	('1608', '16', 'Ogan Komering Ulu Selatan Kab.', 9),
	('1609', '16', 'Ogan Komering Ulu Timur Kab.', 10),
	('1610', '16', 'Ogan Ilir Kab.', 11),
	('1611', '16', 'Empat Lawang Kab.', 12),
	('1612', '16', 'Penukal Abab Lematang Ilir Kab.', 13),
	('1613', '16', 'Musi Rawas Utara Kab.', 14),
	('1671', '16', 'Palembang Kota', 15),
	('1672', '16', 'Prabumulih Kota', 16),
	('1673', '16', 'Pagar Alam Kota', 17),
	('1674', '16', 'Lubuklinggau Kota', 18),
	('1700', '17', 'All City', 1),
	('1701', '17', 'Bengkulu Selatan Kab.', 2),
	('1702', '17', 'Rejang Lebong Kab.', 3),
	('1703', '17', 'Bengkulu Utara Kab.', 4),
	('1704', '17', 'Kaur Kab.', 5),
	('1705', '17', 'Seluma Kab.', 6),
	('1706', '17', 'Mukomuko Kab.', 7),
	('1707', '17', 'Lebong Kab.', 8),
	('1708', '17', 'Kepahiang Kab.', 9),
	('1709', '17', 'Bengkulu Tengah Kab.', 10),
	('1771', '17', 'Bengkulu Kota', 11),
	('1800', '18', 'All City', 1),
	('1801', '18', 'Lampung Barat Kab.', 2),
	('1802', '18', 'Tanggamus Kab.', 3),
	('1803', '18', 'Lampung Selatan Kab.', 4),
	('1804', '18', 'Lampung Timur Kab.', 5),
	('1805', '18', 'Lampung Tengah Kab.', 6),
	('1806', '18', 'Lampung Utara Kab.', 7),
	('1807', '18', 'Way Kanan Kab.', 8),
	('1808', '18', 'Tulangbawang Kab.', 9),
	('1809', '18', 'Pesawaran Kab.', 10),
	('1810', '18', 'Pringsewu Kab.', 11),
	('1811', '18', 'Mesuji Kab.', 12),
	('1812', '18', 'Tulang Bawang Barat Kab.', 13),
	('1813', '18', 'Pesisir Barat Kab.', 14),
	('1871', '18', 'Bandar Lampung Kota', 15),
	('1872', '18', 'Metro Kota', 16),
	('1900', '19', 'All City', 1),
	('1901', '19', 'Bangka Kab.', 2),
	('1902', '19', 'Belitung Kab.', 3),
	('1903', '19', 'Bangka Barat Kab.', 4),
	('1904', '19', 'Bangka Tengah Kab.', 5),
	('1905', '19', 'Bangka Selatan Kab.', 6),
	('1906', '19', 'Belitung Timur Kab.', 7),
	('1971', '19', 'Pangkal Pinang Kota', 8),
	('2100', '21', 'All City', 1),
	('2101', '21', 'Karimun Kab.', 2),
	('2102', '21', 'Bintan Kab.', 3),
	('2103', '21', 'Natuna Kab.', 4),
	('2104', '21', 'Lingga Kab.', 5),
	('2105', '21', 'Kepulauan Anambas Kab.', 6),
	('2171', '21', 'Batam Kota', 7),
	('2172', '21', 'Tanjung Pinang Kota', 8),
	('3101', '31', 'Kepulauan Seribu Kab.', 2),
	('3170', '31', 'All City', 1),
	('3171', '31', 'Jakarta Selatan Kota', 3),
	('3172', '31', 'Jakarta Timur Kota', 4),
	('3173', '31', 'Jakarta Pusat Kota', 5),
	('3174', '31', 'Jakarta Barat Kota', 6),
	('3175', '31', 'Jakarta Utara Kota', 7),
	('3200', '32', 'All City', 1),
	('3201', '32', 'Bogor Kab.', 8),
	('3202', '32', 'Sukabumi Kab. ', 24),
	('3203', '32', 'Cianjur Kab.', 11),
	('3204', '32', 'Bandung Kab.', 4),
	('3205', '32', 'Garut Kab.', 16),
	('3206', '32', 'Tasikmalaya Kab.', 27),
	('3207', '32', 'Ciamis Kab.', 10),
	('3208', '32', 'Kuningan Kab.', 19),
	('3209', '32', 'Cirebon Kab.', 13),
	('3210', '32', 'Majalengka Kab.', 20),
	('3211', '32', 'Sumedang Kab.', 26),
	('3212', '32', 'Indramayu Kab.', 17),
	('3213', '32', 'Subang Kab.', 23),
	('3214', '32', 'Purwakarta Kab.', 22),
	('3215', '32', 'Karawang  Kab.', 18),
	('3216', '32', 'Bekasi Kab.', 6),
	('3217', '32', 'Bandung Barat Kab.', 3),
	('3218', '32', 'Pangandaran  Kab.', 21),
	('3271', '32', 'Bogor Kota ', 9),
	('3272', '32', 'Sukabumi Kota ', 25),
	('3273', '32', 'Bandung Kota ', 2),
	('3274', '32', 'Cirebon Kota ', 14),
	('3275', '32', 'Bekasi Kota ', 7),
	('3276', '32', 'Depok Kota', 15),
	('3277', '32', 'Cimahi Kota', 12),
	('3278', '32', 'Tasikmalaya Kota', 28),
	('3279', '32', 'Banjar Kota', 5),
	('3300', '33', 'All City', 1),
	('3301', '33', 'Cilacap Kota', 2),
	('3302', '33', 'Banyumas Kab.', 3),
	('3303', '33', 'Purbalingga Kab.', 4),
	('3304', '33', 'Banjarnegara Kab.', 5),
	('3305', '33', 'Kebumen Kab.', 6),
	('3306', '33', 'Purworejo Kab.', 7),
	('3307', '33', 'Wonosobo Kab.', 8),
	('3308', '33', 'Magelang Kab.', 9),
	('3309', '33', 'Boyolali Kab.', 10),
	('3310', '33', 'Klaten Kab.', 11),
	('3311', '33', 'Sukoharjo Kab.', 12),
	('3312', '33', 'Wonogiri Kab.', 13),
	('3313', '33', 'Karanganyar Kab.', 14),
	('3314', '33', 'Sragen Kab.', 15),
	('3315', '33', 'Grobogan Kab.', 16),
	('3316', '33', 'Blora Kab.', 17),
	('3317', '33', 'Rembang Kab.', 18),
	('3318', '33', 'Pati Kab.', 19),
	('3319', '33', 'Kudus Kab.', 20),
	('3320', '33', 'Jepara Kab.', 21),
	('3321', '33', 'Demak Kab.', 22),
	('3322', '33', 'Semarang Kab.', 23),
	('3323', '33', 'Temanggung Kab.', 24),
	('3324', '33', 'Kendal Kab.', 25),
	('3325', '33', 'Batang Kab.', 26),
	('3326', '33', 'Pekalongan Kab.', 27),
	('3327', '33', 'Pemalang Kab.', 28),
	('3328', '33', 'Tegal Kab.', 29),
	('3329', '33', 'Brebes Kab.', 30),
	('3371', '33', 'Magelang Kota', 31),
	('3372', '33', 'Surakarta Kota', 32),
	('3373', '33', 'Salatiga Kota', 33),
	('3374', '33', 'Semarang Kota', 34),
	('3375', '33', 'Pekalongan Kota', 35),
	('3376', '33', 'Tegal Kota', 36),
	('3400', '34', 'All City', 1),
	('3401', '34', 'Kulon Progo Kab.', 2),
	('3402', '34', 'Bantul Kab.', 3),
	('3403', '34', 'Gunung Kidul Kab.', 4),
	('3404', '34', 'Sleman Kab.', 5),
	('3471', '34', 'Yogyakarta Kota', 6),
	('3500', '35', 'All City', 1),
	('3501', '35', 'Pacitan Kab.', 2),
	('3502', '35', 'Ponorogo Kab.', 3),
	('3503', '35', 'Trenggalek Kab.', 4),
	('3504', '35', 'Tulungagung Kab.', 5),
	('3505', '35', 'Blitar Kab.', 6),
	('3506', '35', 'Kediri Kab.', 7),
	('3507', '35', 'Malang Kab.', 8),
	('3508', '35', 'Lumajang Kab.', 9),
	('3509', '35', 'Jember Kab.', 10),
	('3510', '35', 'Banyuwangi Kab.', 11),
	('3511', '35', 'Bondowoso Kab.', 12),
	('3512', '35', 'Situbondo Kab.', 13),
	('3513', '35', 'Probolinggo Kab.', 14),
	('3514', '35', 'Pasuruan Kab.', 15),
	('3515', '35', 'Sidoarjo Kab.', 16),
	('3516', '35', 'Mojokerto Kab.', 17),
	('3517', '35', 'Jombang Kab.', 18),
	('3518', '35', 'Nganjuk Kab.', 19),
	('3519', '35', 'Madiun Kab.', 20),
	('3520', '35', 'Magetan Kab.', 21),
	('3521', '35', 'Ngawi Kab.', 22),
	('3522', '35', 'Bojonegoro Kab.', 23),
	('3523', '35', 'Tuban Kab.', 24),
	('3524', '35', 'Lamongan Kab.', 25),
	('3525', '35', 'Gresik Kab.', 26),
	('3526', '35', 'Bangkalan Kab.', 27),
	('3527', '35', 'Sampang Kab.', 28),
	('3528', '35', 'Pamekasan Kab.', 29),
	('3529', '35', 'Sumenep Kab.', 30),
	('3571', '35', 'Kediri Kota', 31),
	('3572', '35', 'Blitar Kota', 32),
	('3573', '35', 'Malang Kota', 33),
	('3574', '35', 'Probolinggo Kota', 34),
	('3575', '35', 'Pasuruan Kota', 35),
	('3576', '35', 'Mojokerto Kota', 36),
	('3577', '35', 'Madiun Kota', 37),
	('3578', '35', 'Surabaya Kota', 38),
	('3579', '35', 'Batu Kota', 39),
	('3600', '36', 'All City', 1),
	('3601', '36', 'Pandeglang Kab.', 2),
	('3602', '36', 'Lebak Kab.', 3),
	('3603', '36', 'Tangerang Kab.', 4),
	('3604', '36', 'Serang Kab.', 5),
	('3671', '36', 'Tangerang Kota', 6),
	('3672', '36', 'Cilegon Kota', 7),
	('3673', '36', 'Serang Kota', 8),
	('3674', '36', 'Tangerang Selatan Kota', 9),
	('5100', '51', 'All City', 1),
	('5101', '51', 'Jembrana Kab.', 2),
	('5102', '51', 'Tabanan Kab.', 3),
	('5103', '51', 'Badung Kab.', 4),
	('5104', '51', 'Gianyar Kab.', 5),
	('5105', '51', 'Klungkung Kab.', 6),
	('5106', '51', 'Bangli Kab.', 7),
	('5107', '51', 'Karang Asem Kab.', 8),
	('5108', '51', 'Buleleng Kab.', 9),
	('5171', '51', 'Denpasar Kota', 10),
	('5200', '52', 'All City', 1),
	('5201', '52', 'Lombok Barat Kab.', 2),
	('5202', '52', 'Lombok Tengah Kab.', 3),
	('5203', '52', 'Lombok Timur Kab.', 4),
	('5204', '52', 'Sumbawa Kab.', 5),
	('5205', '52', 'Dompu Kab.', 6),
	('5206', '52', 'Bima Kab.', 7),
	('5207', '52', 'Sumbawa Barat Kab.', 8),
	('5208', '52', 'Lombok Utara Kab.', 9),
	('5271', '52', 'Mataram Kota', 10),
	('5272', '52', 'Bima Kota', 11),
	('5300', '53', 'All City', 1),
	('5301', '53', 'Sumba Barat Kab.', 2),
	('5302', '53', 'Sumba Timur Kab.', 3),
	('5303', '53', 'Kupang Kab.', 4),
	('5304', '53', 'Timor Tengah Selatan Kab.', 5),
	('5305', '53', 'Timor Tengah Utara Kab.', 6),
	('5306', '53', 'Belu Kab.', 7),
	('5307', '53', 'Alor Kab.', 8),
	('5308', '53', 'Lembata Kab.', 9),
	('5309', '53', 'Flores Timur Kab.', 10),
	('5310', '53', 'Sikka Kab.', 11),
	('5311', '53', 'Ende Kab.', 12),
	('5312', '53', 'Ngada Kab.', 13),
	('5313', '53', 'Manggarai Kab.', 14),
	('5314', '53', 'Rote Ndao Kab.', 15),
	('5315', '53', 'Manggarai Barat Kab.', 16),
	('5316', '53', 'Sumba Tengah Kab.', 17),
	('5317', '53', 'Sumba Barat Daya Kab.', 18),
	('5318', '53', 'Nagekeo Kab.', 19),
	('5319', '53', 'Manggarai Timur Kab.', 20),
	('5320', '53', 'Sabu Raijua Kab.', 21),
	('5321', '53', 'Malaka Kab.', 22),
	('5371', '53', 'Kupang Kota', 23),
	('6100', '61', 'All City', 1),
	('6101', '61', 'Sambas Kab.', 2),
	('6102', '61', 'Bengkayang Kab.', 3),
	('6103', '61', 'Landak Kab.', 4),
	('6104', '61', 'Mempawah Kab.', 5),
	('6105', '61', 'Sanggau Kab.', 6),
	('6106', '61', 'Ketapang Kab.', 7),
	('6107', '61', 'Sintang Kab.', 8),
	('6108', '61', 'Kapuas Hulu Kab.', 9),
	('6109', '61', 'Sekadau Kab.', 10),
	('6110', '61', 'Melawi Kab.', 11),
	('6111', '61', 'Kayong Utara Kab.', 12),
	('6112', '61', 'Kubu Raya Kab.', 13),
	('6171', '61', 'Pontianak Kota', 14),
	('6172', '61', 'Singkawang Kota', 15),
	('6200', '62', 'All City', 1),
	('6201', '62', 'Kotawaringin Barat Kab.', 2),
	('6202', '62', 'Kotawaringin Timur Kab.', 3),
	('6203', '62', 'Kapuas Kab.', 4),
	('6204', '62', 'Barito Selatan Kab.', 5),
	('6205', '62', 'Barito Utara Kab.', 6),
	('6206', '62', 'Sukamara Kab.', 7),
	('6207', '62', 'Lamandau Kab.', 8),
	('6208', '62', 'Seruyan Kab.', 9),
	('6209', '62', 'Katingan Kab.', 10),
	('6210', '62', 'Pulang Pisau Kab.', 11),
	('6211', '62', 'Gunung Mas Kab.', 12),
	('6212', '62', 'Barito Timur Kab.', 13),
	('6213', '62', 'Murung Raya Kab.', 14),
	('6271', '62', 'Palangka Raya Kota', 15),
	('6300', '63', 'All City', 1),
	('6301', '63', 'Tanah Laut Kab.', 2),
	('6302', '63', 'Kota Baru Kab.', 3),
	('6303', '63', 'Banjar Kab.', 4),
	('6304', '63', 'Barito Kuala Kab.', 5),
	('6305', '63', 'Tapin Kab.', 6),
	('6306', '63', 'Hulu Sungai Selatan Kab.', 7),
	('6307', '63', 'Hulu Sungai Tengah Kab.', 8),
	('6308', '63', 'Hulu Sungai Utara Kab.', 9),
	('6309', '63', 'Tabalong Kab.', 10),
	('6310', '63', 'Tanah Bumbu Kab.', 11),
	('6311', '63', 'Balangan Kab.', 12),
	('6371', '63', 'Banjarmasin Kota', 13),
	('6372', '63', 'Banjar Baru Kota', 14),
	('6400', '64', 'All City', 1),
	('6401', '64', 'Paser Kab.', 2),
	('6402', '64', 'Kutai Barat Kab.', 3),
	('6403', '64', 'Kutai Kartanegara Kab.', 4),
	('6404', '64', 'Kutai Timur Kab.', 5),
	('6405', '64', 'Berau Kab.', 6),
	('6409', '64', 'Penajam Paser Utara Kab.', 7),
	('6411', '64', 'Mahakam Hulu Kab.', 8),
	('6471', '64', 'Balikpapan Kota', 9),
	('6472', '64', 'Samarinda Kota', 10),
	('6474', '64', 'Bontang Kota', 11),
	('6501', '65', 'Malinau Kab.', 12),
	('6502', '65', 'Bulungan Kab.', 13),
	('6503', '65', 'Tana Tidung Kab.', 14),
	('6504', '65', 'Nunukan Kab.', 15),
	('6571', '65', 'Tarakan Kota', 16),
	('7100', '71', 'All City', 1),
	('7101', '71', 'Bolaang Mongondow Kab.', 2),
	('7102', '71', 'Minahasa Kab.', 3),
	('7103', '71', 'Kepulauan Sangihe Kab.', 4),
	('7104', '71', 'Kepulauan Talaud Kab.', 5),
	('7105', '71', 'Minahasa Selatan Kab.', 6),
	('7106', '71', 'Minahasa Utara Kab.', 7),
	('7107', '71', 'Bolaang Mongondow Utara Kab.', 8),
	('7108', '71', 'Siau Tagulandang Biaro Kab.', 9),
	('7109', '71', 'Minahasa Tenggara Kab.', 10),
	('7110', '71', 'Bolaang Mongondow Selatan Kab.', 11),
	('7111', '71', 'Bolaang Mongondow Timur Kab.', 12),
	('7171', '71', 'Manado Kota', 13),
	('7172', '71', 'Bitung Kota', 14),
	('7173', '71', 'Tomohon Kota', 15),
	('7174', '71', 'Kotamobagu Kota', 16),
	('7200', '72', 'All City', 1),
	('7201', '72', 'Banggai Kepulauan Kab.', 2),
	('7202', '72', 'Banggai Kab.', 3),
	('7203', '72', 'Morowali Kab.', 4),
	('7204', '72', 'Poso Kab.', 5),
	('7205', '72', 'Donggala Kab.', 6),
	('7206', '72', 'Toli-Toli Kab.', 7),
	('7207', '72', 'Buol Kab.', 8),
	('7208', '72', 'Parigi Moutong Kab.', 9),
	('7209', '72', 'Tojo Una-Una Kab.', 10),
	('7210', '72', 'Sigi Kab.', 11),
	('7211', '72', 'Banggai Laut Kab.', 12),
	('7212', '72', 'Morowali Utara Kab.', 13),
	('7271', '72', 'Palu Kota', 14),
	('7300', '73', 'All City', 1),
	('7301', '73', 'Kepulauan Selayar Kab.', 2),
	('7302', '73', 'Bulukumba Kab.', 3),
	('7303', '73', 'Bantaeng Kab.', 4),
	('7304', '73', 'Jeneponto Kab.', 5),
	('7305', '73', 'Takalar Kab.', 6),
	('7306', '73', 'Gowa Kab.', 7),
	('7307', '73', 'Sinjai Kab.', 8),
	('7308', '73', 'Maros Kab.', 9),
	('7309', '73', 'Pangkajene Dan Kepulauan Kab.', 10),
	('7310', '73', 'Barru Kab.', 11),
	('7311', '73', 'Bone Kab.', 12),
	('7312', '73', 'Soppeng Kab.', 13),
	('7313', '73', 'Wajo Kab.', 14),
	('7314', '73', 'Sidenreng Rappang Kab.', 15),
	('7315', '73', 'Pinrang Kab.', 16),
	('7316', '73', 'Enrekang Kab.', 17),
	('7317', '73', 'Luwu Kab.', 18),
	('7318', '73', 'Tana Toraja Kab.', 19),
	('7322', '73', 'Luwu Utara Kab.', 20),
	('7325', '73', 'Luwu Timur Kab.', 21),
	('7326', '73', 'Toraja Utara Kab.', 22),
	('7371', '73', 'Makassar Kota', 23),
	('7372', '73', 'Parepare Kota', 24),
	('7373', '73', 'Palopo Kota', 25),
	('7400', '74', 'All City', 1),
	('7401', '74', 'Buton Kab.', 2),
	('7402', '74', 'Muna Kab.', 3),
	('7403', '74', 'Konawe Kab.', 4),
	('7404', '74', 'Kolaka Kab.', 5),
	('7405', '74', 'Konawe Selatan Kab.', 6),
	('7406', '74', 'Bombana Kab.', 7),
	('7407', '74', 'Wakatobi Kab.', 8),
	('7408', '74', 'Kolaka Utara Kab.', 9),
	('7409', '74', 'Buton Utara Kab.', 10),
	('7410', '74', 'Konawe Utara Kab.', 11),
	('7411', '74', 'Kolaka Timur Kab.', 12),
	('7412', '74', 'Konawe Kepulauan Kab.', 13),
	('7413', '74', 'Muna Barat Kab.', 14),
	('7414', '74', 'Buton Tengah Kab.', 15),
	('7415', '74', 'Buton Selatan Kab.', 16),
	('7471', '74', 'Kendari Kota', 17),
	('7472', '74', 'Baubau Kota', 18),
	('7500', '75', 'All City', 1),
	('7501', '75', 'Boalemo Kab.', 2),
	('7502', '75', 'Gorontalo Kab.', 3),
	('7503', '75', 'Pohuwato Kab.', 4),
	('7504', '75', 'Bone Bolango Kab.', 5),
	('7505', '75', 'Gorontalo Utara Kab.', 6),
	('7571', '75', 'Gorontalo Kota', 7),
	('7601', '76', 'Majene Kab.', 8),
	('7602', '76', 'Polewali Mandar Kab.', 9),
	('7603', '76', 'Mamasa Kab.', 10),
	('7604', '76', 'Mamuju Kab.', 11),
	('7605', '76', 'Mamuju Utara Kab.', 12),
	('7606', '76', 'Mamuju Tengah Kab.', 13),
	('8100', '81', 'All City', 1),
	('8101', '81', 'Maluku Tenggara Barat Kab.', 2),
	('8102', '81', 'Maluku Tenggara Kab.', 3),
	('8103', '81', 'Maluku Tengah Kab.', 4),
	('8104', '81', 'Buru Kab.', 5),
	('8105', '81', 'Kepulauan Aru Kab.', 6),
	('8106', '81', 'Seram Bagian Barat Kab.', 7),
	('8107', '81', 'Seram Bagian Timur Kab.', 8),
	('8108', '81', 'Maluku Barat Daya Kab.', 9),
	('8109', '81', 'Buru Selatan Kab.', 10),
	('8171', '81', 'Ambon Kota', 11),
	('8172', '81', 'Tual Kota', 12),
	('8200', '82', 'All City', 1),
	('8201', '82', 'Halmahera Barat Kab.', 2),
	('8202', '82', 'Halmahera Tengah Kab.', 3),
	('8203', '82', 'Kepulauan Sula Kab.', 4),
	('8204', '82', 'Halmahera Selatan Kab.', 5),
	('8205', '82', 'Halmahera Utara Kab.', 6),
	('8206', '82', 'Halmahera Timur Kab.', 7),
	('8207', '82', 'Pulau Morotai Kab.', 8),
	('8208', '82', 'Pulau Taliabu Kab.', 9),
	('8271', '82', 'Ternate Kota', 10),
	('8272', '82', 'Tidore Kepulauan Kota', 11),
	('9100', '91', 'All City', 1),
	('9101', '91', 'Fakfak Kab.', 2),
	('9102', '91', 'Kaimana Kab.', 3),
	('9103', '91', 'Teluk Wondama Kab.', 4),
	('9104', '91', 'Teluk Bintuni Kab.', 5),
	('9105', '91', 'Manokwari Kab.', 6),
	('9106', '91', 'Sorong Selatan Kab.', 7),
	('9107', '91', 'Sorong Kab.', 8),
	('9108', '91', 'Raja Ampat Kab.', 9),
	('9109', '91', 'Tambrauw Kab.', 10),
	('9110', '91', 'Maybrat Kab.', 11),
	('9111', '91', 'Manokwari Selatan Kab.', 12),
	('9112', '91', 'Pegunungan Arfak Kab.', 13),
	('9171', '91', 'Sorong Kota', 14),
	('9400', '94', 'All City', 1),
	('9401', '94', 'Merauke Kab.', 2),
	('9402', '94', 'Jayawijaya Kab.', 3),
	('9403', '94', 'Jayapura Kab.', 4),
	('9404', '94', 'Nabire Kab.', 5),
	('9408', '94', 'Kepulauan Yapen Kab.', 6),
	('9409', '94', 'Biak Numfor Kab.', 7),
	('9410', '94', 'Paniai Kab.', 8),
	('9411', '94', 'Puncak Jaya Kab.', 9),
	('9412', '94', 'Mimika Kab.', 10),
	('9413', '94', 'Boven Digoel Kab.', 11),
	('9414', '94', 'Mappi Kab.', 12),
	('9415', '94', 'Asmat Kab.', 13),
	('9416', '94', 'Yahukimo Kab.', 14),
	('9417', '94', 'Pegunungan Bintang Kab.', 15),
	('9418', '94', 'Tolikara Kab.', 16),
	('9419', '94', 'Sarmi Kab.', 17),
	('9420', '94', 'Keerom Kab.', 18),
	('9426', '94', 'Waropen Kab.', 19),
	('9427', '94', 'Supiori Kab.', 20),
	('9428', '94', 'Mamberamo Raya Kab.', 21),
	('9429', '94', 'Nduga Kab.', 22),
	('9430', '94', 'Lanny Jaya Kab.', 23),
	('9431', '94', 'Mamberamo Tengah Kab.', 24),
	('9432', '94', 'Yalimo Kab.', 25),
	('9433', '94', 'Puncak Kab.', 26),
	('9434', '94', 'Dogiyai Kab.', 27),
	('9435', '94', 'Intan Jaya Kab.', 28),
	('9436', '94', 'Deiyai Kab.', 29),
	('9471', '94', 'Jayapura Kota', 30);
/*!40000 ALTER TABLE `regencies` ENABLE KEYS */;

-- Dumping structure for table joedaart.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(25) COLLATE utf8_unicode_ci DEFAULT '0',
  `admins` smallint(2) NOT NULL DEFAULT '0',
  `isVerified` tinyint(1) DEFAULT '0',
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `roles` smallint(2) DEFAULT '0',
  `access_token` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wa` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sms` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` smallint(2) DEFAULT NULL,
  `province` smallint(5) DEFAULT NULL,
  `city` smallint(5) DEFAULT NULL,
  `status` smallint(1) DEFAULT '1',
  `status_permanen` smallint(1) DEFAULT '1',
  `gallery` varchar(150) COLLATE utf8_unicode_ci DEFAULT '1',
  `last_login` timestamp NULL DEFAULT NULL,
  `facebook` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkedin` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table joedaart.users: ~4 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `kode`, `admins`, `isVerified`, `name`, `roles`, `access_token`, `email`, `password`, `phone`, `wa`, `sms`, `address`, `country`, `province`, `city`, `status`, `status_permanen`, `gallery`, `last_login`, `facebook`, `instagram`, `twitter`, `google`, `linkedin`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(3, 'hLYjiMLy9BPzDpqzcj7WxlWQE', 0, 1, 'guest', 0, NULL, 'guest@gmail.com', '4297f44b13955235245b2497399d7a93', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '1', NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-21 01:58:54', '2019-10-14 07:39:19', NULL, NULL),
	(4, '1DWYH470pJ4OJVJiPXSgoLB1s', 1, 1, 'admin', 0, NULL, 'admin@admin.com', '4297f44b13955235245b2497399d7a93', '0812333333', '0812333332', '0812333330', 'Jl. Address', NULL, 1, 1, 1, 1, '1', NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-21 01:59:22', '2019-09-16 21:32:26', NULL, NULL),
	(5, 'enGkSamJzp8ZK35SBoDr0G5hI', 2, 1, 'user', 0, NULL, 'ekafise@gmail.com', '4297f44b13955235245b2497399d7a93', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-21 02:05:32', '2019-09-16 21:32:28', NULL, NULL),
	(23, 's4barqnOdx3lTYgli6KRpZtt9', 2, 1, 'boy1', 0, NULL, 'ekafise.pns@gmail.com', '0c0b3da4ac402bd86191d959be081114', '0812345678', '0812345678', '09812332323', 'jl. country', 1, 11, 1, 1, 1, '1', NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-11 16:19:50', '2019-09-19 05:50:31', NULL, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


-- Dumping database structure for taatjoedadbase
DROP DATABASE IF EXISTS `taatjoedadbase`;
CREATE DATABASE IF NOT EXISTS `taatjoedadbase` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `taatjoedadbase`;

-- Dumping structure for table taatjoedadbase.hitung
DROP TABLE IF EXISTS `hitung`;
CREATE TABLE IF NOT EXISTS `hitung` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pengunjung_harian` int(11) NOT NULL DEFAULT '0',
  `pengunjung_bulanan` int(11) NOT NULL DEFAULT '0',
  `pengunjung_sampai_saat_ini` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table taatjoedadbase.hitung: 1 rows
DELETE FROM `hitung`;
/*!40000 ALTER TABLE `hitung` DISABLE KEYS */;
INSERT INTO `hitung` (`id`, `pengunjung_harian`, `pengunjung_bulanan`, `pengunjung_sampai_saat_ini`) VALUES
	(1, 785, 785, 786);
/*!40000 ALTER TABLE `hitung` ENABLE KEYS */;

-- Dumping structure for table taatjoedadbase.kategori
DROP TABLE IF EXISTS `kategori`;
CREATE TABLE IF NOT EXISTS `kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `gambar` varchar(50) DEFAULT NULL,
  `folder` varchar(50) DEFAULT NULL,
  `index_lihat` int(3) DEFAULT '1',
  `flag_aktif` enum('Y','N') DEFAULT 'Y',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(25) DEFAULT NULL,
  `updated_by` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Dumping data for table taatjoedadbase.kategori: 4 rows
DELETE FROM `kategori`;
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;
INSERT INTO `kategori` (`id`, `judul`, `keterangan`, `gambar`, `folder`, `index_lihat`, `flag_aktif`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(18, '1999', '', '05082019042200IMG_20140924_130521.jpg', '-', 1, 'Y', '2019-08-05 11:22:00', '2019-09-17 11:07:09', NULL, NULL),
	(17, '2000', '', '05082019042102GATE.jpg', '-', 1, 'Y', '2019-08-05 11:21:02', '2019-09-17 11:07:12', NULL, NULL),
	(16, '2001', '', '05082019041454BASE MIRING PORTFOLIO ATAU BUKU.jpg', '-', 1, 'Y', '2019-08-05 11:14:54', '2019-09-17 11:07:18', NULL, NULL),
	(15, '2002', '-', '050820190411181. SITH ICBP 2009.JPG', '-', 1, 'Y', '2019-08-05 11:11:18', '2019-09-17 11:07:21', NULL, NULL);
/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;

-- Dumping structure for table taatjoedadbase.kategori_sub
DROP TABLE IF EXISTS `kategori_sub`;
CREATE TABLE IF NOT EXISTS `kategori_sub` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) DEFAULT NULL,
  `kategori` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `gambar` varchar(100) DEFAULT NULL,
  `folder` varchar(50) DEFAULT NULL,
  `media` varchar(150) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `ukuran` varchar(150) DEFAULT NULL,
  `index_lihat` int(11) DEFAULT NULL,
  `flag_aktif` enum('Y','N') DEFAULT 'Y',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table taatjoedadbase.kategori_sub: 9 rows
DELETE FROM `kategori_sub`;
/*!40000 ALTER TABLE `kategori_sub` DISABLE KEYS */;
INSERT INTO `kategori_sub` (`id`, `judul`, `kategori`, `keterangan`, `gambar`, `folder`, `media`, `tahun`, `ukuran`, `index_lihat`, `flag_aktif`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(9, 'Mentari Pagi', 9, 'kehidupan pagi yang indah', '15072019045531backdrop-17-1024x576.jpg', '-', 'canvas', '1990', '200cm x 100cm', 2, 'Y', '2019-07-15 11:55:31', '2019-09-23 10:08:13', NULL, NULL),
	(8, 'Solusi Kehidupan Mental', 9, 'mentari solusi manusia ', '14072019133050backdrop-33.jpg', '-', 'canvas', '1991', '100cm x 50cm', 1, 'Y', '2019-07-14 20:30:50', '2019-09-23 10:08:28', NULL, NULL),
	(7, 'Kuda Lumping ', 9, 'karya pada saat melihat kuda lumping', '14072019130914backdrop-12-1024x768.jpg', '-', 'canvas', '2000', '175cm x 100cm ', 3, 'Y', '2019-07-14 20:09:14', '2019-09-23 10:08:37', NULL, NULL),
	(10, 'Kabah Mendekat', 10, 'keinginan naik haji kembali', '15072019045531backdrop-17-1024x576.jpg', '-', 'canvas', '1995', '250cm x 300cm', 1, 'Y', '2019-08-04 05:43:05', '2019-09-23 10:08:46', NULL, NULL),
	(11, 'Dekat Dengan Saya', 15, 'Kedekatan anak dan orang tua', '12082019032232BACKDROP SBY (2) ACARA.jpg', '-', 'canvas', '1996', '100cm x 100cm', 1, 'Y', '2019-08-12 10:22:32', '2019-09-23 10:09:06', NULL, NULL),
	(12, 'Ujian Agar Dekat Dengan Alloh', 15, 'ujian adalah ujian', '12082019032959BACKDROP SBY (1).jpg', '-', 'canvas', '1997', '125cm x 100cm', 1, 'Y', '2019-08-12 10:29:59', '2019-09-23 10:09:15', NULL, NULL),
	(13, 'Masalah Siapa', 15, 'Siapa yang bermasalah', '12082019033010BACKDROP SBY (4).jpg', '-', 'canvas', '1998', '235cm x 150cm', 1, 'Y', '2019-08-12 10:30:10', '2019-09-23 10:09:24', NULL, NULL),
	(14, 'Lima Puluh Tahun Yang Lalu', 15, '50 tahun yang lalu', '12082019033020BACKDROP SBY (16).jpg', '-', 'canvas', '1999', '100cm x 100cm', 1, 'Y', '2019-08-12 10:30:20', '2019-09-23 10:09:29', NULL, NULL),
	(15, 'Pahala 1000 Bulan', 15, 'Lailatul Qodar', '12082019033030BACKDROP SBY (7).jpg', '-', 'canvas', '2000', '150cm x 150cm', 1, 'Y', '2019-08-12 10:30:30', '2019-09-23 10:09:43', NULL, NULL);
/*!40000 ALTER TABLE `kategori_sub` ENABLE KEYS */;

-- Dumping structure for table taatjoedadbase.petugas
DROP TABLE IF EXISTS `petugas`;
CREATE TABLE IF NOT EXISTS `petugas` (
  `kd_petugas` char(4) NOT NULL,
  `nm_petugas` varchar(100) NOT NULL,
  `no_telepon` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(200) NOT NULL,
  `level` varchar(20) NOT NULL DEFAULT 'Kasir',
  `aktif` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  PRIMARY KEY (`kd_petugas`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table taatjoedadbase.petugas: 1 rows
DELETE FROM `petugas`;
/*!40000 ALTER TABLE `petugas` DISABLE KEYS */;
INSERT INTO `petugas` (`kd_petugas`, `nm_petugas`, `no_telepon`, `username`, `password`, `level`, `aktif`) VALUES
	('1', 'administrator', '1111', 'admin', '123', 'Admin', 'Yes');
/*!40000 ALTER TABLE `petugas` ENABLE KEYS */;

-- Dumping structure for table taatjoedadbase.rumah
DROP TABLE IF EXISTS `rumah`;
CREATE TABLE IF NOT EXISTS `rumah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul_web_header` varchar(200) NOT NULL DEFAULT '-',
  `logo_header` varchar(200) NOT NULL DEFAULT '-',
  `deskripsi_header` text NOT NULL,
  `deskripsi_menu` text NOT NULL,
  `logo_footer` varchar(100) NOT NULL DEFAULT '-',
  `deskripsi_footer` text NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `wa1` varchar(100) NOT NULL DEFAULT '-',
  `wa2` varchar(100) NOT NULL DEFAULT '-',
  `hp1` varchar(100) NOT NULL DEFAULT '-',
  `hp2` varchar(100) NOT NULL DEFAULT '-',
  `email1` varchar(100) NOT NULL DEFAULT '-',
  `email2` varchar(100) NOT NULL DEFAULT '-',
  `fb` varchar(100) NOT NULL DEFAULT '-',
  `ig` varchar(100) NOT NULL DEFAULT '-',
  `tw` varchar(100) NOT NULL DEFAULT '-',
  `logo_about_us` varchar(100) NOT NULL DEFAULT '-',
  `deskripsi_about_us` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table taatjoedadbase.rumah: 1 rows
DELETE FROM `rumah`;
/*!40000 ALTER TABLE `rumah` DISABLE KEYS */;
INSERT INTO `rumah` (`id`, `judul_web_header`, `logo_header`, `deskripsi_header`, `deskripsi_menu`, `logo_footer`, `deskripsi_footer`, `alamat`, `wa1`, `wa2`, `hp1`, `hp2`, `email1`, `email2`, `fb`, `ig`, `tw`, `logo_about_us`, `deskripsi_about_us`) VALUES
	(1, 'Taat Joeda', 'LOGO.jpg', '', '', 'FOOTER.jpg', '', 'jl. sukahaji permai II no.3 bandung 40152, jawa barat, indonesia', '081394202000', 'apitexhibition', '081394202000', '081394202001', 'chafidyoeda@gmail.com', 'chafidyoeda@gmail.com', 'apitexhibition', 'apitexhibition', 'apitexhibition', '', 'Taat Joeda');
/*!40000 ALTER TABLE `rumah` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
