</div>

<footer class="page-footer font-small pt-4 bottom">
<hr style="background: #fff;">
    <div class="container-fluid text-center text-md-left">
      <div class="row">
        <div class="col-md-6 mt-md-0 text-center">
          <h3>Joeda Art</h3>
          <p>Situs Jual Beli Karya Seni</p>
              <a class="ins-ic mr-3" role="button"><i class="fab fa-lg fa-instagram"></i></a>
              <a class="fb-ic mr-3" role="button"><i class="fab fa-lg fa-facebook-f"></i></a>
              <a class="tw-ic mr-3" role="button"><i class="fab fa-lg fa-twitter"></i></a>
              <a class="yt-ic mr-3" role="button"><i class="fab fa-lg fa-youtube"></i></a>
        </div>
        <div class="col-md-3 mb-md-0 mb-3 mx-auto">
            <h5 class="text-uppercase"></h5>
            <ul class="list-unstyled">
              <li>
                <a href="?page=pusat-bantuan">Pusat Bantuan</a>
              </li>
              <li>
                <a href="?page=syarat-ketentuan">Syarat & Ketentuan</a>
              </li>
              <li>
                <a href="?page=kebijakan-privasi">Kebijakan Privasi</a>
              </li>
              <li>
                <a href="#!">About Joeda Art</a>
              </li>
            </ul>
          </div>
      </div>
    </div>
    <div class="footer-copyright text-center py-3">Copyright © 2019 <b>Joeda Art</b>
    </div>
  </footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

<script src="../asset/js/app.js"></script>
<script src="../public/js/dropzone.js"></script>


</body>
</html>