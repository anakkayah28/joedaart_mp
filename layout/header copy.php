<?php 
  //include_once "./library/inc.seslogin.php"; 
  include_once "./library/inc.connection.php"; 
  include_once "./library/config.php";
  session_start();

      if (isset($_GET['code'])) {
        $mySql = mysqli_query ($koneksidb, "SELECT * FROM users WHERE kode='".$_GET['code']."'") or die ("Query Salah : ".mysqli_error());
        $myData= mysqli_fetch_assoc ($mySql);
        $_SESSION['SESLOGINJA'] = $myData['id'];
        $_SESSION['SESKODEJA'] = $myData['kode'];  
        $_SESSION['SESEMAILJA'] = $myData['email']; 
        $_SESSION['SESNAMAJA'] = $myData['name']; 
       include_once "./library/inc.seslogin.php";
      }

?>
<!doctype html>
<html lang="en">
<!-- <head> -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
      <!-- fav and touch icons -->
    <link rel="apple-touch-icon" sizes="57x57" href="/images/favicon.ico">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/favicon.ico">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favicon.ico">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favicon.icog">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/favicon.ico">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/favicon.ico">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/favicon.ico">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/favicon.ico">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon.ico">
    <link rel="icon" type="image/png" sizes="192x192"  href="/images/favicon.ico">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon.ico">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/favicon.ico">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/images/favicon.ico">
    <meta name="theme-color" content="#ffffff">

  <title>Joeda Art</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.css" />
  <link rel="stylesheet" href="./public/css/style.css">
  <link rel="stylesheet" href="./public/css/slick.css">
  <link rel="stylesheet" href="./public/css/slick-theme.css">



  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script src="./asset/select2-4.0.6-rc.1/dist/js/select2.min.js"></script>   
 
  <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- script src="./asset/js/app.js"></script -->
  <title></title>
</head>
<body>
<!-- nitip di header_bu.ejs -->
 <header>
  <!-- Fixed navbar -->
<!-- example 8 - center logo on mobile, search right -->
<nav class="navbar navbar-expand-md navbar-light bg-light">
    <div class="navbar-collapse collapse w-100 order-4 order-md-0 collapsenav">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                 <a class="two my-2 my-sm-0 pr-4"  href="./?page=collections&id=1" style="color: grey;font-size: 1.0rem;"><b>PAINTING</b></a>
            </li>
            <li class="nav-item">
               <a class="two  my-2 my-sm-0 pr-4" href="./?page=collections&id=2" style="color: grey;font-size: 1.0rem;"><b>SCULPTURE</b></a>
            </li>
             <li class="nav-item">
               <a class="two  my-2 my-sm-0 pr-4" href="./?page=collections&id=3" style="color: grey;font-size: 1.0rem;"><b>POTTERY</b></a>
            </li>
             <li class="nav-item">
               <a class="two  my-2 my-sm-0 pr-4" href="./?page=collections&id=4" style="color: grey;font-size: 1.0rem;"><b>CRAFT</b></a>
            </li>
             <li class="nav-item">
               <a class="two  my-2 my-sm-0 pr-4" href="./?page=collections&id=5" style="color: grey;font-size: 1.0rem;"><b>DIGITAL ART</b></a>
            </li>
             <li class="nav-item">
               <a class="two  my-2 my-sm-0 pr-4" href="./?page=collections&id=6" style="color: grey;font-size: 1.0rem;"><b>ARTWORK</b></a>
            </li>
        </ul>
    </div>
    <div class="w-100 d-flex flex-nowrap">
        <div class="w-100 d-md-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".collapsenav">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="d-flex mx-auto order-0">
        </div>
        <div class="d-flex justify-content-end order-3">
          <div class="navbar-collapse collapse w-100 order-4 order-md-0 collapsenav">
          <ul class="navbar-nav mr-auto">
          
                   <li class="nav-item">
                        <div class="dropdown1">
                          <a class="btn btn-default dropbtn1 my-2 my-sm-0 mr-1" style="color:black" href="./?page">Home</a>
                        </div>
                   </li>
                   <li class="nav-item">
                        <div class="dropdown1">
                          <a class="btn btn-default dropbtn1 my-2 my-sm-0 mr-1" style="color:black" href="./?page=register">Register</a>
                        </div>
                   </li>
                   <li class="nav-item">
                        <div class="dropdown1">
                          <a class="btn btn-default dropbtn1 my-2 my-sm-0 mr-1" style="color:black" href="./?page=login">Sign In</a>
                        </div>
                   </li>
                
                  <li>
                      <div class="dropdown1">
                        <a href="#" data-toggle="popover" data-placement="bottom" class="btn btn-default dropbtn1 my-2 my-sm-0 mr-1" style="color:black" title="Find" data-content="">Filter<b class="caret"></b></a>
                      </div>
                  </li>
                    <div id="popover_content_wrapper"style="display:none">
                        <div>
                              Category :
                              <select class="browser-default custom-select" id="cmbcategory">
                                <option value="0">All Category</option>
                              <?php
                                      $mySql = "SELECT * FROM categories ORDER BY id ASC";
                                      $myQry = mysqli_query($koneksidb, $mySql)  or die ("Query salah : ".mysql_error());
                                      while ($myData = mysqli_fetch_array($myQry)) {
                              ?>           
                                  <option value="<?php echo $myData['id']; ?>"><?php echo $myData['title'] .' '. $myData['judul22']; ?></option>
                              <?php 
                                      };
                              ?>
                            </select> 
                        </div>

                        <div>
                              Country :
                              <?php                    
                    $sql_provinsi = mysqli_query($koneksidb,"SELECT * FROM countries ORDER BY id ASC");
                   ?>
                              <select class="form-control" name="country" id="country" onchange="ubahnegara();">
                              <option value="0">All Country</option>
                    <?php 
                        $tbh=1;
                        while($rs_provinsi = mysqli_fetch_assoc($sql_provinsi)){  
                          if($tbh==1){ 
                            echo "<option class='font-weight-bold' value='" .$rs_provinsi['id']. "'>" .$rs_provinsi['name']. "</option>";
                          }else{
                            echo '<option value="'.$rs_provinsi['id'].'">'.$rs_provinsi['name'].'</option>';
                          } 

                           
                           $tbh++;
                        }                        
                      ?>
                  </select>
                  <img src="./asset/img/loading.gif" width="35" id="load1" style="display:none;"/>
                        
                      </div>
                        <div>
                              Province :
                
                              <select class="form-control" name="provinsi" id="provinsi">
                              <option value="0">All Province</option>
                
                  </select>
                  <img src="./asset/img/loading.gif" width="35" id="load1" style="display:none;"/>
                        
                      </div>
                     
                        <div>City :
                        <select class="form-control"name="kota" id="kota">
                        <option value="0">All City</option>
                  </select>
                  <img src="./asset/img/loading.gif"width="35" id="load2" style="display:none;"/>
                        </div>
                        
                     
                        <div class="mt-2">
                              <button class="btn btn-default dropbtn1 my-2 my-sm-0" style="color:black" type="button" onClick="searching()" id="cari2" value="Cari"><i class="fas fa-search"></i> Search</button>
                        </div>
                          
                    </div>
                  <li class="nav-item">
                        <form class="d-flex flex-nowrap align-items-center"  id="forma1">          
                          <input id="caritext" class="form-control mr-sm-2 my-2 my-sm-0" type="text" placeholder="Search" aria-label="Search">
                          <div class="dropdown1">
                            <button class="btn btn-default dropbtn1 my-2 my-sm-0" style="color:black" type="button" onClick="searching()" id="cari" value="Cari"><i class="fas fa-search"></i></button>
                          </div>
                        </form>
                  </li>    
        </ul>
    </div>  
</nav>
<div class="text-right">

  
</div>
</header>
<main role="main">
  <div class="container-fluid text-white" style="background-color:#FF0000;">
        <div  style="margin-bottom: 20px;">
            <img alt="" src="./public/images/logo-yoedaart.png" style="height:125px;margin-top: 20px;margin-left: 40px;">
        </div>
    </div>
  </div>
</main>
 
  </div>

    
 