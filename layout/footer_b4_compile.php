</div>

<footer class="page-footer font-small pt-4 bottom">
<hr style="background: #fff;">
    <!-- Footer Links -->
    <div class="container-fluid text-center text-md-left">
      <!-- Grid row -->
      <div class="row">
        <!-- Grid column -->
        <div class="col-md-6 mt-md-0 text-center">
          <!-- Content -->
          <h3>Joeda Art</h3>
          <p>Situs Jual Beli Karya Seni</p>
              <!--Instagram-->
              <a class="ins-ic mr-3" role="button"><i class="fab fa-lg fa-instagram"></i></a>
              <!--Facebook-->
              <a class="fb-ic mr-3" role="button"><i class="fab fa-lg fa-facebook-f"></i></a>
              <!--Twitter-->
              <a class="tw-ic mr-3" role="button"><i class="fab fa-lg fa-twitter"></i></a>
              <!--Youtube-->
              <a class="yt-ic mr-3" role="button"><i class="fab fa-lg fa-youtube"></i></a>
              <!--Slack-->
        </div>

        <!-- Grid column -->
        <div class="col-md-3 mb-md-0 mb-3 mx-auto">
            <!-- Links -->
            <h5 class="text-uppercase"></h5>
            <ul class="list-unstyled">
              <li>
                <a href="?page=pusat-bantuan">Pusat Bantuan</a>
              </li>
              <li>
                <a href="?page=syarat-ketentuan">Syarat & Ketentuan</a>
              </li>
              <li>
                <a href="?page=kebijakan-privasi">Kebijakan Privasi</a>
              </li>
              <li>
                <a href="#!">About Joeda Art</a>
              </li>
            </ul>
          </div>
      </div>
      <!-- Grid row -->
    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">Copyright © 2019 <b>Joeda Art</b>
    </div>
    <!-- Copyright -->
  </footer>
  <!-- Footer -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
  crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.js"></script>
<script src="./public/js/main.js"></script>
<!-- </body> </html> -->
</body>
</html>

<script type="text/javascript">
    
    function ubahnegara(){
         $("img#load1").show();
         var id_countries = 1;//$(this.target).val(); 
         $.ajax({
            type: "POST",
            dataType: "html",
            url: "data-wilayah.php?jenis=provinsi",
            data: "id_countries="+id_countries,
            success: function(msg){
               $("select#provinsi").html(msg);                                                       
               $("img#load1").hide();
               getAjaxProvince();                                                        
            }
         });                    
       }; 


    function getAjaxProvince(){
         $("img#load1").show();
         var id_provinces = 32;//$(this.target).val(); 
         $.ajax({
            type: "POST",
            dataType: "html",
            url: "data-wilayah.php?jenis=kota",
            data: "id_provinces="+id_provinces,
            success: function(msg){
               $("select#kota").html(msg);                                                       
               $("img#load1").hide();
               getAjaxKota();                                                        
            }
         });                    
       }; 

       function getAjaxKota(){
            $("img#load2").show();
            var id_regencies = $("#kota").val();
            $.ajax({
               type: "POST",
               dataType: "html",
               url: "data-wilayah.php?jenis=kecamatan",
               data: "id_regencies="+id_regencies,
               success: function(msg){
                  $("select#kecamatan").html(msg);                              
                  $("img#load2").hide(); 
                 //getAjaxKecamatan();                                                    
               }
            });
       }
    


    function searching(){
      var n1 = document.getElementById("caritext").value;
      location.href =  "./?page=searching&find=" + n1 ;            
    };

  $(document).ready(function(){
     
   $("#provinsi").change(function(){
         $("img#load1").show();
         var id_provinces = $(this).val(); 
         $.ajax({
            type: "POST",
            dataType: "html",
            url: "data-wilayah.php?jenis=kota",
            data: "id_provinces="+id_provinces,
            success: function(msg){
               $("select#kota").html(msg);                                                       
               $("img#load1").hide();
               getAjaxKota();                                                        
            }
         });                    
       });  
   
   $("#kota").change(getAjaxKota);
       function getAjaxKota(){
            $("img#load2").show();
            var id_regencies = $("#kota").val();
            $.ajax({
               type: "POST",
               dataType: "html",
               url: "data-wilayah.php?jenis=kecamatan",
               data: "id_regencies="+id_regencies,
               success: function(msg){
                  $("select#kecamatan").html(msg);                              
                  $("img#load2").hide(); 
                 getAjaxKecamatan();                                                    
               }
            });
       }
   
       $("#kecamatan").change(getAjaxKecamatan);
       function getAjaxKecamatan(){
            $("img#load3").show();
            var id_district = $("#kecamatan").val();
            $.ajax({
               type: "POST",
               dataType: "html",
               url: "data-wilayah.php?jenis=kelurahan",
               data: "id_district="+id_district,
               success: function(msg){
                  $("select#kelurahan").html(msg);                              
                  $("img#load3").hide();                                                 
               }
            });
       }


       $('[data-toggle="popover"]').popover({ 
        html : true,
        content: function() {
        return $('#popover_content_wrapper').html();
      }
    });


    $("#myCarousel").on("slide.bs.carousel", function(e) {
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 3;
    var totalItems = $(".carousel-item").length;

    if (idx >= totalItems - (itemsPerSlide - 1)) {
      var it = itemsPerSlide - (totalItems - idx);
      for (var i = 0; i < it; i++) {
        // append slides to end
        if (e.direction == "left") {
          $(".carousel-item")
            .eq(i)
            .appendTo(".carousel-inner");
        } else {
          $(".carousel-item")
            .eq(0)
            .appendTo($(this).find(".carousel-inner"));
        }
      }
    }
  });
});

function viewprovince(){
        var dropdown = document.getElementById("cmbcountry");
				var selection = dropdown.value;
        console.log(selection);
    };

 </script>