<style>
      /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
      .glyphicon { margin-right:5px; }
            .thumbnail
            {
                margin-bottom: 10px;
                padding: 0px;
                -webkit-border-radius: 0px;
                -moz-border-radius: 0px;
                border-radius: 0px;
            }
            .item.list-group-item
            {
                float: none;
                width: 100%;
                background-color: #fff;
                margin-bottom: 10px;
            }
            .item.list-group-item:nth-of-type(odd):hover,.item.list-group-item:hover
            {
                background: #428bca;
            }
            .item.list-group-item .list-group-image
            {
                margin-right: 10px;
            }
            .item.list-group-item .thumbnail
            {
                margin-bottom: 5px;
            }
            .item.list-group-item .caption
            {
                padding: 9px 9px 0px 9px;
            }
            .item.list-group-item:nth-of-type(odd)
            {
                background: #eeeeee;
            }

            .item.list-group-item:before, .item.list-group-item:after
            {
                display: table;
                content: " ";
            }

            .item.list-group-item img
            {
                float: left;
            }
            .item.list-group-item:after
            {
                clear: both;
            }
            .list-group-item-text
            {
                margin: 0 0 11px;
            }
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
</head>

<body>
  <div class="col-lg-12 my-3">
    <div class="float-right">
        <div class="btn-group">
            <a href="#" id="list" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-th-list"></span>List</a> 
            <a href="#" id="grid" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-th"></span>Grid</a>
        </div>
    </div>
  </div>
  <div class="container" style=" max-width:90%;">
    
    <div id="products" class="row view-group">
        <?php
            $find =  $_GET['find'];
            $mySql = "SELECT * FROM products WHERE title LIKE '%$find%' OR galery LIKE '%$find%' OR categorychar LIKE '%$find%' OR sellername LIKE '%$find%'";
			$myQry = mysqli_query($koneksidb, $mySql)  or die ("Query salah : ".mysql_error());
			while ($myData = mysqli_fetch_array($myQry)) {
		?>           
        <div class="item box1 col-xs-4 col-lg-3" style="margin-bottom: 20px;">
            <div class="thumbnail" style="text-align: center;margin-bottom: 30px;margin-top: 20px;">
                <a class="group list-group-image img-link img-wrap w_hover text-center"  href="./?page=detail-collection&id=<?php echo $myData['id'] ?>"> <img class="img-thumbnail img-fluid"  alt="" src="./public/product_images/<?php echo $myData['id'] ?>/<?php echo $myData['image'] ?>" style="width: auto; height:200px; text-align: center;"> <span class="link-icon"></span> </a> 
                <div class="caption" style="font-family: Arial Bold;  line-height: 0.5;margin-top: 10px;">
                        <h3 class="group inner list-group-item-heading" style="margin-bottom: 0px; font-size:17px"><?php echo $myData['artist'] ?></h3>
                        <h4 class="group inner  " style="margin-top: 5px;"><i>"<?php echo $myData['title'] ?>"</i></h4>
                        <p class="group inner"><?php echo $myData['ukuran'] ?></p>
                        <p class="group inner list-group-item-heading"><?php echo $myData['media'] ?></p>
                        <p class="group inner list-group-item-heading"><?php echo $myData['tahun'] ?></p>
                         <p><i><div class="<?php echo $myData['id'] ?>"><?php echo $myData['price'] ?></div> </i> <span class="add-on" id="loanAmount1Cur"></span></p>    
                         <div><i class="far fa-heart float-center" ></i></div>
                    <p class="group inner list-group-item-text">
                 </div>
                 <div class="row" style="text-align: center">
                        <div class="col-xs-12">
              </div>
             </div>
            </div>
        </div>
          <?php }; ?>     
    </div>
</div>



</div>

<footer class="page-footer font-small pt-4 bottom">
<hr style="background: #fff;">
    <!-- Footer Links -->
    <div class="container-fluid text-center text-md-left">
      <!-- Grid row -->
      <div class="row">
        <!-- Grid column -->
        <div class="col-md-6 mt-md-0 text-center">
          <!-- Content -->
          <h3>Joeda Art</h3>
          <p>Situs Jual Beli Karya Seni</p>
              <!--Instagram-->
              <a class="ins-ic mr-3" role="button"><i class="fab fa-lg fa-instagram"></i></a>
              <!--Facebook-->
              <a class="fb-ic mr-3" role="button"><i class="fab fa-lg fa-facebook-f"></i></a>
              <!--Twitter-->
              <a class="tw-ic mr-3" role="button"><i class="fab fa-lg fa-twitter"></i></a>
              <!--Youtube-->
              <a class="yt-ic mr-3" role="button"><i class="fab fa-lg fa-youtube"></i></a>
              <!--Slack-->
        </div>

        <!-- Grid column -->
        <div class="col-md-3 mb-md-0 mb-3 mx-auto">
            <!-- Links -->
            <h5 class="text-uppercase"></h5>
            <ul class="list-unstyled">
              <li>
                <a href="?page=pusat-bantuan">Pusat Bantuan</a>
              </li>
              <li>
                <a href="?page=syarat-ketentuan">Syarat & Ketentuan</a>
              </li>
              <li>
                <a href="?page=kebijakan-privasi">Kebijakan Privasi</a>
              </li>
              <li>
                <a href="#!">About Joeda Art</a>
              </li>
            </ul>
          </div>
      </div>
      <!-- Grid row -->
    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">Copyright © 2019 <b>Joeda Art</b>
    </div>
    <!-- Copyright -->
  </footer>
  <!-- Footer -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
  crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.js"></script>
<script src="./public/js/main.js"></script>
<!-- </body> </html> -->
</body>
</html>

<script type="text/javascript">
    
    function ubahnegara(){
         $("img#load1").show();
         var id_countries = 1;//$(this.target).val(); 
         $.ajax({
            type: "POST",
            dataType: "html",
            url: "data-wilayah.php?jenis=provinsi",
            data: "id_countries="+id_countries,
            success: function(msg){
               $("select#provinsi").html(msg);                                                       
               $("img#load1").hide();
               getAjaxProvince();                                                        
            }
         });                    
       }; 


    function getAjaxProvince(){
         $("img#load1").show();
         var id_provinces = 32;//$(this.target).val(); 
         $.ajax({
            type: "POST",
            dataType: "html",
            url: "data-wilayah.php?jenis=kota",
            data: "id_provinces="+id_provinces,
            success: function(msg){
               $("select#kota").html(msg);                                                       
               $("img#load1").hide();
               getAjaxKota();                                                        
            }
         });                    
       }; 

       function getAjaxKota(){
            $("img#load2").show();
            var id_regencies = $("#kota").val();
            $.ajax({
               type: "POST",
               dataType: "html",
               url: "data-wilayah.php?jenis=kecamatan",
               data: "id_regencies="+id_regencies,
               success: function(msg){
                  $("select#kecamatan").html(msg);                              
                  $("img#load2").hide(); 
                 //getAjaxKecamatan();                                                    
               }
            });
       }
    


    function searching(){
      var n1 = document.getElementById("caritext").value;
      location.href =  "./?page=searching&find=" + n1 ;            
    };

  $(document).ready(function(){
     
   $("#provinsi").change(function(){
         $("img#load1").show();
         var id_provinces = $(this).val(); 
         $.ajax({
            type: "POST",
            dataType: "html",
            url: "data-wilayah.php?jenis=kota",
            data: "id_provinces="+id_provinces,
            success: function(msg){
               $("select#kota").html(msg);                                                       
               $("img#load1").hide();
               getAjaxKota();                                                        
            }
         });                    
       });  
   
   $("#kota").change(getAjaxKota);
       function getAjaxKota(){
            $("img#load2").show();
            var id_regencies = $("#kota").val();
            $.ajax({
               type: "POST",
               dataType: "html",
               url: "data-wilayah.php?jenis=kecamatan",
               data: "id_regencies="+id_regencies,
               success: function(msg){
                  $("select#kecamatan").html(msg);                              
                  $("img#load2").hide(); 
                 getAjaxKecamatan();                                                    
               }
            });
       }
   
       $("#kecamatan").change(getAjaxKecamatan);
       function getAjaxKecamatan(){
            $("img#load3").show();
            var id_district = $("#kecamatan").val();
            $.ajax({
               type: "POST",
               dataType: "html",
               url: "data-wilayah.php?jenis=kelurahan",
               data: "id_district="+id_district,
               success: function(msg){
                  $("select#kelurahan").html(msg);                              
                  $("img#load3").hide();                                                 
               }
            });
       }


       $('[data-toggle="popover"]').popover({ 
        html : true,
        content: function() {
        return $('#popover_content_wrapper').html();
      }
    });


    $("#myCarousel").on("slide.bs.carousel", function(e) {
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 3;
    var totalItems = $(".carousel-item").length;

    if (idx >= totalItems - (itemsPerSlide - 1)) {
      var it = itemsPerSlide - (totalItems - idx);
      for (var i = 0; i < it; i++) {
        // append slides to end
        if (e.direction == "left") {
          $(".carousel-item")
            .eq(i)
            .appendTo(".carousel-inner");
        } else {
          $(".carousel-item")
            .eq(0)
            .appendTo($(this).find(".carousel-inner"));
        }
      }
    }
  });
});

function viewprovince(){
        var dropdown = document.getElementById("cmbcountry");
				var selection = dropdown.value;
        console.log(selection);
    };

 </script>