<style>
      /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
      .glyphicon { margin-right:5px; }
            .thumbnail
            {
                margin-bottom: 0px;
                padding: 0px;
                -webkit-border-radius: 0px;
                -moz-border-radius: 0px;
                border-radius: 0px;
            }

            .list-thumnail{
                margin-bottom: 30px;
                margin-top: 20px;
            }

            .group-thumnail{
                margin-bottom: 30px;
                
            }

            .text-centertea{
                text-align:center;
            }

            .text-kanantea{
                text-align:left;
            }

            .img-sizegroup{
                margin-top: 5px;
                margin-bottom: 5px;
                margin-right: 5px;
                margin-left: 5px;
                width: auto; 
                height:320px; 
            }

            .img-sizelist{
                width: auto; 
                height:250px; 
                text-align: center;
            }

            .item.list-group-item img {
                    max-height: 320px;
                    max-width: auto;
            }

            .item.list-group-item{
                float: none;
                width: 100%;
                background-color: #fff;
                margin-bottom: 10px;
            }

            .item.list-group-item:nth-of-type(odd):hover,.item.list-group-item:hover
            {
                /*background: #428bca;*/
            }

            .item.list-group-item .list-group-image
            {
                margin-right: 10px;
            }

            .item.list-group-item .thumbnail
            {
                margin-bottom: 5px;
            }

            .item.list-group-item .caption
            {
                padding: 9px 9px 0px 9px;
            }

            .item.list-group-item:nth-of-type(odd)
            {
                /*background: #eeeeee;*/
            }

            .item.list-group-item:before, .item.list-group-item:after
            {
                display: table;
                content: " ";
            }

            .item.list-group-item img
            {
                float: left;
            }

            .item.list-group-item:after
            {
                clear: both;
            }

            .list-group-item-text
            {
                margin: 0 0 11px;
            }

            .ratakiri-off{
                
            }
            
            .ratakiri-on{
                margin-left:450px;
            }

            .artist-font-off{
                font-size:17px;
            }
            .title-font-off{
                font-size:16px;
            }
            .ukuran-font-off{
                font-size:16px;
            }
            .media-font-off{    
                font-size:16px;
            }
            .tahun-font-off{
                font-size:16px;
            }
            .price-font-off{    
                font-size:16px;
            }

            .artist-font-on{
                font-size:24px;
                margin-bottom:16px;
            }
            .title-font-on{
                font-size:20px;
                margin-bottom:16px;
            }
            .ukuran-font-on{
                font-size:19px;
                margin-bottom:16px;
            }
            .media-font-on{    
                font-size:19px;
                margin-bottom:16px;
            }
            .tahun-font-on{
                font-size:19px;
                margin-bottom:16px;
            }
            .price-font-on{    
                font-size:19px;
                margin-bottom:16px;
            }

            .caption-margin{
                margin-top: 10px;
            }

            .caption-margin-grid{
                margin-top: -25px;
            }
    </style>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
</head>

<body>
  <div class="col-lg-12 my-3">
    <div class="float-right">
        <div class="btn-group">
            <a href="#" id="list" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-th-list"></span>List</a> 
            <a href="#" id="grid" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-th"></span>Grid</a>
        </div>
    </div>
  </div>
  <div class="container" style=" max-width:90%;">
    
    <div id="products" class="row view-group">
        <?php
            $mySql = "SELECT * FROM products WHERE category=" . $_GET['id'];
			$myQry = mysqli_query($koneksidb, $mySql)  or die ("Query salah : ".mysql_error());
			while ($myData = mysqli_fetch_array($myQry)) {
		?>           
        <div class="item box1 col-xs-4 col-lg-3" style="">
            <div class="thumbnail list-thumnail text-centertea" >
               
            <?php
            $filename = './public/product_images/'.$myData['id'].'/'.$myData['image'];
            if (!file_exists($filename)) {
            ?>
                    <a class="group list-group-image img-link img-wrap w_hover text-center" href="./?page=detail-collection&id=<?php echo $myData['id'] ?>"> <img class="img img-thumbnail img-fluid img-sizelist"  alt="" src="./public/product_images/noimage.png"> <span class="link-icon"></span> </a> 
            <?php
            } else
            {
            ?>
                    <a class="group list-group-image img-link img-wrap w_hover text-center" href="./?page=detail-collection&id=<?php echo $myData['id'] ?>"> <img class="img img-thumbnail img-fluid img-sizelist"  alt="" src="./public/product_images/<?php echo $myData['id'] ?>/<?php echo $myData['image'] ?>"> <span class="link-icon"></span> </a> 
            <?php
            }
            ?>
               
                <div class="caption caption-margin" style="font-family: Arial Bold;  line-height: 0.5;">
                        <h3 class="group1 inner ratakiri-off artist-font-off" style="margin-bottom: 0px;"><?php echo $myData['artist'] ?></h3>
                        <h4 class="group2 inner ratakiri-off title-font-off" style="margin-top: 5px;"><i>"<?php echo $myData['title'] ?>"</i></h4>
                        <p  class="group3 inner ratakiri-off ukuran-font-off"><?php echo $myData['ukuran'] ?></p>
                        <p  class="group4 inner ratakiri-off media-font-off"><?php echo $myData['media'] ?></p>
                        <p  class="group5 inner ratakiri-off tahun-font-off">( <?php echo $myData['tahun'] ?> )</p>
                        <p  class="group inner ratakiri-off price-font-off"><i><div class="<?php echo $myData['id'] ?> group6 inner ratakiri-off" ><?php echo $myData['price'] ?></div></i><span class="add-on" id="loanAmount1Cur"></span></p>    
                        <?php 
                            $mySqlfavorite = "SELECT * FROM favorites WHERE product='" . $myData['id'] . "' AND user='". $_SESSION['SESLOGINJA'] ."' AND flag=1";
                            //echo $mySqlfavorite;
                            $myQryfavorite = mysqli_query($koneksidb, $mySqlfavorite)  or die ("Query salah : ".mysql_error());
                            $myDatafavorite = mysqli_fetch_array($myQryfavorite);
                            $myDatafavoritenum = mysqli_num_rows($myQryfavorite);
                            $favblack ='black_'.$_SESSION['SESLOGINJA'].'_'.$myData['id'];
                            $favwhite ='white_'.$_SESSION['SESLOGINJA'].'_'.$myData['id'];
                            $favblackclick = $_SESSION['SESLOGINJA'].'_'.$myData['id'];
                            $favwhiteclick = $_SESSION['SESLOGINJA'].'_'.$myData['id'];
                    
                        ?>
                        <div class="logoo ratakiri-off">
                            <div id="<?php echo  'div'.$favblack?>" style="display:none;" > <button type="button" onclick="klikfavoriteblack('<?php echo $favwhiteclick;?>')" id="<?php echo $favblack;?>"  style="border:none;padding:0;" class="btn btn-default"><i class="fas fa-heart float-center" style="color:red"></i></button></div>
                            <div id="<?php echo  'div'.$favwhite?>" style="display:block;" ><button type="button" onclick="klikfavoritewhite('<?php echo $favblackclick;?>')" id="<?php echo $favwhite;?>"  style="border:none;padding:0;" class="btn btn-default"><i class="far fa-heart float-center"></i></button></div>
                        </div>
                         <p class="group inner list-group-item-text">
                 </div>
                 <div class="row" style="text-align: center">
                        <div class="col-xs-12">
              </div>
             </div>
            </div>
        </div>
          <?php }; ?>     
    </div>
</div>

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js'></script>


<script type="text/javascript">

function klikfavoriteblack(ids){
    document.getElementById('divwhite_'+ids).style.display='block';
    document.getElementById('divblack_'+ids).style.display='none';
}


function klikfavoritewhite(ids){
    document.getElementById('divblack_'+ids).style.display='block';
    document.getElementById('divwhite_'+ids).style.display='none';
}



  $(document).ready(function() {
    $('#list').click(function(event){event.preventDefault();
        $('#products .item').addClass('list-group-item');
        $('#products .item').removeClass('grid-group-item');
        $('#products .item').removeClass('col-lg-3');

        $('#products .img').addClass('img-sizegroup');
        $('#products .img').removeClass('img-sizelist');

        $('#products .thumbnail').addClass('group-thumnail');
        $('#products .thumbnail').removeClass('list-thumnail');

        $('#products .thumbnail').removeClass('text-centertea');
        $('#products .thumbnail').addClass('text-kanantea');
        
        $('#products .group').removeClass('ratakiri-off');
        $('#products .group').addClass('ratakiri-on');


      

        $('#products .group1').addClass('artist-font-on');
        $('#products .group2').addClass('title-font-on');
        $('#products .group3').addClass('ukuran-font-on');
        $('#products .group4').addClass('media-font-on');
        $('#products .group5').addClass('tahun-font-on');
        $('#products .group6').addClass('price-font-on');

        $('#products .group1').removeClass('artist-font-off');
        $('#products .group2').removeClass('title-font-off');
        $('#products .group3').removeClass('ukuran-font-off');
        $('#products .group4').removeClass('media-font-off');
        $('#products .group5').removeClass('tahun-font-off');
        $('#products .group6').removeClass('price-font-off');

        $('#products .group1').addClass('ratakiri-on');
        $('#products .group2').addClass('ratakiri-on');
        $('#products .group3').addClass('ratakiri-on');
        $('#products .group4').addClass('ratakiri-on');
        $('#products .group5').addClass('ratakiri-on');
        $('#products .group6').addClass('ratakiri-on');

        $('#products .group1').removeClass('ratakiri-off');
        $('#products .group2').removeClass('ratakiri-off');
        $('#products .group3').removeClass('ratakiri-off');
        $('#products .group4').removeClass('ratakiri-off');
        $('#products .group5').removeClass('ratakiri-off');
        $('#products .group6').removeClass('ratakiri-off');

        $('#products .caption').removeClass('caption-margin');
        $('#products .caption').addClass('caption-margin-grid');

        $('#products .logoo').addClass('ratakiri-on');
        $('#products .logoo').removeClass('ratakiri-off');
      
     
    });
    
    $('#grid').click(function(event){event.preventDefault();
        $('#products .item').removeClass('list-group-item');
        $('#products .item').addClass('grid-group-item');
        $('#products .item').addClass('col-lg-3');

        $('#products .img').removeClass('img-sizegroup');
        $('#products .img').addClass('img-sizelist');

        $('#products .thumbnail').removeClass('group-thumnail');
        $('#products .thumbnail').addClass('list-thumnail');
 
         $('#products .thumbnail').addClass('text-centertea');
         $('#products .thumbnail').removeClass('text-kanantea');

         $('#products .group ').addClass('ratakiri-off');
        $('#products .group ').removeClass('ratakiri-on');

              
        $('#products .group1').removeClass('artist-font-on');
        $('#products .group2').removeClass('title-font-on');
        $('#products .group3').removeClass('ukuran-font-on');
        $('#products .group4').removeClass('media-font-on');
        $('#products .group5').removeClass('tahun-font-on');
        $('#products .group6').removeClass('price-font-on');

        $('#products .group1').addClass('artist-font-off');
        $('#products .group2').addClass('title-font-off');
        $('#products .group3').addClass('ukuran-font-off');
        $('#products .group4').addClass('media-font-off');
        $('#products .group5').addClass('tahun-font-off');
        $('#products .group6').addClass('price-font-off');

        $('#products .group1').removeClass('ratakiri-on');
        $('#products .group2').removeClass('ratakiri-on');
        $('#products .group3').removeClass('ratakiri-on');
        $('#products .group4').removeClass('ratakiri-on');
        $('#products .group5').removeClass('ratakiri-on');
        $('#products .group6').removeClass('ratakiri-on');

        $('#products .group1').addClass('ratakiri-off');
        $('#products .group2').addClass('ratakiri-off');
        $('#products .group3').addClass('ratakiri-off');
        $('#products .group4').addClass('ratakiri-off');
        $('#products .group5').addClass('ratakiri-off');
        $('#products .group6').addClass('ratakiri-off');

        $('#products .caption').addClass('caption-margin');
        $('#products .caption').removeClass('caption-margin-grid');

        $('#products .logoo').addClass('ratakiri-off');
        $('#products .logoo').removeClass('ratakiri-on');
    });
    
     $("#loanAmount1").on("new", null, function () {
                  var input = $("#loanAmount1").val();
                  var num = parseFloat(input).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') + " $";
                  $("#loanAmount1Cur").html(num);
              });

            //   $('.far').click(function () {
            //       console.log('fas0');
            //         $('.far').toggleClass('far fa-heart fas fa-heart')  
            //       });
                  
            //       $('.fas').click(function () {
            //       console.log('fas1');
            //         $('.fas').toggleClass('fas fa-heart far fa-heart')  
            //       });
            
                <?php
                        $mySql = "SELECT * FROM products WHERE category=" . $_GET['id'];
			            $myQry = mysqli_query($koneksidb, $mySql)  or die ("Query salah : ".mysql_error());
			            while ($myData = mysqli_fetch_array($myQry)) {
		         ?>           
                          var num = $('div.<?php echo $myData['id'] ?>').text()
                          num = addPeriod(num);
                          $('div.<?php echo $myData['id'] ?>').text('IDR '+num)
                <?php
                        };
                ?>
 
  });


    function addPeriod(nStr)
              {
                  nStr += '';
                  x = nStr.split('.');
                  x1 = x[0];
                  x2 = x.length > 1 ? '.' + x[1] : '';
                  var rgx = /(\d+)(\d{3})/;
                  while (rgx.test(x1)) {
                      x1 = x1.replace(rgx, '$1' + '.' + '$2');
                  }
                  return x1 + x2;
              }

</script>

