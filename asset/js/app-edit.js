$(document).ready(function() {

$('.reorder').on('click',function(){
          $("ul.nav").sortable({ tolerance: 'pointer' });
          $('.reorder').html('Save Reordering');
          $('.reorder').attr("id","updateReorder");
          $('#reorder-msg').slideDown('');
          $('.img-link').attr("href","javascript:;");
          $('.img-link').css("cursor","move");
          
          $("#updateReorder").click(function( e ){
             if(!$("#updateReorder i").length){
                $(this).html('').prepend('<i class="fa fa-spin fa-spinner"></i>');
                $("ul.nav").sortable('destroy');
                $("#reorder-msg").html( "Reordering Photos - This could take a moment. Please don't navigate away from this page." ).removeClass('light_box').addClass('notice notice_error');
        
                var h = [];
                $("ul.nav li").each(function() {  
                   console.log($(this).attr('id'));
                   h.push($(this).attr('id').substr(9));  
                });
                 
                $.ajax({
                   type: "POST",
                   url: ".././ajax/update.php",
                
                   data: {ids: " " + h + ""},
                   success: function(data){
                      if(data==1 || parseInt(data)==1){
                         window.location.reload();
                      }
                   }
                }); 
                return false;
             }       
             e.preventDefault();     
          });
       });
        
       $(function() {
         $("#myDropEdit").sortable({
          items: '.dz-preview',
          cursor: 'move',
          opacity: 0.5,
          containment: '#myDropEdit',
          distance: 20,
          tolerance: 'pointer',
         });
         $("#myDropEdit").disableSelection();
         /**** */
         $("#myDropEdit").sortable({
           items: '.dz-preview',
           cursor: 'move',
           opacity: 0.5,
           containment: '#myDropEdit',
           distance: 20,
           tolerance: 'pointer',
          });
          
          $("#myDropEdit").disableSelection();
      
        });
        
       //Dropzone script
       Dropzone.autoDiscover = false;

       var myDropzoneEdit = new Dropzone("div#myDropEdit", 
       { 
           paramName: "files", // The name that will be used to transfer the file
           addRemoveLinks: true,
           uploadMultiple: true,
           autoProcessQueue: false,
           parallelUploads: 50,
           maxFilesize: 5, // MB
           acceptedFiles: ".png, .jpeg, .jpg, .gif",
           url: ".././ajax/action-z.ajax.php",
       });
        
       myDropzoneEdit.on("sending", function(file, xhr, formData) {
         var filenames = [];
          
         $('.dz-preview .dz-filename').each(function() {
          filenames.push($(this).find('span').text());
         });

         formData.append('title', document.getElementById('title').value);
         formData.append('sellerid', document.getElementById('sellerid').value);
         formData.append('artist', document.getElementById('artist').value);
         formData.append('category', document.getElementById('category').value);
         formData.append('descs', document.getElementById("description").value);
         formData.append('price', document.getElementById("price").value);
         formData.append('discount', document.getElementById("discount").value);
         formData.append('negotiable', document.getElementById("negotiable").value);
         formData.append('media', document.getElementById("media").value);
         formData.append('tahun', document.getElementById("year").value);
         formData.append('ukuran', document.getElementById("size").value);
         formData.append('active', document.getElementById("active").value);
         formData.append('randgenerate',document.getElementById('codeproduct').value);
         formData.append('filenames', filenames);
       });
        
       /* Add Files Script*/
       myDropzoneEdit.on("success", function(file, message){
          $("#msg").html(message);
          //setTimeout(function(){window.location.href="index.php"},200);
           setTimeout(function(){window.location.href="?page=adsja-data&id="+document.getElementById('sellerid').value},200);
       });
         
       myDropzoneEdit.on("error", function (data) {
           $("#msg").html('<div class="alert alert-danger">There is some thing wrong, Please try again!</div>');
       });
         
       myDropzoneEdit.on("complete", function(file) {
          myDropzoneEdit.removeFile(file);
       });
         
       $("#editadd_file").on("click",function (){
         alert('e');
        if (document.getElementById('category').value === "NULL") {
           alert("Category is empty");
           return false;
         }
         else
         if (document.getElementById('artist').value === "") {
           alert("Artist is empty");
           return false;
         }
         else   
         if (document.getElementById('title').value === "") {
           alert("Title is empty");
           return false;
         }
         else
         if (document.getElementById('size').value === "") {
           alert("Size is empty");
           return false;
         }
         else
         if (document.getElementById('media').value === "") {
           alert("Media is empty");
           return false;
         }
         else
         if (document.getElementById('description').value === "") {
           alert("Description is empty");
           return false;
         }
         else
         if (document.getElementById('discount').value === "") {
           alert("Discount is empty");
           return false;
         }
         else
         if (document.getElementById('year').value === "") {
           alert("Year is empty");
           return false;
         } else
         if (myDropzoneEdit.getUploadingFiles().length === 0 && myDropzoneEdit.getQueuedFiles().length === 0) {      
           alert('No file selected for upload');  
           return false;
         }
         else {
           /* Remove event listener and start processing */ 
           myDropzoneEdit.removeEventListeners();
           myDropzoneEdit.processQueue(); 
           
         }
         
});

});