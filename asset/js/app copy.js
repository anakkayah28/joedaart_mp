$( document ).ready(function() {
   
   // //untuk memanggil plugin select2
   //  $('#provinsi').select2({
   //    placeholder: 'Pilih Provinsi',
   //    language: "id"
   // });
   // $('#kota').select2({
   //    placeholder: 'Pilih Kota/Kabupaten',
   //    language: "id"
   // });
   // $('#kecamatan').select2({
   //    placeholder: 'Pilih Kecamatan',
   //    language: "id"
   // });
   // $('#kelurahan').select2({
   //    placeholder: 'Pilih Kelurahan',
   //    language: "id"
   // });
   // //saat pilihan provinsi di pilih, maka akan mengambil data kota
   //di data-wilayah.php menggunakan ajax
   $("#provinsi").change(function(){
         $("img#load1").show();
         var id_provinces = $(this).val(); 
         $.ajax({
            type: "POST",
            dataType: "html",
            url: "data-wilayah.php?jenis=kota",
            data: "id_provinces="+id_provinces,
            success: function(msg){
               $("select#kota").html(msg);                                                       
               $("img#load1").hide();
               getAjaxKota();                                                        
            }
         });                    
       });  
   //saat pilihan kota di pilih, maka akan mengambil data kecamatan
   //di data-wilayah.php menggunakan ajax
   $("#kota").change(getAjaxKota);
       function getAjaxKota(){
            $("img#load2").show();
            var id_regencies = $("#kota").val();
            $.ajax({
               type: "POST",
               dataType: "html",
               url: "data-wilayah.php?jenis=kecamatan",
               data: "id_regencies="+id_regencies,
               success: function(msg){
                  $("select#kecamatan").html(msg);                              
                  $("img#load2").hide(); 
                 getAjaxKecamatan();                                                    
               }
            });
       }
       //saat pilihan kecamatan di pilih, maka akan mengambil data kelurahan
   //di data-wilayah.php menggunakan ajax
       $("#kecamatan").change(getAjaxKecamatan);
       function getAjaxKecamatan(){
            $("img#load3").show();
            var id_district = $("#kecamatan").val();
            $.ajax({
               type: "POST",
               dataType: "html",
               url: "data-wilayah.php?jenis=kelurahan",
               data: "id_district="+id_district,
               success: function(msg){
                  $("select#kelurahan").html(msg);                              
                  $("img#load3").hide();                                                 
               }
            });
       }


//text editor
if ($("textarea#ta").length) {
   CKEDITOR.replace("ta");
 }
 //confirm delete 
 $("a.confirmDeletion").on("click", function() {
   if (!confirm("Are you sure want to delete1 this page?")) return false;
 });

 if ($("[data-fancybox]").length) {
   $("[data-fancybox]").fancybox();
 }

 $("#myCarousel").on("slide.bs.carousel", function(e) {
   var $e = $(e.relatedTarget);
   var idx = $e.index();
   var itemsPerSlide = 3;
   var totalItems = $(".carousel-item").length;

   if (idx >= totalItems - (itemsPerSlide - 1)) {
     var it = itemsPerSlide - (totalItems - idx);
     for (var i = 0; i < it; i++) {
       // append slides to end
       if (e.direction == "left") {
         $(".carousel-item")
           .eq(i)
           .appendTo(".carousel-inner");
       } else {
         $(".carousel-item")
           .eq(0)
           .appendTo($(this).find(".carousel-inner"));
       }
     }
   }
 });

 $('.reorder').on('click',function(){
           $("ul.nav").sortable({ tolerance: 'pointer' });
           $('.reorder').html('Save Reordering');
           $('.reorder').attr("id","updateReorder");
           $('#reorder-msg').slideDown('');
           $('.img-link').attr("href","javascript:;");
           $('.img-link').css("cursor","move");
           
           $("#updateReorder").click(function( e ){
              if(!$("#updateReorder i").length){
                 $(this).html('').prepend('<i class="fa fa-spin fa-spinner"></i>');
                 $("ul.nav").sortable('destroy');
                 $("#reorder-msg").html( "Reordering Photos - This could take a moment. Please don't navigate away from this page." ).removeClass('light_box').addClass('notice notice_error');
         
                 var h = [];
                 $("ul.nav li").each(function() {  
                    console.log($(this).attr('id'));
                    h.push($(this).attr('id').substr(9));  
                 });
                  
                 $.ajax({
                    type: "POST",
                    url: ".././ajax/update.php",
                 
                    data: {ids: " " + h + ""},
                    success: function(data){
                       if(data==1 || parseInt(data)==1){
                          window.location.reload();
                       }
                    }
                 }); 
                 return false;
              }       
              e.preventDefault();     
           });
        });
         
        $(function() {
          $("#myDrop").sortable({
           items: '.dz-preview',
           cursor: 'move',
           opacity: 0.5,
           containment: '#myDrop',
           distance: 20,
           tolerance: 'pointer',
          });
      
          $("#myDrop").disableSelection();
        });
         
        //Dropzone script
        Dropzone.autoDiscover = false;

        var myDropzone = new Dropzone("div#myDrop", 
        { 
            paramName: "files", // The name that will be used to transfer the file
            addRemoveLinks: true,
            uploadMultiple: true,
            autoProcessQueue: false,
            parallelUploads: 50,
            maxFilesize: 5, // MB
            acceptedFiles: ".png, .jpeg, .jpg, .gif",
            url: ".././ajax/actionsaveproducts.php",
        });
         
        myDropzone.on("sending", function(file, xhr, formData) {
          var filenames = [];
           
          $('.dz-preview .dz-filename').each(function() {
           filenames.push($(this).find('span').text());
          });
         
          formData.append('randgenerate',document.getElementById('codeproduct').value);
          formData.append('filenames', filenames);

        });
         
        /* Add Files Script*/
        myDropzone.on("success", function(file, message){
           $("#msg").html(message);
           //setTimeout(function(){window.location.href="index.php"},200);
        });
          
        myDropzone.on("error", function (data) {
            $("#msg").html('<div class="alert alert-danger">There is some thing wrong, Please try again!</div>');
        });
          
        myDropzone.on("complete", function(file) {
           myDropzone.removeFile(file);
        });
          
        $("#add_file").on("click",function (){
           klikadd_file();
           myDropzone.processQueue();
        });
});

function klikadd_file(){
  var category = document.getElementById('category').value; 
  var artist = document.getElementById('artist').value;
  var title = document.getElementById('title').value; 
  var size = document.getElementById("size").value;
  var media = document.getElementById("media").value;
  var description = document.getElementById("description").value;
  var year = document.getElementById("year").value;
  var price = document.getElementById("price").value;
  var discount = document.getElementById("discount").value;
  var negotiable = document.getElementById("negotiable").value;
  var active = document.getElementById("active").value;
  
  var xhr;
  if (window.XMLHttpRequest) { // Mozilla, Safari, ...
     xhr = new XMLHttpRequest();
  } else if (window.ActiveXObject) { // IE 8 and older
     xhr = new ActiveXObject("Microsoft.XMLHTTP");
  }
  var data = "aplikasi=1put";
                xhr.open("POST", "../ajax/saveproduct.php?category=" + category + 
                "&artist=" + artist + 
                "&title=" + title + 
                "&size=" + size + 
                "&media=" + media + 
                "&description=" + description + 

                "&year=" + year + 
                "&price=" + price + 
                "&discount=" + discount + 
                "&negotiable=" + negotiable + 

                "&active=" + active, true); 
     xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");                  
     xhr.send(data);
    xhr.onreadystatechange = display_data;
    function display_data() {
    if (xhr.readyState == 4) {
       if (xhr.status == 200) {
       //alert(xhr.responseText);	   
       //document.getElementById("pemesananobatdivid").innerHTML = xhr.responseText;

       } else {
        alert('There was a problem with the request.');
       }
     }
    }

   }