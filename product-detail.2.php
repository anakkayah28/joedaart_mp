    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
   
   <style>
   .carousel-control-prev-icon,
    .carousel-control-next-icon {
     
      background-image: none;
    }

    .carousel-control-next-icon:after
    {
      content: '>';
      font-size: 55px;
      color: red;
    }

    .carousel-control-prev-icon:after {
      content: '<';
      font-size: 55px;
      color: red;
    }

    .button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  text-decoration: none;
  display: inline-block;
  font-size: 14px;
  margin: 4px 2px;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
  cursor: pointer;
}

.button1 {
  background-color: white; 
  color: black; 
  border: 2px solid #4CAF50;
}

.button1:hover {
  background-color: #4CAF50;
  color: white;
}

.button2 {
  background-color: white; 
  color: black; 
  border: 2px solid #008CBA;
}

.button2:hover {
  background-color: #008CBA;
  color: white;
}
.button4 {
  background-color: white;
  color: black;
  border: 2px solid #e7e7e7;
}

.button4:hover {background-color: #e7e7e7;}

.carousel-control-next-icon:after {color:black;}
.carousel-control-prev-icon:after {color:black;}


ol li.active {
  width: 100%;
  height: 100%;
  border-radius: 10px;
  padding: 10px;
  border: 2px solid #44492e; 
}

ol li.active:hover {
  width: 100%;
  height: 100%;
  border-radius: 10px;
  padding: 10px;
  border: 2px solid #44492e; 
}


   </style>

    <?php
            $mySql = "SELECT * FROM products WHERE id=" . $_GET['id'];
			$myQry = mysqli_query($koneksidb, $mySql)  or die ("Query salah : ".mysql_error());
			while ($myData = mysqli_fetch_array($myQry)) {
	?>           

      <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">

                    <div class="ibox product-detail">
                        <div class="ibox-content">

                            <div class="row">
                                <div class="col-md-5 mr-3 ml-3">
                                <!--Carousel Wrapper-->

                                        <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel">
                                          <!--Slides-->
                                          <div class="carousel-inner" role="listbox">
                                            <div class="carousel-item active">
                                              <img class="d-block w-100" src="./public/product_images/<?php echo $myData['id'] ?>/<?php echo $myData['image'] ?>"
                                                alt="First slide">
                                            </div>
                                            <div class="carousel-item">
                                              <img class="d-block w-100" src="./public/product_images/<?php echo $myData['id'] ?>/<?php echo $myData['image'] ?>"
                                                alt="Second slide">
                                            </div>
                                            <div class="carousel-item">
                                              <img class="d-block w-100" src="./public/product_images/<?php echo $myData['id'] ?>/<?php echo $myData['image'] ?>"
                                                alt="Third slide">
                                            </div>
                                              <div class="carousel-item">
                                              <img class="d-block w-100" src="./public/product_images/<?php echo $myData['id'] ?>/<?php echo $myData['image'] ?>"
                                                alt="Third slide">
                                            </div>
                                              <div class="carousel-item">
                                              <img class="d-block w-100" src="./public/product_images/<?php echo $myData['id'] ?>/<?php echo $myData['image'] ?>"
                                                alt="Third slide">
                                            </div>
                                          
                                              <div class="carousel-item">
                                              <img class="d-block w-100" src="./public/product_images/<?php echo $myData['id'] ?>/<?php echo $myData['image'] ?>"
                                                alt="Third slide">
                                            </div>
                                          </div>
                                          <!--/.Slides-->
                                          <!--Controls-->
                                          <a class="carousel-control-prev" style="margin-left: -35px;" href="#carousel-thumb" role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon" style="margin-left: -80px;margin-top:-100px"></span>
                                            <span class="sr-only">Previous</span>
                                          </a>
                                          <a class="carousel-control-next" style="margin-right: -25px;" href="#carousel-thumb" role="button" data-slide="next">
                                            <span class="carousel-control-next-icon" style="margin-right: -80px;margin-top:-100px"></span>
                                            <span class="sr-only">Next</span>
                                          </a>
                                           <!--/.Controls-->
                                          <div>
                                            <ol class="carousel-indicators" style="bottom:auto;margin-top:1rem;">
                                              <li data-target="#carousel-thumb" data-slide-to="0" class="active" style="width:auto;margin-left:10px;"><div class="img-hover"><img class="d-block w-100 img-responsive img-rounded"  
                                                  src="./public/product_images/<?php echo $myData['id'] ?>/<?php echo $myData['image'] ?>"  class="img-fluid" ></div>
                                              </li>
                                              <li data-target="#carousel-thumb" data-slide-to="1" style="width:auto; margin-left:10px;"><div class="img-hover"><img class="d-block w-100 img-responsive img-rounded"
                                                  src="./public/product_images/<?php echo $myData['id'] ?>/<?php echo $myData['image'] ?>" class="img-fluid"></div>
                                              </li>
                                              <li data-target="#carousel-thumb" data-slide-to="2" style="width:auto; margin-left:10px;"><div class="img-hover"><img class="d-block w-100 img-responsive img-rounded"
                                                  src="./public/product_images/<?php echo $myData['id'] ?>/<?php echo $myData['image'] ?>" class="img-fluid"></div>
                                              </li>
                                              <li data-target="#carousel-thumb" data-slide-to="3" style="width:auto; margin-left:10px;"><div class="img-hover"><img class="d-block w-100 img-responsive img-rounded"
                                                  src="./public/product_images/<?php echo $myData['id'] ?>/<?php echo $myData['image'] ?>" class="img-fluid"></div>
                                              </li>
                                              <li data-target="#carousel-thumb" data-slide-to="4" style="width:auto; margin-left:10px;"><div class="img-hover"><img class="d-block w-100 img-responsive img-rounded"
                                                  src="./public/product_images/<?php echo $myData['id'] ?>/<?php echo $myData['image'] ?>" class="img-fluid"></div>
                                              </li>
                                              <li data-target="#carousel-thumb" data-slide-to="5" style="width:auto; margin-left:10px;"><div class="img-hover"><img class="d-block w-100 img-responsive img-rounded"
                                                  src="./public/product_images/<?php echo $myData['id'] ?>/<?php echo $myData['image'] ?>" class="img-fluid"></div>
                                              </li>
                                            </ol>
                                          </div>
                                          <br><br><br>
                                        </div>
                                  <!--/.Carousel Wrapper-->
                                </div>
                                <div class="col-md-4 ml-5" style=" font-family: Times New Roman, Times, serif;">
                                    <h1 class="font-bold m-b-xs mt-0">
                                       <b><?php echo $myData['artist'] ?></b>
                                    </h1>
                                     <h2 class="font-bold m-b-xs mt-0 mb-4"><i>"<?php echo $myData['title'] ?>"</i></h2>
                                     <h2 class="mb-1"><?php echo $myData['ukuran'] ?></h2>

						                         <h2 class="mb-1 "><?php echo $myData['media'] ?></h2>
						                         
                                     <h2>( <?php echo $myData['tahun'] ?> )</h2> 
                                    <div class="m-t-md">
                                      <b>
                                        <h2><div class="price font-bold m-b-xs mt-0 "><?php echo $myData['price'] ?></div></h3>
                                      </b>  
                                    </div>
                                    <div style="font-size:20px;font-family: Calibri;"><u>Description :</u></div>
                                    <div style="font-size:18px;font-family: Calibri;">
                                    <?php echo $myData['descs'] ?>
                                    </div>
                                     <hr>
                                    <div style="margin-left:-10px;font-family: Calibri;">
                                        <div>
                                            <a class="btn btn-white btn-sm mb-2 button button4" style="border: 2px;width:250px;text-align:left;font-size:20px;color:#676a6c;" href="tel:+62123456789" target="_blank">Phone   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<?php echo $myData['phone'] ?></a>
                                        </div>
                                        <div>
                                        <a class="btn btn-white btn-sm mb-2 button button4" style="border: 2px;width:250px;text-align:left;font-size:20px;color:#676a6c;" href="sms:+62123456789">SMS &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<?php echo $myData['sms'] ?></a>
                                        </div>
                                        <div>
                                        <a class="btn btn-white btn-sm mb-2 button button4" style="border: 2px;width:250px;text-align:left;font-size:20px;color:#676a6c;" href="https://api.whatsapp.com/send?phone=<?php echo $myData['wa'] ?>" target="_blank">Whatsapp&nbsp;&nbsp;:&nbsp;&nbsp;<?php echo $myData['wa'] ?></a>
                                        </div>
                                        <div>
                                        <a class="btn btn-white btn-sm mb-2 button button4" style="border: 2px;width:250px;text-align:left;font-size:20px;color:#676a6c;"  href="mailto:abc@example.com?subject = Feedback&body = Message" target="_blank">Email &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<?php echo $myData['email'] ?></a>
                                        </div>
                                    </div>
                                      </div>
                                </div>
                            </div>

                        </div>
                
                    </div>

                </div>
            </div>

        <?php
            }; 
        ?>

   <script type="text/javascript">
      
     $(document).ready(function () {
  
      
               var num = $('div.price').text()
            num = addPeriod(num);
            $('div.price').text('IDR '+num)
    
});


function addPeriod(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}

      </script>
  
