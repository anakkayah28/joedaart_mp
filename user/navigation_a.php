<?php

# KONTROL MENU PROGRAM
if($_GET) {
	// Jika mendapatkan variabel URL ?page
	switch($_GET['page']){
		
		case '' :
		if(!file_exists ("./dashboard/dashboard.php")) die ("Access Denied"); 
		include "./dashboard/dashboard.php";	break;
		
		case 'category' :
			if(!file_exists ("category.php")) die ("Access Denied"); 
			include "category.php";	break;
			
		 case 'collections' :
		 	if(!file_exists ("products.php")) die ("Access Denied"); 
			include "products.php";	break;
			 
		case 'kebijakan-privasi' :
			if(!file_exists ("kebijakanprivasi.php")) die ("Access Denied"); 
			include "kebijakanprivasi.php";	break;

		case 'pusat-bantuan' :
			if(!file_exists ("pusatbantuan.php")) die ("Access Denied"); 
			include "pusatbantuan.php";	break;
		
		case 'syarat-ketentuan' :
			if(!file_exists ("syaratketentuan.php")) die ("Access Denied"); 
			include "syaratketentuan.php";	break;

		case 'detail-collection' :
			if(!file_exists ("product-detail.php")) die ("Access Denied"); 
			include "product-detail.php";	break;

		case 'searching' :
			if(!file_exists ("search.php")) die ("Access Denied"); 
			include "search.php";	break;
			
		case 'logout' :
			if(!file_exists ("login_out.php")) die ("Access Denied"); 
			include "login_out.php"; break;		

		case 'adsja-data' :
			if(!file_exists ("./ads/ads_data.php")) die ("Access Denied"); 
			include "./ads/ads_data.php"; break;		

		case 'adsja-add' :
			if(!file_exists ("./ads/ads_add.php")) die ("Access Denied"); 
			//include "./ads/ads_add.php"; break;		
			include "./ads/ads_add.php"; break;		

		case 'adsja-edit' :
			if(!file_exists ("./ads/ads_edit.php")) die ("Access Denied"); 
			include "./ads/ads_edit.php"; break;		

		case 'adsja-delete' :
			if(!file_exists ("./ads/ads_delete.php")) die ("Access Denied"); 
			include "./ads/ads_delete.php"; break;		
		
		case 'Useract-Data' :
			if(!file_exists ("./useract/useract_data.php")) die ("Access Denied"); 
			include "./useract/useract_data.php"; break;		
		
		case 'Advact-Data' :
			if(!file_exists ("./useract/advertise_data.php")) die ("Access Denied"); 
			include "./useract/advertise_data.php"; break;		

		case 'Useract-Suspend' :
			if(!file_exists ("./useract/useract_suspend.php")) die ("Access Denied"); 
			include "./useract/useract_suspend.php"; break;		
		
		case 'Useract-Delete' :
			if(!file_exists ("./useract/useract_deleteuser.php")) die ("Access Denied"); 
			include "./useract/useract_deleteuser.php"; break;		

		case 'Adsact-Suspend' :
			if(!file_exists ("./useract/adsact_suspend.php")) die ("Access Denied"); 
			include "./useract/adsact_suspend.php"; break;		
		
		case 'Adsact-Delete' :
			if(!file_exists ("./useract/adsact_deleteads.php")) die ("Access Denied"); 
			include "./useract/adsact_deleteads.php"; break;		

		case 'Useract-Myaccount' :
			if(!file_exists ("./useract/useract_account.php")) die ("Access Denied"); 
			include "./useract/useract_account.php"; break;		
		

		// case 'admin-i' :
		// 	if(!file_exists ("index-admin.php")) die ("Access Denied"); 
		// 	include "index-admin.php"; break;
			
		# Kategori
		case 'Kategori-Data' :
			if(!file_exists ("kategori/kategori_data.php")) die ("Access Denied"); 
			include "kategori/kategori_data.php"; break;		
		case 'Kategori-Add' :
			if(!file_exists ("kategori/kategori_add.php")) die ("Access Denied"); 
			include"kategori/kategori_add.php"; break;
		case 'Kategori-Add?status=true' :
			if(!file_exists ("kategori/kategori_add.php")) die ("Access Denied"); 
			include"kategori/kategori_add.php"; break;		
		case 'Kategori-Delete' :
			if(!file_exists ("kategori/kategori_delete.php")) die ("Access Denied"); 
			include "kategori/kategori_delete.php"; break;		
		case 'Kategori-Edit' :
			if(!file_exists ("kategori/kategori_edit.php")) die ("Access Denied"); 
			include "kategori/kategori_edit.php"; break;

		// default:
		// 	if(!file_exists ("category.php")) die ("Access Denied"); 
		// 	include "category.php";						
		// break;
	}
}
else {
	// Jika tidak mendapatkan variabel URL : ?page
	if(!file_exists ("category.php")) die ("Access Denied1"); 
	include "category.php";	
}
?>