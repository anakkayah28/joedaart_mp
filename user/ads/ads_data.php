<style>
.grey_color {
    background: #00ff00;
    color: #2e2c2e;
    font-weight: bold;
}
.red_color {
    background: #d40a42;
    color: #fff;
    font-weight: bold;
}
</style>
<?php
include_once "../library/inc.seslogin.php";

# UNTUK PAGING (PEMBAGIAN HALAMAN)
$row = 50;
$hal = isset($_GET['hal']) ? $_GET['hal'] : 0;
$pageSql = "SELECT * FROM products WHERE sellerid='" . $_GET['id'] . "' ORDER BY created_at ASC";
$pageQry = mysqli_query( $koneksidb,$pageSql) or die ("error paging: ".mysql_error());
$jml	 = mysqli_num_rows($pageQry);
$max	 = ceil($jml/$row);
$num_rows = mysqli_num_rows($pageQry);
?>

<div class="ibox-content col-lg-8 mx-auto">

 <div class="stepsBox">
                <div class="claimSteps" id="stepOne">
                     <h1  style=' font-weight: bold;'> <b>ADS</b></h1>  
                </div>
  </div>
<div><a href="./?page=adsja-add&id=<?php  echo $_GET['id']?>" class="btn btn-primary" style="margin-bottom: 10px;width: 235px;  border: 1.5px solid grey;">Add a new Ads</a></div>
<?php if ($num_rows > 0) { ?>
  <?php
    $mySql = "SELECT * FROM products WHERE sellerid='" . $_GET['id'] . "' ORDER BY created_at ASC";
    $myQry = mysqli_query($koneksidb, $mySql)  or die ("Query salah : ".mysql_error());
    $nomor = 0; 
	?>

<table class="table table-striped alignmiddle">
  <thead  style='background-color:#FF4500; color:#FFF;'>
    <tr class="home">
      <th>No.</th>
      <th>IMAGE</th>
      <th>ARTIST & TITLE</th>
      <th>PRICE</th>
      <th>CATEGORY</th>
      <th>EDIT</th>
      <th>DELETE</th>
      <th>SUNDUL</th>
      <th>ACTIVE</th>
    </tr>
  </thead>
  <tbody>
    <tr>
    <?php
    while ($myData = mysqli_fetch_array($myQry)) {
		$nomor++;
    $Kode = $myData['id'];
    $code = $myData['kode'];
    ?>
      <td>
        <?php echo $nomor; ?> 
      </td>
     <td>
        <?php if ($myData['image'] == "") { ?>
        <img id="noimage" height="100" width="100" src="../public/images/noimage.png">
        <?php } else { ?>
        <img id="noimage" height="100" width="100" src="../public/product_images/<?php echo $myData['id'] ?>/<?php echo $myData['image'] ?>">
        <?php } ?>
      </td>
      <td>
        <?php echo $myData['artist']; ?>  "<i><?php echo $myData['title']; ?></i>"
      </td>
      <td>IDR.
        <?php echo $myData['price']; ?>
      </td>
      <td>
      <?php
        $mySqlcat = "SELECT title FROM categories WHERE id=".$myData['category'];
        $myQrycat = mysqli_query($koneksidb, $mySqlcat)  or die ("Query salah : ".mysql_error());
        $myDataCat = mysqli_fetch_array($myQrycat);
        ?>
      <?php echo $myDataCat['title']; ?>
      </td>
      <td><a href="./?page=adsja-edit&id=<?php  echo $_GET['id']?>&kode=<?php echo $myData['kode']; ?>"><i class="fa fa-edit"></i></a></td>
      <td><a class="confirmDeletion" href="/user/ads/ads_delete/<%= product._id %>"><i class="fa fa-trash"></i></a></td>
      <td><a class="confirmSundul" href="/user/ads/sundul/<%= product._id %>">Sundul</a></td>
      <td>
        <select  onchange="this.className=this.options[this.selectedIndex].className" name="negotiable" id="negotiable" class='grey_color'  >
            <?php
                  echo "<option class='grey_color' value='y'>Active</option>";
                  echo "<option class='red_color' value='n'>No Active</option>";
            ?> 
        </select>
      </td>
    </tr>
    <?php }; ?>
  </tbody>
</table>

<?php } else { ?>
<h3 class="text-center">There are no products.</h3>
<?php } ?>
