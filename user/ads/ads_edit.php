<?php
include_once "../library/inc.seslogin.php";
include_once "../library/config.php";
include_once "../library/inc.library.php";
  
  // Ambil data NIS yang dikirim oleh index.php melalui URL
  $id = $_GET['id'];
  $kode = $_GET['kode'];
  // Query untuk menampilkan data siswa berdasarkan NIS yang dikirim
  $query = "SELECT * FROM users WHERE kode='".$id."'";
  $sql = mysqli_query($koneksidb, $query);  // Eksekusi/Jalankan query dari variabel $query
  $myData = mysqli_fetch_array($sql); // Ambil data dari hasil eksekusi $sql

  $queryproduct = "SELECT * FROM products WHERE kode='".$kode."'";
  $sqlproduct = mysqli_query($koneksidb, $queryproduct);  // Eksekusi/Jalankan query dari variabel $query
  $dataproduct = mysqli_fetch_array($sqlproduct); // Ambil data dari hasil eksekusi $sql
?>

<input type="hidden" name="sellerid" id="sellerid" value="<?php echo $id; ?>"/>

<div class="ibox-content col-lg-8 mx-auto">
<div class="stepsBox">
                <div class="claimSteps" id="stepOne">
                     <h1  style=' font-weight: bold;'> <b>Input Ads</b></h1>  
                </div>
            </div>
<table class="table table-striped">
  <tbody>
  <tr>
      <td><strong>Category </strong></td>
      <td><strong>:</strong></td>
      <td>
      <select id="category" name="category"  onchange="" width="100px" style="color:black;width:200px;overflow:hidden;">
																		<option value="NULL">Pilih</option>
																		<?php
                                          $datasql= "SELECT * FROM categories ORDER BY created_at";
																					$datasqlqry = mysqli_query($koneksidb, $datasql) or die ("Gagal Query".mysql_error());
																					while ($dataRowRegs = mysqli_fetch_array($datasqlqry)) {
																					if ($dataRowRegs['id'] == $dataproduct['category']) {
																						$cek = "selected";
																					} else { $cek=""; }
																					echo "<option value='$dataRowRegs[id]' $cek >$dataRowRegs[judul]</option>";
																				}
																		?>
																</select>
      </td>
    </tr>
    <tr>
      <td>Artist</td>
      <td>:</td>
      <td><input style="color:black;" name="artist" id="artist" size="30" placeholder="artist" value="<?php echo $dataproduct['artist']; ?>"/></td>
    </tr>
    <tr>
      <td>Art Title</td>
      <td>:</td>
      <td><input style="color:black;" name="title" id="title" size="50" placeholder="title" value="<?php echo $dataproduct['title']; ?>" /></td>
    </tr>
    <tr>
      <td>Size</td>
      <td>:</td>
      <td><input style="color:black;" name="size" id="size" size="40" placeholder="size" value="<?php echo $dataproduct['ukuran']; ?>" /></td>
    </tr>
    <tr>
      <td>Media</td> 
      <td>:</td>
       <td><input style="color:black;" name="media" id="media" size="40" placeholder="media" value="<?php echo $dataproduct['media']; ?>" /></td>
    </tr>
    <tr>
      <td>Description</td>
      <td>:</td>
       <td><input style="color:black;" name="description" id="description" size="70" placeholder="description of art" value="<?php echo $dataproduct['descs']; ?>"/></td>
    </tr>
    <tr>
      <td>Year</td>
      <td>:</td>
       <td><input style="color:black;" name="year" id="year" size="5" placeholder="year" value="<?php echo $dataproduct['tahun']; ?>" /></td>
    </tr>
    <tr>
      <td>Price</td>
      <td>:</td>
       <td><input style="color:black;" name="price" id="price" size="15" placeholder="price" value="<?php echo $dataproduct['price']; ?>"  /></td>
    </tr>
    <tr>
      <td>Discount</td>
      <td>:</td>
       <td><input style="color:black;" name="discount" id="discount" size="10" placeholder="discount" value="<?php echo $dataproduct['discount']; ?>"/></td>
    </tr> 
    <tr>
      <td>Negotiable</td>
      <td>:</td>
       <td><b>
        <select name="negotiable" id="negotiable" >
          <option value="Y">Y</option>  
          <?php
		      $pilihan	= array("Y", "N");
          foreach ($pilihan as $nilai) {
            echo "<option value='$nilai'>$nilai</option>";
          }
          ?> 
        </b>
        </select>&nbsp;<i>Y : yes, N : no</i>
      </td>
    </tr>
    <tr>
      <td>Active</td>
      <td>:</td>
      <td><b>
        <select name="active" id="active">
          <option value="Y">Y</option>  
          <?php
		      $pilihan	= array("Y", "N");
          foreach ($pilihan as $nilai) {
            echo "<option value='$nilai'>$nilai</option>";
          }
          ?>
        </b>
        </select>&nbsp;<i>Y : yes, N : no</i>
      </td>
    </tr>
    <tr>
      <td>Image</td>
      <td>:</td>
       <td><!-- input style="color:black;" name="image" id="image" size="50"  type="file" onchange="fileValidation();" / -->
       <div class="container">
        <div class="dropzone dz-clickable" id="myDrop">
            <div class="dz-default dz-message" data-dz-message="">
                <span>Drop files here to upload</span>
            </div>
        </div>
    </div>
    <hr class="my-1">
    <div class="container">
    	<div id="msg" class="mb-3"></div>
        <a href="javascript:void(0);" class="btn btn-outline-primary reorder" id="updateReorder">Reorder Images</a>
        <div id="reorder-msg" class="alert alert-warning mt-3" style="display:none;">
            <!--i class="fa fa-3x fa-exclamation-triangle float-right"></i--> <div class="h6">1. Drag photos to reorder.<br>2. Click 'Save Reordering' when finished.</div>
        </div>
        <div class="gallery">
            <ul class="nav nav-pills">
            <?php
        //Fetch all images from database
                $images = $db->getAllRecords(TB_IMG, '*', "And product='".  $dataproduct['kode'] ."'" ,"ORDER BY img_order ASC");
               // $images = $db->get(TB_IMG,"product=>keZl3lVUCLwOrx8ot41mxj1PH,img_order=>1");
                if(!empty($images)){
                    foreach($images as $row){
                ?>
                <li id="image_li_<?php echo $row['id']; ?>" class="ui-sortable-handle mr-2 mt-2">
                    <div><a href="javascript:void(0);" class="img-link"><img src="../public/product_images/<?php echo $row['img_name']; ?>" alt="" class="img-rounded"   style="width:142px;"></a></div>
                </li>
                <?php
                    }
                }
            ?>
            </ul>
        </div>
    </div>
       
       </td>
    </tr>  
    
    <input type="hidden" name="codeproduct" id="codeproduct" value="<?php echo  randgenerate(25);?> " />  
    
    <tr>
        <td colspan="3">
          <input type="button" id="edit_file" value="Save" class="btn btn-primary mt-3">
          <input type="button" id="" value="Cancel" class="btn btn-primary mt-3">
        </td>
    </tr>
  </tbody>
</table>
</div>

<!-- script type="text/javascript">
  // function fileValidation(){
  //     var fileInput = document.getElementById('image');
  //     var filePath = fileInput.value;
  //     var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
  //     if(!allowedExtensions.exec(filePath)){
  //         alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
  //         fileInput.value = '';
  //         return false;
  //     }else{
  //         //Image preview
  //         if (fileInput.files && fileInput.files[0]) {
  //             var reader = new FileReader();
  //             reader.onload = function(e) {
  //                 document.getElementById('imagePreview').innerHTML = '<img width="200" src="'+e.target.result+'"/>';
  //             };
  //             reader.readAsDataURL(fileInput.files[0]);
  //         }
  //     }
  //   }
</script -->

