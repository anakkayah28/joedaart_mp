<?php

include_once "../library/inc.connection.php";
include_once "../library/inc.library.php";
# Tombol Simpan diklik

	# Validasi form, jika kosong sampaikan pesan error
	$pesanError = array();
	if (trim($_POST['category'])=="") {
		$pesanError[] = "Please choose <b>category</b> first";		
  }
  if (trim($_POST['title'])=="") {
		$pesanError[] = "<b>title</b> is not empty";		
	}

  # Baca Variabel Form
    $kode = randgenerate(25);
    $category     = $_POST['category'];
    $artist		    = $_POST['artist'];
    $title		    = $_POST['title'];
    $ukuran		    = $_POST['size'];
    $media		    = $_POST['media'];
    $desc	        = $_POST['description'];
    $tahun	      = $_POST['year'];
    $price		    = $_POST['price'];
    $discount		  = $_POST['discount'];
    $sellername		= $_POST['sellername'];
    $galery	  	  = $_POST['gallery'];
    $phone	      = $_POST['phone'];
    $sms		      = $_POST['sms'];
    $wa     		  = $_POST['whatsapp'];
    $email		    = $_POST['email'];
    $address		  = $_POST['address'];
    $country		  = $_POST['country'];
    $province		  = $_POST['province'];
    $city		      = $_POST['city'];
    $negotiable		= $_POST['negotiable'];
    $activeflag		= $_POST['active'];
    //$		= $_POST[''];

    $foto = $_FILES['image']['name'];
    $tmp = $_FILES['image']['tmp_name'];

    $fotobaru = date('dmYHis').$foto;
    //*
    $path = "../../images/kategori/kategorisub/".$fotobaru;


// Proses upload
if(move_uploaded_file($tmp, $path)){ // Cek apakah gambar berhasil diupload atau tidak
    // Proses simpan ke Database
    $query = "INSERT INTO `products` 
    (`kode`, `title`, `artist`, `sellerid`, `sellername`, `category`,  `categorychar`, `slug`, `descs`, `price`, 
     `discount`, `negotiable`, `galery`, `phone`, `address`, `activeflag`, `image`, `sms`, `wa`, `media`, 
     `tahun`, `ukuran`, `email`, `country`, `province`, `city`, `uploaddate`, `userid`, `created_by`, `updated_by`) 
     VALUES
    (`$kode`, `$title`, `$artist`, `$sellerid`, `$sellername`, `$category`, `$categorychar`, `$slug`, `$descs`, `$price`, 
     `$discount`, `$negotiable`, `$galery`, `$phone`, `$address`, `$activeflag`, `$image`, `$sms`, `$wa`, `$media`, 
     `$tahun`, `$ukuran`,  `$email`, `$country`, `$province`, `$city`, `$uploaddate`, `$userid`, `$created_by`, `$updated_by`)";

    $sql = mysqli_query($koneksidb, $query); // Eksekusi/ Jalankan query dari variabel $query
    if($sql){ // Cek jika proses simpan ke database sukses atau tidak
      // Jika Sukses, Lakukan :
      echo "<script type='text/javascript'>alert('Simpan Berhasil');</script>";
      echo '<script>window.open("..?page=Kategorisub-Data","_self")</script>;';
    }else{
      // Jika Gagal, Lakukan :
      echo "Maaf, Terjadi kesalahan saat mencoba untuk menyimpan data ke database.";
      echo "<br><a href='form_simpan.php'>Kembali Ke Form</a>";
    }
  }else{
    // Jika gambar gagal diupload, Lakukan :
    echo "Maaf, Gambar gagal untuk diupload.";
    echo "<br><a href='form_simpan.php'>Kembali Ke Form</a>";
  }
  ?>