<?php
include_once "library/inc.seslogin.php";
  
  // Ambil data NIS yang dikirim oleh index.php melalui URL
  $id = '1';
  
  // Query untuk menampilkan data siswa berdasarkan NIS yang dikirim
  $query = "SELECT * FROM rumah WHERE id='".$id."'";
  
  $sql = mysqli_query($koneksidb, $query);  // Eksekusi/Jalankan query dari variabel $query
  $myData = mysqli_fetch_array($sql); // Ambil data dari hasil eksekusi $sql

?>

<form method="post" action="rumah/rumah_saveedit.php" enctype="multipart/form-data">
  <table style="font-size:12px;" class="table" width="100%" border="0" cellspacing="1" cellpadding="3">
    <tr>
      <th colspan="3" scope="col"  bgcolor="#CCCCCC">Home</th>
      <input name="txtKode" type="hidden" value="<?php echo $myData['id']; ?>" />
    </tr>

    <tr>
      <td><strong>Judul Header</strong></td>
      <td><strong>:</strong></td>
      <td><input name="txtjudul" value="<?php echo $myData['judul_web_header']; ?>" size="100" maxlength="100" /></td>
    </tr>

		<tr>
      <td><strong>Logo Header </strong></td>
      <td><strong>:</strong></td>
      <td>
      <?php
      if(is_file("./images/kategori/".$myData['logo_header'])){ // Jika foto ada
      ?>
        <img src="./images/kategori/".<?php $myData['logo_header']; ?> />
      <?php
      }
      ?>
      <input name="txtlogoheader" id="txtlogoheader" type="file" value="<?php echo $myData['logo_header']; ?>" onchange="fileValidationlogoheader();" />
      <input type="checkbox" name="ubah_foto" value="true"> Ceklis jika ingin mengubah foto<b>
      <div id="imglogoheader" width="100px" length="100px"></div>
      </td>
    </tr>

    <tr>
      <td><strong>Deskripsi Header </strong></td>
      <td><strong>:</strong></td>
      <td>
					<textarea name="txtdeskripsiheader" rows="4" cols="100"><?php  echo $myData['deskripsi_header']; ?></textarea>
			</td>
    </tr>

		<tr>
      <td><strong>Deskripsi Menu </strong></td>
      <td><strong>:</strong></td>
      <td><input name="txtdeskripsimenu" value="<?php  echo $myData['deskripsi_menu']; ?>" size="100" /></td>
    </tr>

		<tr>
      <td><strong>Logo Footer </strong></td>
      <td><strong>:</strong></td>
      <td><input name="txtlogofooter" id="txtlogofooter"  type="file" value="<?php echo $myData['logo_footer'];; ?>"  onchange="fileValidationlogofooter();" />
      <input type="checkbox" name="ubah_foto2" value="true"> Ceklis jika ingin mengubah foto<b>
      <div id="imglogofooter" width="100px" length="100px"></div>
      </td>
    </tr>

		<tr>
      <td><strong>Deskripsi Footer</strong></td>
      <td><strong>:</strong></td>
      <td>
			<textarea name="txtdeskripsifooter" rows="4" cols="100"><?php  echo $myData['deskripsi_footer']; ?></textarea>
			</td>
    </tr>

		<tr>
      <td><strong>Alamat</strong></td>
      <td><strong>:</strong></td>
      <td><input name="txtalamat" value="<?php  echo $myData['alamat']; ?>" size="100" maxlength="100"/></td>
    </tr>

		<tr>
      <td><strong>Whatsapp</strong></td>
      <td><strong>:</strong></td>
      <td><input name="txtwa1" value="<?php  echo $myData['wa1']; ?>" size="100" maxlength="100"/></td>
    </tr>

		<tr>
      <td><strong>Line</strong></td>
      <td><strong>:</strong></td>
      <td><input name="txtwa2" value="<?php  echo $myData['wa2']; ?>" size="100" maxlength="100"/></td>
    </tr>

		<tr>
      <td><strong>Handphone</strong></td>
      <td><strong>:</strong></td>
      <td><input name="txthp1" value="<?php  echo $myData['hp1']; ?>"size="100" maxlength="100"/></td>
    </tr>

		<tr>
      <td><strong>SMS</strong></td>
      <td><strong>:</strong></td>
      <td><input name="txthp2" value="<?php  echo $myData['hp2']; ?>"size="100" maxlength="100"/></td>
    </tr>

		<tr>
      <td><strong>Email</strong></td>
      <td><strong>:</strong></td>
      <td><input name="txtemail1" value="<?php  echo $myData['email1']; ?>"size="100" maxlength="100"/></td>
    </tr>

		<tr>
      <td><strong>Email2</strong></td>
      <td><strong>:</strong></td>
      <td><input name="txtemail2" value="<?php  echo $myData['email2']; ?>"size="100" maxlength="100"/></td>
    </tr>

		<tr>
      <td><strong>Facebook</strong></td>
      <td><strong>:</strong></td>
      <td><input name="txtfb" value="<?php  echo $myData['fb']; ?>"size="100" maxlength="100"/></td>
    </tr>

		<tr>
      <td><strong>Instagram</strong></td>
      <td><strong>:</strong></td>
      <td><input name="txig" value="<?php  echo $myData['ig']; ?>"size="100" maxlength="100"/></td>
    </tr>

		<tr>
      <td><strong>Linkedin</strong></td>
      <td><strong>:</strong></td>
      <td><input name="txtw" value="<?php  echo $myData['tw']; ?>"size="100" maxlength="100"/></td>
    </tr>

		<tr>
      <td><strong>Logo About Us </strong></td>
      <td><strong>:</strong></td>
      <td><input name="txtlogoaboutus" id="txtlogoaboutus" type="file" value="<?php echo $myData['logo_about_us'];; ?>"  onchange="fileValidationlogoaboutus();" />
      <input type="checkbox" name="ubah_foto3" value="true"> Ceklis jika ingin mengubah foto<b>
      <div id="imglogoaboutus" width="100px" length="100px"></div>
      </td>
    </tr>

		<tr>
      <td><strong>Deskripsi About Us</strong></td>
      <td><strong>:</strong></td>
      <td>
			<textarea name="txtdeskripsiaboutus" rows="4" cols="100"><?php  echo $myData['deskripsi_about_us']; ?></textarea>
			</td>
    </tr>

    <tr>
		<td><input type="submit" name="btnSimpan"  class="btn btn-primary" value=" SIMPAN " style="cursor:pointer;">
			<a href="?page=Kategori-Data" target="_self"  class="btn btn-primary" id="btnbatal">BATAL
        </td>
    </tr>
  </table>
</form>

<script type="text/javascript">
  function fileValidationlogoheader(){
      var fileInput = document.getElementById('txtlogoheader');
      var filePath = fileInput.value;
      var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
      if(!allowedExtensions.exec(filePath)){
          alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
          fileInput.value = '';
          return false;
      }else{
          //Image preview
          if (fileInput.files && fileInput.files[0]) {
              var reader = new FileReader();
              reader.onload = function(e) {
                  document.getElementById('imglogoheader').innerHTML = '<img width="200" src="'+e.target.result+'"/>';
              };
              reader.readAsDataURL(fileInput.files[0]);
          }
      }
    }

    function fileValidationlogofooter(){
      var fileInput = document.getElementById('txtlogofooter');
      var filePath = fileInput.value;
      var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
      if(!allowedExtensions.exec(filePath)){
          alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
          fileInput.value = '';
          return false;
      }else{
          //Image preview
          if (fileInput.files && fileInput.files[0]) {
              var reader = new FileReader();
              reader.onload = function(e) {
                  document.getElementById('imglogofooter').innerHTML = '<img width="200" src="'+e.target.result+'"/>';
              };
              reader.readAsDataURL(fileInput.files[0]);
          }
      }
    }

    function fileValidationlogoaboutus(){
      var fileInput = document.getElementById('txtlogoaboutus');
      var filePath = fileInput.value;
      var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
      if(!allowedExtensions.exec(filePath)){
          alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
          fileInput.value = '';
          return false;
      }else{
          //Image preview
          if (fileInput.files && fileInput.files[0]) {
              var reader = new FileReader();
              reader.onload = function(e) {
                  document.getElementById('imglogoaboutus').innerHTML = '<img width="200" src="'+e.target.result+'"/>';
              };
              reader.readAsDataURL(fileInput.files[0]);
          }
      }
    }
</script>