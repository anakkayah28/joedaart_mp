<?php
include_once "library/inc.seslogin.php";

# Tombol Simpan diklik
if(isset($_POST['btnSimpan'])){
	# VALIDASI FORM, jika ada kotak yang kosong, buat pesan error ke dalam kotak $pesanError
	$pesanError = array();
	if (trim($_POST['txtKode'])=="") {
		$pesanError[] = "Data <b>Kode </b> tidak terbaca !";		
	}
	if (trim($_POST['txtNama'])=="") {
		$pesanError[] = "Data <b>Nama Petugas</b> tidak boleh kosong !";		
	}
	if (trim($_POST['txtTelepon'])=="") {
		$pesanError[] = "Data <b>No. Telepon</b> tidak boleh kosong !";		
	}
	if (trim($_POST['txtUsername'])=="") {
		$pesanError[] = "Data <b>Username</b> tidak boleh kosong !";		
	}
	if (trim($_POST['cmbLevel'])=="KOSONG") {
		$pesanError[] = "Data <b>Level login</b> belum dipilih !";		
	}
			
	# BACA DATA DALAM FORM, masukkan datake variabel
	$txtNama	= $_POST['txtNama'];
	$txtUsername= $_POST['txtUsername'];
	$txtPassword= $_POST['txtPassword'];
	$txtPassLama= $_POST['txtPassLama'];	
	$txtTelepon	= $_POST['txtTelepon'];	
	$cmbLevel	= $_POST['cmbLevel'];
	$cmbAktif	= $_POST['cmbAktif'];
	
	# VALIDASI petugas LOGIN (username), jika sudah ada akan ditolak
	$cekSql="SELECT * FROM petugas WHERE username='$txtUsername' AND NOT(username='".$_POST['txtUsernameLm']."')";
	$cekQry=mysqli_query($koneksidb, $cekSql) or die ("Eror Query".mysql_error()); 
	if(mysqli_num_rows($cekQry)>=1){
		$pesanError[] = "Username<b> $txtUsername </b> sudah ada, ganti dengan yang lain";
	}

	# JIKA ADA PESAN ERROR DARI VALIDASI
	if (count($pesanError)>=1 ){
		echo "<div class='mssgBox'>";
		echo "<img src='images/attention.png'> <br><hr>";
			$noPesan=0;
			foreach ($pesanError as $indeks=>$pesan_tampil) { 
			$noPesan++;
				echo "&nbsp;&nbsp; $noPesan. $pesan_tampil<br>";	
			} 
		echo "</div> <br>"; 
	}
	else {
		# Cek Password baru
		if (trim($txtPassword)=="") {
			$sqlPasword = ", password='$txtPassLama'";
		}
		else {
			$sqlPasword = ",  password ='".$txtPassword."'";
			#$sqlPasword = ",  password ='".md5($txtPassword)."'";
		}
		
		# SIMPAN DATA KE DATABASE (Jika tidak menemukan error, simpan data ke database)
		$mySql  = "UPDATE petugas SET nm_petugas='$txtNama', username='$txtUsername', 
					no_telepon='$txtTelepon', level='$cmbLevel', aktif='$cmbAktif'
					$sqlPasword  
					WHERE kd_petugas='".$_POST['txtKode']."'";
		$myQry=mysqli_query($koneksidb, $mySql) or die ("Gagal query".mysql_error());
		if($myQry){
			echo "<meta http-equiv='refresh' content='0; url=?page=Petugas-Data'>";
		}
		exit;
	}	
} // Penutup Tombol Simpan


# TAMPILKAN DATA DARI DATABASE, Untuk ditampilkan kembali ke form edit
$Kode	= isset($_GET['Kode']) ?  $_GET['Kode'] : $_POST['txtKode']; 
$mySql	= "SELECT * FROM petugas WHERE kd_petugas='$Kode'";
$myQry	= mysqli_query($koneksidb, $mySql)  or die ("Query ambil data salah : ".mysql_error());
$myData = mysqli_fetch_array($myQry);

	// Data Variabel Temporary (sementara)
	$dataKode		= $myData['kd_petugas'];
	$dataNama		= isset($_POST['txtNama']) ? $_POST['txtNama'] : $myData['nm_petugas'];
	$dataUsername	= isset($_POST['txtUsername']) ? $_POST['txtUsername'] : $myData['username'];
	$dataTelepon	= isset($_POST['txtTelepon']) ? $_POST['txtTelepon'] : $myData['no_telepon'];
	$dataLevel		= isset($_POST['cmbLevel']) ? $_POST['cmbLevel'] : $myData['level'];
	$dataAktif		= isset($_POST['cmbAktif']) ? $_POST['cmbAktif'] : $myData['aktif'];
?>

<form action="<?php $_SERVER['PHP_SELF']; ?>" method="post" name="form1" target="_self">
  <table style="font-size:12px;" width="100%" class="table-list" border="0" cellspacing="1" cellpadding="4">
    <tr>
		<th colspan="3" scope="col"  bgcolor="#CCCCCC"><b>UBAH Master Petugas </b></th>
    </tr>
    <tr>
      <td width="181"><b>Kode</b></td>
      <td width="5"><b>:</b></td>
      <td width="1000"> <input name="textfield" type="text"  value="<?php echo $dataKode; ?>" size="10" maxlength="10"  readonly="readonly"/>
      <input name="txtKode" type="hidden" value="<?php echo $dataKode; ?>" /></td>
    </tr>
    <tr>
      <td><b>Nama Petugas </b></td>
      <td><b>:</b></td>
      <td><input name="txtNama" type="text" value="<?php echo $dataNama; ?>" size="80" maxlength="100" /></td>
    </tr>
    <tr>
      <td><b>No. Telepon </b></td>
      <td><b>:</b></td>
      <td><input name="txtTelepon" type="text" value="<?php echo $dataTelepon; ?>" size="60" maxlength="20" /></td>
    </tr>
    <tr>
      <td><b>Username</b></td>
      <td><b>:</b></td>
      <td><input name="txtUsername" type="text"  value="<?php echo $dataUsername; ?>" size="60" maxlength="20" />
      <input name="txtUsernameLm" type="hidden" value="<?php echo $myData['username']; ?>" /></td>
    </tr>
    <tr>
      <td><b>Password</b></td>
      <td><b>:</b></td>
      <td><input name="txtPassword" type="password" size="60" maxlength="20" />
      <input name="txtPassLama" type="hidden" value="<?php echo $myData['password']; ?>" /></td>
    </tr>
    <tr>
      <td><b>Level</b></td>
      <td><b>:</b></td>
      <td><b>
        <select name="cmbLevel">
          <option value="KOSONG">--Please Select--</option>
          <?php
						$pilihan	= array("Apotek", "Admin" ,"Dokter" ,"AdminApotek" ,"Klinik","Toko");
						foreach ($pilihan as $nilai) {
							if ($dataLevel==$nilai) {
									$cek=" selected";
							} else { $cek = ""; }
							echo "<option value='$nilai' $cek>$nilai</option>";
						}
						?>
        </select>
      </b></td>
    </tr>
		<tr>
      <td><b>Aktif</b></td>
      <td><b>:</b></td>
      <td><b>
        <select name="cmbAktif">
          <option value="KOSONG">....</option>
          <?php
						$pilihan	= array("Yes", "No");
						foreach ($pilihan as $nilai){
							if ($dataAktif==$nilai) {
									$cek=" selected";
							} else { $cek = ""; }
							echo "<option value='$nilai' $cek>$nilai</option>";
						}
          ?>
        </select>
      </b></td>
    </tr>
    <tr>
		<td><input type="submit" name="btnSimpan"  class="btn btn-primary" value=" SIMPAN " style="cursor:pointer;">
			<a href="?page=Petugas-Data" target="_self"  class="btn btn-primary" id="btnbatal">BATAL</td>
    </tr>
  </table>
</form>
