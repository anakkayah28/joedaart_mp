<?php
include_once "../library/inc.seslogin.php";

# Tombol Simpan diklik
if(isset($_POST['btnSimpan'])){
	# VALIDASI FORM, jika ada kotak yang kosong, buat pesan error ke dalam kotak $pesanError
	$pesanError = array();

	if (trim($_POST['txtNama'])=="") {
		$pesanError[] = "Data <b>Nama Petugas</b> tidak boleh kosong !";		
	}
	if (trim($_POST['txtTelepon'])=="") {
		$pesanError[] = "Data <b>No. Telepon</b> tidak boleh kosong !";		
    }
    if (trim($_POST['txtWa'])=="") {
		$pesanError[] = "Data <b>No. Wa</b> tidak boleh kosong !";		
    }
    if (trim($_POST['txtCountry'])=="") {
		$pesanError[] = "Data <b>Country</b> tidak boleh kosong !";		
    }
    if (trim($_POST['txtProvince'])=="") {
		$pesanError[] = "Data <b>Province</b> tidak boleh kosong !";		
    }
  
			
	# BACA DATA DALAM FORM, masukkan datake variabel
	$txtNama	 = $_POST['txtNama'];
	$txtTelepon  = $_POST['txtTelepon'];
	$txtWa       = $_POST['txtWa'];
	$txtCountry  = $_POST['txtCountry'];	
	$txtProvince = $_POST['txtProvince'];

    $txtPassword= $_POST['txtPassword'];
	//$txtPassLama= $_POST['txtPassLama'];	
	
	// # VALIDASI petugas LOGIN (username), jika sudah ada akan ditolak
	// $cekSql="SELECT * FROM petugas WHERE email='$txtUsername' AND NOT(username='".$_POST['txtUsernameLm']."')";
	// $cekQry=mysqli_query($koneksidb, $cekSql) or die ("Eror Query".mysql_error()); 
	// if(mysqli_num_rows($cekQry)>=1){
	// 	$pesanError[] = "Username<b> $txtUsername </b> sudah ada, ganti dengan yang lain";
	// }

    # JIKA ADA PESAN ERROR DARI VALIDASI
    $sqlPasword  ='';
	if (count($pesanError)>=1 ){
		echo "<div class='mssgBox'>";
		//echo "<img src='images/attention.png'> <br><hr>";
			$noPesan=0;
			foreach ($pesanError as $indeks=>$pesan_tampil) { 
			$noPesan++;
				echo "&nbsp;&nbsp; $noPesan. $pesan_tampil<br>";	
			} 
		echo "</div> <br>"; 
	}
	else {
		# Cek Password baru
		if (trim($txtPassword)=="") {
            #$sqlPasword = ", password='$txtPassLama'";
            //$sqlPasword = ",  password ='".md5($txtPassLama)."'";
		}
		else {
			$sqlPasword = ",  password ='".md5($txtPassword)."'";
		}
		
		# SIMPAN DATA KE DATABASE (Jika tidak menemukan error, simpan data ke database)
		$mySql  = "UPDATE users SET name='$txtNama', phone='$txtTelepon', 
					wa='$txtWa', country='$txtCountry', province='$txtProvince'
					$sqlPasword  
                    WHERE kode='".$_GET['id']."'";
        $myQry=mysqli_query($koneksidb, $mySql) or die ("Gagal query".mysql_error());
        if($myQry){
            
            echo "<meta http-equiv='refresh' content='0; url=?page=Useract-Myaccount&id=". $_GET['id']. "'>"; 
        }
		exit;
	}	
} // Penutup Tombol Simpan
   
    # TAMPILKAN DATA DARI DATABASE, Untuk ditampilkan kembali ke form edit
    $Kode	= isset($_GET['id']) ?  $_GET['id'] : $_POST['txtKode']; 
    $mySql	= "SELECT * FROM users WHERE kode='$Kode'";
    $myQry	= mysqli_query($koneksidb, $mySql)  or die ("Query ambil data salah : ".mysql_error());
    $myData = mysqli_fetch_array($myQry);

	// Data Variabel Temporary (sementara)
	$dataKode		= $myData['kode'];
	$dataNama		= isset($_POST['txtNama']) ? $_POST['txtNama'] : $myData['name'];
	$dataTelepon	= isset($_POST['txtTelepon']) ? $_POST['txtTelepon'] : $myData['phone'];
	$dataWa         = isset($_POST['txtWa']) ? $_POST['txtWa'] : $myData['wa'];
	$dataCountry    = isset($_POST['txtCountry']) ? $_POST['txtCountry'] : $myData['country'];
	$dataProvince	= isset($_POST['txtProvince']) ? $_POST['txtProvince'] : $myData['province'];
?>
<div class="ibox-content col-lg-8 mx-auto">
<form action="<?php $_SERVER['PHP_SELF']; ?>" method="post" name="form1" target="_self">
  <table style="font-size:12px;" width="100%" class="table-list" border="0" cellspacing="1" cellpadding="4">
    <tr>
		<th colspan="3" scope="col"  bgcolor="#CCCCCC"><b>Data Pribadi</b></th>
    </tr>
    <tr>
      <td><b>Nama </b></td>
      <td><b>:</b></td>
      <td><input name="txtNama" type="text" value="<?php echo $dataNama; ?>" size="80" maxlength="100" /></td>
    </tr>
    <tr>
      <td><b>No. Telepon </b></td>
      <td><b>:</b></td>
      <td><input name="txtTelepon" type="text" value="<?php echo $dataTelepon; ?>" size="60" maxlength="20" /></td>
    </tr>
    <tr>
      <td><b>No. Whatsapp </b></td>
      <td><b>:</b></td>
      <td><input name="txtWa" type="text" value="<?php echo $dataTelepon; ?>" size="60" maxlength="20" /></td>
    </tr>
 

    <tr>
      <td><strong>Country </strong></td>
      <td><strong>:</strong></td>
      <td>
      <select id="txtCountry" name="txtCountry"  onchange="" width="100px" style="color:black;width:200px;overflow:hidden;">
																		<option value="NULL">Pilih</option>
																		<?php
                                          $datasql= "SELECT * FROM countries";
																					$datasqlqry = mysqli_query($koneksidb, $datasql) or die ("Gagal Query".mysql_error());
																					while ($dataRow = mysqli_fetch_array($datasqlqry)) {
																					if ($dataRow['id'] == $dataCountry) {
																						$cek = "selected";
																					} else { $cek=""; }
																					echo "<option value='$dataRow[id]' $cek >$dataRow[name]</option>";
																				}
																		?>
																</select>
      </td>
    </tr>

    <tr>
      <td><strong>Province </strong></td>
      <td><strong>:</strong></td>
      <td>
      <select id="txtProvince" name="txtProvince"  onchange="" width="100px" style="color:black;width:200px;overflow:hidden;">
																		<option value="">Pilih</option>
																		<?php
                                          $datasql= "SELECT * FROM provinces";
																					$datasqlqry = mysqli_query($koneksidb, $datasql) or die ("Gagal Query".mysql_error());
																					while ($dataRow1 = mysqli_fetch_array($datasqlqry)) {
																					if ($dataRow1['id'] == $dataProvince) {
																						$cek = "selected";
																					} else { $cek=""; }
																					echo "<option value='$dataRow1[id]' $cek >$dataRow1[name]</option>";
																				}
																		?>
																</select>
      </td>
    </tr>
    <tr>
		<th colspan="3" scope="col"  bgcolor="#CCCCCC"><b>Ganti Password</b></th>
    </tr>
    <tr>
      <td><b>Password Lama</b></td>
      <td><b>:</b></td>
      <td><input name="txtPassLama" type="password" size="60" maxlength="20" />
    </tr>

    <tr>
      <td><b>Password Baru</b></td>
      <td><b>:</b></td>
      <td><input name="txtPassword" type="password" size="60" maxlength="20" />
    </tr>
	
    <tr>
		<td><input type="submit" name="btnSimpan"  class="btn btn-primary" value=" SIMPAN " style="cursor:pointer;">
			<a href="?page=Useract-Myaccount&id=<?php echo $_GET['id']; ?>" target="_self"  class="btn btn-primary" id="btnbatal">BATAL</td>
    </tr>
  </table>
</form>
</div>
