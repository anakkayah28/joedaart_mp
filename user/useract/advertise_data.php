<?php
include_once "../library/inc.seslogin.php";

# UNTUK PAGING (PEMBAGIAN HALAMAN)
$row = 50;
$hal = isset($_GET['hal']) ? $_GET['hal'] : 0;
$pageSql = "SELECT * FROM products WHERE status_permanen=1 ORDER BY created_at ASC";
$pageQry = mysqli_query( $koneksidb,$pageSql) or die ("error paging: ".mysql_error());
$jml	 = mysqli_num_rows($pageQry);
$max	 = ceil($jml/$row);


$idusr = isset($_GET['id']) ? $_GET['id'] : 0;
?>
<div class="container">

<div class="row">
<div class="col-lg-12">
<h3 class="page-header">Daftar Advertising</h3>
</div>
<!-- /.col-lg-12 -->
</div>

<table style="font-size:12px;" class="table table-striped table-bordered table-hover" id="dataTables-example"  width="50%"cellspacing="1" cellpadding="3">
  <tr>
    <td colspan="2">
<div class="dataTable-wrapper">
<div class="table-responsive">
<table style="font-size:12px;" class="table table-striped table-bordered table-hover" id="dataTables-example"  width="50%" cellspacing="1" cellpadding="3">
      <tr>
        <th width="10" align="center"><strong>No</strong></th>
        <th  width="10" align="center"><strong>Category</strong></th>
        <th  width="10" align="center"><strong>Seller ID</strong></th>
        <th  width="10" align="center"><strong>Title</strong></th>
        <th  width="10" align="center"><strong>Artist</strong></th>
        <td colspan="2" align="center" bgcolor="#CCCCCC"><strong>Aksi</strong></td>
      </tr>
      <?php
	$mySql = "SELECT pro.id as id, pro.kode as kodepro, cat.title as category,  usr.name as sellerid, pro.title as title, pro.artist as artist,
            pro.status,pro.status_permanen, usr.kode as kode
            FROM products pro, categories cat, users usr WHERE pro.category=cat.id AND usr.kode=pro.sellerid AND pro.status_permanen=1 ORDER BY pro.created_at DESC LIMIT $hal, $row";
	$myQry = mysqli_query($koneksidb, $mySql)  or die ("Query salah : ".mysql_error());
	$nomor = 0; 
	while ($myData = mysqli_fetch_array($myQry)) {
		$nomor++;
		$Kode = $myData['id'];
	?>
      <tr>
        <td><?php echo $nomor; ?></td>
        <td><?php echo $myData['category']; ?></td>
        <td><?php echo $myData['sellerid']; ?></td>
        <td><?php echo $myData['title']; ?></td>
        <td><?php echo $myData['artist']; ?></td>  
        <?php if ($myData['status']==1){ ?>
        <td width="10" align="center"><a href="?page=Adsact-Suspend&suspend=0&id=<?php echo $idusr; ?>&prodid=<?php echo $myData['kodepro'];?>" style="color:red" target="_self"  alt="Suspend User" onclick="return confirm('Anda yakin akan mensuspend iklan ini ?')">Suspend</a></td>
        <?php } else { ?>
          <td width="10" align="center"><a href="?page=Adsact-Suspend&suspend=1&id=<?php echo $idusr; ?>&prodid=<?php echo $myData['kodepro'];?>" target="_self" style="color:green"  alt="Suspend User" onclick="return confirm('Anda yakin akan mensuspend iklan ini ?')">Unsuspend</a></td>
        <?php } ?>
        <td width="10" align="center"><a href="?page=Adsact-Delete&suspend=0&id=<?php echo $idusr; ?>&prodid=<?php echo $myData['kodepro'];?>" style="color:red" target="_self"  alt="Hapus User" onclick="return confirm('Anda yakin akan mensuspend iklan ini ?')">Delete</a></td>
      </tr>
      <?php } ?>
    </table>
</div></div>
    </td>
  </tr>
  <tr class="selKecil">
    <td width="306"><strong>Jumlah Data :</strong> <?php echo $jml; ?></td>
    <td width="483" align="right"><strong>Halaman ke :</strong>
      <?php
	for ($h = 1; $h <= $max; $h++) {
		$list[$h] = $row * $h - $row;
		echo " <a href='?page=Kategori-Data&hal=$list[$h]'>$h</a> ";
	}
	?></td>
  </tr>
</table>

</div>  
