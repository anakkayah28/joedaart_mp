<?php
include_once "../library/inc.seslogin.php";

# UNTUK PAGING (PEMBAGIAN HALAMAN)
$row = 50;
$hal = isset($_GET['hal']) ? $_GET['hal'] : 0;
$pageSql = "SELECT * FROM categories";
$pageQry = mysqli_query( $koneksidb,$pageSql) or die ("error paging: ".mysql_error());
$jml	 = mysqli_num_rows($pageQry);
$max	 = ceil($jml/$row);

$iduser = isset($_GET['id']) ? $_GET['id'] : 0;
?>
<div class="container">
<div class="row">
<div class="col-lg-12">
<h3 class="page-header" style="text-align:center;">Category Lists</h3>
</div>
<!-- /.col-lg-12 -->
</div>


<table style="font-size:12px;" class="table table-striped table-bordered table-hover" id="dataTables-example"  width="100%"cellspacing="1" cellpadding="3">
  <tr>
    <td colspan="2"><a href="?page=Kategori-Add&id=<?php echo $iduser; ?>" target="_self"><img src="../images/btn_add.png" height="30" border="0" /> Add Category</a> </td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">
<div class="dataTable-wrapper">
<div class="table-responsive">
<table style="font-size:12px;" class="table table-striped table-bordered table-hover" id="dataTables-example"  width="100%"cellspacing="1" cellpadding="3">
      <tr>
        <th width="10" style="text-align:center;"><strong>No</strong></th>
        <th  width="10" align="center"><strong>Kategori</strong></th>
        <th  width="10" style="text-align:center;"  ><strong>Index</strong></th>

        <td colspan="2" align="center" bgcolor="#CCCCCC"><strong>Aksi</strong></td>
      </tr>
      <?php
	$mySql = "SELECT * FROM categories ORDER BY indeks ASC LIMIT $hal, $row";
	$myQry = mysqli_query($koneksidb, $mySql)  or die ("Query salah : ".mysql_error());
	$nomor = 0; 
	while ($myData = mysqli_fetch_array($myQry)) {
		$nomor++;
		$Kode = $myData['id'];
	?>
      <tr>
        <td style="text-align:center;"><?php echo $nomor; ?></td>
        <td><?php echo $myData['judul']; ?></td>
        <td  width="10" align="center"><input value="<?php echo $myData['indeks']; ?>" size="1" id="txtindex" name="txtindex" /></td>

        <td width="10" align="center"><a href="?page=Kategori-Edit&Kode=<?php echo $Kode; ?>&id=<?php echo $iduser; ?>" target="_self">Edit</a></td>
        <td width="10" align="center"><a href="?page=Kategori-Delete&Kode=<?php echo $Kode; ?>&id=<?php echo $iduser; ?>" target="_self" alt="Delete Data" onclick="return confirm('ANDA YAKIN AKAN MENGHAPUS DATA INI ... ?')">Delete</a></td>
      </tr>
      <?php } ?>
    </table>
</div></div>
    </td>
  </tr>
  <tr class="selKecil">
    <td width="306"><strong>Jumlah Data :</strong> <?php echo $jml; ?></td>
    <td width="483" align="right"><strong>Halaman ke :</strong>
      <?php
	for ($h = 1; $h <= $max; $h++) {
		$list[$h] = $row * $h - $row;
		echo " <a href='?page=Kategori-Data&hal=$list[$h]'>$h</a> ";
	}
	?></td>
  </tr>
</table>
</div>
